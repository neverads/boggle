package com.neverads.gobble.feature

import org.junit.Assert.*
import org.junit.Test
import kotlin.math.abs


data class Parameter(
    val index: Int,
    val isOptional: Boolean = false,
    val isVararg: Boolean = false,
    val name: String?,
    val type: String
)


class ClassPath {
    @Test
    fun type() {
        assertEquals("com.neverads.boggle.feature.Path", ::Path.returnType.toString())
    }

    @Test
    fun hasParameters() {
        val expectedParams = listOf(
            Parameter(index = 0, name = "coordinates", type = "kotlin.Array<com.neverads.boggle.feature.Coordinate>")
        )
        val actualParams = mutableListOf<Parameter>()
        ::Path.parameters.forEach {
            actualParams.add(
                Parameter(
                    index = it.index,
                    isOptional = it.isOptional,
                    isVararg = it.isVararg,
                    name = it.name,
                    type = it.type.toString()
                )
            )
        }
        assertEquals(expectedParams.size, actualParams.size)
        assertEquals(expectedParams, actualParams)
    }

    @Test(expected = IllegalArgumentException::class)
    fun badUsageNoCoordinates() {
        Path(emptyArray())
    }

    @Test(expected = IllegalArgumentException::class)
    fun badUsageDuplicateCoordinates() {
        Path(arrayOf(coordinate11, coordinate11))
    }

    @Test(expected = IllegalArgumentException::class)
    fun badUsageNonAdjacentCoordinates0() {
        Path(arrayOf(coordinate11, coordinate13))
    }

    @Test(expected = IllegalArgumentException::class)
    fun badUsageNonAdjacentCoordinates1() {
        Path(arrayOf(coordinate01, coordinate11, coordinate22, coordinate00))
    }

    @Test
    fun usage() {
        val shortPath = Path(arrayOf(coordinate00))
        assert(shortPath.coordinates[0] == coordinate00)
        assert(shortPath.coordinates[0] == Coordinate(0, 0))
        assert(shortPath[0] == coordinate00)

        val longPath = Path(arrayOf(coordinate22, coordinate11, coordinate01, coordinate00, coordinate10, coordinate21))
        assert(longPath[0] == coordinate22)
        assert(longPath[1] == coordinate11)
        assert(longPath[2] == coordinate01)
        assert(longPath[3] == coordinate00)
        assert(longPath[4] == coordinate10)
        assert(longPath[5] == coordinate21)
    }
}


class DataClassCoordinate {
    @Test
    fun type() {
        assertEquals("com.neverads.boggle.feature.Coordinate", ::Coordinate.returnType.toString())
    }

    @Test
    fun hasParameters() {
        val expectedParams = listOf(
            Parameter(index = 0, name = "rowIndex", type = "kotlin.Int"),
            Parameter(index = 1, name = "columnIndex", type = "kotlin.Int")
        )
        val actualParams = mutableListOf<Parameter>()
        ::Coordinate.parameters.forEach {
            actualParams.add(
                Parameter(
                    index = it.index,
                    isOptional = it.isOptional,
                    isVararg = it.isVararg,
                    name = it.name,
                    type = it.type.toString()
                )
            )
        }
        assertEquals(expectedParams.size, actualParams.size)
        assertEquals(expectedParams, actualParams)
    }

    @Test
    fun usage() {
        var coordinate = Coordinate(0, 0)
        assertEquals(0, coordinate.rowIndex)
        assertEquals(0, coordinate.columnIndex)

        coordinate = Coordinate(42, 99)
        assertEquals(42, coordinate.rowIndex)
        assertEquals(99, coordinate.columnIndex)
    }
}


class DataClassWord {
    @Test
    fun type() {
        assertEquals("com.neverads.boggle.feature.Word", ::Word.returnType.toString())
    }

    @Test
    fun hasParameters() {
        val expectedParams = listOf(
            Parameter(index = 0, name = "text", type = "kotlin.String"),
            Parameter(index = 1, name = "isCounted", type = "kotlin.Boolean", isOptional = true),
            Parameter(index = 2, name = "isMissed", type = "kotlin.Boolean", isOptional = true)
        )
        val actualParams = mutableListOf<Parameter>()
        ::Word.parameters.forEach {
            actualParams.add(
                Parameter(
                    index = it.index,
                    isOptional = it.isOptional,
                    isVararg = it.isVararg,
                    name = it.name,
                    type = it.type.toString()
                )
            )
        }
        assertEquals(expectedParams.size, actualParams.size)
        assertEquals(expectedParams, actualParams)
    }

    @Test
    fun create() {
        val one = Word("one")
        assertEquals("one", one.text)
        assertEquals(1, one.pointsWithClassicScoring)
        assertEquals(1, one.pointsWithSimplifiedScoring)
        assertEquals(true, one.isCounted)

        val twos = Word("twos")
        assertEquals("twos", twos.text)
        assertEquals(1, twos.pointsWithClassicScoring)
        assertEquals(2, twos.pointsWithSimplifiedScoring)
        assertEquals(true, twos.isCounted)

        val three = Word("three")
        assertEquals("three", three.text)
        assertEquals(2, three.pointsWithClassicScoring)
        assertEquals(3, three.pointsWithSimplifiedScoring)
        assertEquals(true, three.isCounted)

        val others = Word("others")
        assertEquals("others", others.text)
        assertEquals(3, others.pointsWithClassicScoring)
        assertEquals(4, others.pointsWithSimplifiedScoring)
        assertEquals(true, others.isCounted)

        val forests = Word("forests")
        assertEquals("forests", forests.text)
        assertEquals(5, forests.pointsWithClassicScoring)
        assertEquals(5, forests.pointsWithSimplifiedScoring)
        assertEquals(true, forests.isCounted)

        val forester = Word("forester")
        assertEquals("forester", forester.text)
        assertEquals(11, forester.pointsWithClassicScoring)
        assertEquals(6, forester.pointsWithSimplifiedScoring)
        assertEquals(true, forester.isCounted)
    }

    @Test
    fun toggleIsCounted() {
        val toggleWord = Word("toggle")
        assertTrue(toggleWord.isCounted)
        assertEquals(3, toggleWord.pointsWithClassicScoring)
        assertEquals(4, toggleWord.pointsWithSimplifiedScoring)
        toggleWord.isCounted = toggleWord.isCounted.not()
        assertFalse(toggleWord.isCounted)
        assertEquals(0, toggleWord.pointsWithClassicScoring)
        assertEquals(0, toggleWord.pointsWithSimplifiedScoring)
    }
}


class FunctionGetTotalPoints {
    private val function = List<Word>::getTotalPoints

    @Test
    fun hasName() {
        assertEquals("getTotalPoints", function.name)
    }

    @Test
    fun hasParameters() {
        val expectedParams = listOf(
            Parameter(index = 0, name = "null", type = "kotlin.collections.List<com.neverads.boggle.feature.Word>"),
            Parameter(index = 1, name = "scoringStyle", type = "com.neverads.boggle.feature.ScoringStyles")
        )
        val actualParams = mutableListOf<Parameter>()
        function.parameters.forEach {
            actualParams.add(
                Parameter(
                    index = it.index,
                    isOptional = it.isOptional,
                    isVararg = it.isVararg,
                    name = it.name,
                    type = it.type.toString()
                )
            )
        }
        assertEquals(expectedParams.size, actualParams.size)
        assertEquals(expectedParams.toString(), actualParams.toString())
    }

    @Test
    fun returnType() {
        assertEquals("kotlin.Int", function.returnType.toString())
    }

    @Test
    fun usageWithEmptyWordsList() {
        assertEquals(0, emptyList<Word>().getTotalPoints(ScoringStyles.SIMPLIFIED))
        assertEquals(0, emptyList<Word>().getTotalPoints(ScoringStyles.CLASSIC))
    }

    @Test
    fun usageWithOneWord() {
        assertEquals(1, listOf(Word("one")).getTotalPoints(ScoringStyles.CLASSIC))
        assertEquals(1, listOf(Word("ones")).getTotalPoints(ScoringStyles.CLASSIC))
        assertEquals(2, listOf(Word("three")).getTotalPoints(ScoringStyles.CLASSIC))
        assertEquals(3, listOf(Word("threes")).getTotalPoints(ScoringStyles.CLASSIC))
        assertEquals(5, listOf(Word("seventh")).getTotalPoints(ScoringStyles.CLASSIC))
        assertEquals(11, listOf(Word("forester")).getTotalPoints(ScoringStyles.CLASSIC))
        assertEquals(11, listOf(Word("foresters")).getTotalPoints(ScoringStyles.CLASSIC))

        assertEquals(1, listOf(Word("one")).getTotalPoints(ScoringStyles.SIMPLIFIED))
        assertEquals(2, listOf(Word("ones")).getTotalPoints(ScoringStyles.SIMPLIFIED))
        assertEquals(3, listOf(Word("three")).getTotalPoints(ScoringStyles.SIMPLIFIED))
        assertEquals(4, listOf(Word("threes")).getTotalPoints(ScoringStyles.SIMPLIFIED))
        assertEquals(5, listOf(Word("seventh")).getTotalPoints(ScoringStyles.SIMPLIFIED))
        assertEquals(6, listOf(Word("forester")).getTotalPoints(ScoringStyles.SIMPLIFIED))
        assertEquals(7, listOf(Word("foresters")).getTotalPoints(ScoringStyles.SIMPLIFIED))
    }

    @Test
    fun usageWithMultipleWords() {
        assertEquals(3, listOf(Word("two"), Word("words")).getTotalPoints(ScoringStyles.CLASSIC))
        assertEquals(4, listOf(Word("queen"), Word("queer")).getTotalPoints(ScoringStyles.CLASSIC))
        val lotsOfWords = listOf(Word("for"), Word("fore"), Word("forest"), Word("forester"))
        assertEquals(16, lotsOfWords.getTotalPoints(ScoringStyles.CLASSIC))

        assertEquals(4, listOf(Word("two"), Word("words")).getTotalPoints(ScoringStyles.SIMPLIFIED))
        assertEquals(6, listOf(Word("queen"), Word("queer")).getTotalPoints(ScoringStyles.SIMPLIFIED))
        assertEquals(13, lotsOfWords.getTotalPoints(ScoringStyles.SIMPLIFIED))
    }
}


class FunctionGetAllTexts {
    private val function = List<Word>::getAllTexts

    @Test
    fun hasName() {
        assertEquals("getAllTexts", function.name)
    }

    @Test
    fun hasParameters() {
        val expectedParams = listOf(
            Parameter(index = 0, name = "null", type = "kotlin.collections.List<com.neverads.boggle.feature.Word>")
        )
        val actualParams = mutableListOf<Parameter>()
        function.parameters.forEach {
            actualParams.add(
                Parameter(
                    index = it.index,
                    isOptional = it.isOptional,
                    isVararg = it.isVararg,
                    name = it.name,
                    type = it.type.toString()
                )
            )
        }
        assertEquals(expectedParams.size, actualParams.size)
        assertEquals(expectedParams.toString(), actualParams.toString())
    }

    @Test
    fun returnType() {
        assertEquals("kotlin.collections.List<kotlin.String>", function.returnType.toString())
    }

    @Test
    fun usage() {
        assertEquals(emptyList<String>(), emptyList<Word>().getAllTexts())
        assertEquals(listOf("one"), listOf(Word("one")).getAllTexts())
        assertEquals(
            listOf("for", "fore", "forest", "forester"),
            listOf(Word("for"), Word("fore"), Word("forest"), Word("forester")).getAllTexts()
        )
    }
}


class FunctionIsGameCode {
    private val function = String::isGameCode

    @Test
    fun hasName() {
        assertEquals("isGameCode", function.name)
    }

    @Test
    fun hasParameters() {
        val expectedParams = listOf(
            Parameter(index = 0, name = "null", type = "kotlin.String")
        )
        val actualParams = mutableListOf<Parameter>()
        function.parameters.forEach {
            actualParams.add(
                Parameter(
                    index = it.index,
                    isOptional = it.isOptional,
                    isVararg = it.isVararg,
                    name = it.name,
                    type = it.type.toString()
                )
            )
        }
        assertEquals(expectedParams.size, actualParams.size)
        assertEquals(expectedParams.toString(), actualParams.toString())
    }

    @Test
    fun returnType() {
        assertEquals("kotlin.Boolean", function.returnType.toString())
    }

    @Test
    fun usage() {
        assertFalse("".isGameCode())
        assertFalse(" ".isGameCode())
        assertFalse("   ".isGameCode())
        assertFalse("55!!!".isGameCode())
        assertFalse("fff".isGameCode())
        assertFalse(("\nffff").isGameCode())
        assertFalse(("fCfCf").isGameCode())
        assertFalse(("\tfffff   ").isGameCode())

        assertTrue(("fCfCfC").isGameCode())
        assertTrue(("\tffffff   ").isGameCode())
    }
}


class FunctionGetPathsThroughDiceGrid {
    private val diceModel = DiceModel()
    private val function = diceModel::getPathsThroughDiceGrid
    private val gridTwoByTwo = arrayOf(
        arrayOf(coordinate00, coordinate01),
        arrayOf(coordinate10, coordinate11)
    )
    private val expectedPathsFrom00and01 = arrayOf(
        Path(arrayOf(coordinate00, coordinate01, coordinate10)),
        Path(arrayOf(coordinate00, coordinate01, coordinate10, coordinate11)),
        Path(arrayOf(coordinate00, coordinate01, coordinate11)),
        Path(arrayOf(coordinate00, coordinate01, coordinate11, coordinate10)),
        Path(arrayOf(coordinate00, coordinate10, coordinate01)),
        Path(arrayOf(coordinate00, coordinate10, coordinate01, coordinate11)),
        Path(arrayOf(coordinate00, coordinate10, coordinate11)),
        Path(arrayOf(coordinate00, coordinate10, coordinate11, coordinate01)),
        Path(arrayOf(coordinate00, coordinate11, coordinate01)),
        Path(arrayOf(coordinate00, coordinate11, coordinate01, coordinate10)),
        Path(arrayOf(coordinate00, coordinate11, coordinate10)),
        Path(arrayOf(coordinate00, coordinate11, coordinate10, coordinate01)),
        Path(arrayOf(coordinate01, coordinate00, coordinate10)),
        Path(arrayOf(coordinate01, coordinate00, coordinate10, coordinate11)),
        Path(arrayOf(coordinate01, coordinate00, coordinate11)),
        Path(arrayOf(coordinate01, coordinate00, coordinate11, coordinate10)),
        Path(arrayOf(coordinate01, coordinate10, coordinate00)),
        Path(arrayOf(coordinate01, coordinate10, coordinate00, coordinate11)),
        Path(arrayOf(coordinate01, coordinate10, coordinate11)),
        Path(arrayOf(coordinate01, coordinate10, coordinate11, coordinate00)),
        Path(arrayOf(coordinate01, coordinate11, coordinate00)),
        Path(arrayOf(coordinate01, coordinate11, coordinate00, coordinate10)),
        Path(arrayOf(coordinate01, coordinate11, coordinate10)),
        Path(arrayOf(coordinate01, coordinate11, coordinate10, coordinate00))
    )

    @Test
    fun hasName() {
        assertEquals("getPathsThroughDiceGrid", function.name)
    }

    @Test
    fun hasParameters() {
        val expectedParams = listOf(
            Parameter(index = 0, name = "coordinatesGrid", type = "kotlin.Array<kotlin.Array<com.neverads.boggle.feature.Coordinate>>", isOptional = false),
            Parameter(index = 1, name = "rowIndex", type = "kotlin.Int", isOptional = true),
            Parameter(index = 2, name = "columnIndex", type = "kotlin.Int", isOptional = true),
            Parameter(index = 3, name = "minPathSize", type = "kotlin.Int", isOptional = true),
            Parameter(index = 4, name = "maxPathSize", type = "kotlin.Int", isOptional = true),
            Parameter(index = 5, name = "path", type = "kotlin.collections.MutableList<com.neverads.boggle.feature.Coordinate>", isOptional = true),
            Parameter(index = 6, name = "paths", type = "kotlin.collections.MutableList<com.neverads.boggle.feature.Path>", isOptional = true)
        )
        val actualParams = mutableListOf<Parameter>()
        diceModel::getPathsThroughDiceGrid.parameters.forEach {
            actualParams.add(
                Parameter(index = it.index, isOptional = it.isOptional, isVararg = it.isVararg, name = it.name, type = it.type.toString())
            )
        }
        assertEquals(expectedParams.size, actualParams.size)
        assertEquals(expectedParams.toString(), actualParams.toString())
    }

    @Test
    fun returnType() {
        assertEquals("kotlin.collections.MutableList<com.neverads.boggle.feature.Path>", function.returnType.toString())
    }

    @Test(expected = IllegalArgumentException::class)
    fun badUsageGridEmpty() {
        diceModel.getPathsThroughDiceGrid(coordinatesGrid = emptyArray())
    }

    @Test(expected = IllegalArgumentException::class)
    fun badUsageGridNotSquare0() {
        val gridWithOneCell = arrayOf(arrayOf(coordinate00, coordinate01))
        diceModel.getPathsThroughDiceGrid(coordinatesGrid = gridWithOneCell)
    }

    @Test(expected = IllegalArgumentException::class)
    fun badUsageGridNotSquare1() {
        val gridNotSquare = arrayOf(arrayOf(coordinate00), arrayOf(coordinate10, coordinate11))
        diceModel.getPathsThroughDiceGrid(coordinatesGrid = gridNotSquare)
    }

    @Test(expected = IllegalArgumentException::class)
    fun badUsageGridNotSquare2() {
        val gridNotSquare = arrayOf(arrayOf(coordinate00, coordinate01), arrayOf(coordinate10))
        diceModel.getPathsThroughDiceGrid(coordinatesGrid = gridNotSquare)
    }

    @Test
    fun usageGridOneByOne() {
        val gridOneByOne = arrayOf(arrayOf(coordinate00))
        val actualPaths = diceModel.getPathsThroughDiceGrid(gridOneByOne, minPathSize = 1)
        val expectedPaths = mutableListOf(Path(arrayOf(coordinate00)))
        assert(expectedPaths.size == actualPaths.size) {
            "Must have the same number of expected (${expectedPaths.size}) and actual (${actualPaths.size}) paths."
        }
        expectedPaths.forEachIndexed { index, expectedPath ->
            val actualPath = actualPaths[index]
            assert(expectedPath.coordinates.contentDeepEquals(actualPath.coordinates)) {
                "\nexpected-path: [${expectedPath.coordinates.joinToString()}]" +
                        "\n  actual-path: [${actualPath.coordinates.joinToString()}]"
            }
        }
    }

    @Test
    fun usageGridTwoByTwo() {
        val minPathSizes = 1..4
        val expectedPathsFrom01 = arrayOf(
            Path(arrayOf(coordinate01)),
            Path(arrayOf(coordinate01, coordinate00)),
            Path(arrayOf(coordinate01, coordinate00, coordinate10)),
            Path(arrayOf(coordinate01, coordinate00, coordinate10, coordinate11)),
            Path(arrayOf(coordinate01, coordinate00, coordinate11)),
            Path(arrayOf(coordinate01, coordinate00, coordinate11, coordinate10)),
            Path(arrayOf(coordinate01, coordinate10)),
            Path(arrayOf(coordinate01, coordinate10, coordinate00)),
            Path(arrayOf(coordinate01, coordinate10, coordinate00, coordinate11)),
            Path(arrayOf(coordinate01, coordinate10, coordinate11)),
            Path(arrayOf(coordinate01, coordinate10, coordinate11, coordinate00)),
            Path(arrayOf(coordinate01, coordinate11)),
            Path(arrayOf(coordinate01, coordinate11, coordinate00)),
            Path(arrayOf(coordinate01, coordinate11, coordinate00, coordinate10)),
            Path(arrayOf(coordinate01, coordinate11, coordinate10)),
            Path(arrayOf(coordinate01, coordinate11, coordinate10, coordinate00))
        )
        minPathSizes.forEach { minPathSize ->
            val actualPaths = diceModel.getPathsThroughDiceGrid(
                coordinatesGrid = gridTwoByTwo, minPathSize = minPathSize, rowIndex = 0, columnIndex = 1
            )
            val relevantExpectedPaths = expectedPathsFrom01.filter {
                it.size >= minPathSize
            }
            assert(relevantExpectedPaths.size == actualPaths.size) {
                "Must have the same number of expected (${relevantExpectedPaths.size}) and actual (${actualPaths.size}) paths."
            }
            actualPaths.forEachIndexed { index, actualPath ->
                val expectedPath = relevantExpectedPaths[index]
                assert(expectedPath.coordinates.contentDeepEquals(actualPath.coordinates)) {
                    "\nexpected-path: [${expectedPath.coordinates.joinToString()}]" +
                            "\n  actual-path: [${actualPath.coordinates.joinToString()}]"
                }
            }
        }
    }

    @Test
    fun usageRealisticMultipleStartingCoordinates() {
        val relevantCoordinates = arrayOf(coordinate00, coordinate01)
        val actualPaths = mutableListOf<Path>()
        relevantCoordinates.forEach { coordinate ->
            actualPaths.addAll(diceModel.getPathsThroughDiceGrid(gridTwoByTwo, coordinate.rowIndex, coordinate.columnIndex))
        }
        assert(expectedPathsFrom00and01.size == actualPaths.size) {
            "Must have the same number of expected (${expectedPathsFrom00and01.size}) and actual (${actualPaths.size}) paths."
        }
        actualPaths.forEachIndexed { index, actualPath ->
            val expectedPath = expectedPathsFrom00and01[index]
            assert(expectedPath.coordinates.contentDeepEquals(actualPath.coordinates)) {
                "\nexpected-path: [${expectedPath.coordinates.joinToString()}]" +
                        "\n  actual-path: [${actualPath.coordinates.joinToString()}]"
            }
        }
    }

    @Test
    fun usageMaxPathSize() {
        val relevantCoordinates = arrayOf(coordinate00, coordinate01)
        (3..4).forEach { maxPathSize ->
            val actualPaths = mutableListOf<Path>()
            relevantCoordinates.forEach { coordinate ->
                actualPaths.addAll(diceModel.getPathsThroughDiceGrid(gridTwoByTwo, coordinate.rowIndex, coordinate.columnIndex, maxPathSize = maxPathSize))
            }
            val expectedPaths = expectedPathsFrom00and01.filter { it.size <= maxPathSize }
            assert(expectedPaths.size == actualPaths.size) {
                "Must have the same number of expected (${expectedPaths.size}) and actual (${actualPaths.size}) paths."
            }
            actualPaths.forEachIndexed { index, actualPath ->
                val expectedPath = expectedPaths[index]
                assert(expectedPath.coordinates.contentDeepEquals(actualPath.coordinates)) {
                    "\nexpected-path: [${expectedPath.coordinates.joinToString()}]" +
                            "\n  actual-path: [${actualPath.coordinates.joinToString()}]"
                }
            }
        }
    }

    @Test
    fun usageMinAndMaxPathSizes() {
        data class TestData(val pathSize: Int, val expectedPathsCount: Int)
        val testData = arrayOf(
            TestData(pathSize = 0, expectedPathsCount = 0),
            TestData(pathSize = 1, expectedPathsCount = 1),
            TestData(pathSize = 2, expectedPathsCount = 3),
            TestData(pathSize = 3, expectedPathsCount = 15),
            TestData(pathSize = 4, expectedPathsCount = 75),
            TestData(pathSize = 5, expectedPathsCount = 322),
            TestData(pathSize = 6, expectedPathsCount = 1184),
            TestData(pathSize = 7, expectedPathsCount = 3814)
        )

        testData.forEach { _testData ->
            val paths = diceModel.getPathsThroughDiceGrid(coordinates4x4, minPathSize = _testData.pathSize, maxPathSize = _testData.pathSize)
            assert(_testData.expectedPathsCount == paths.size) { "Paths-count:\nexpected: ${_testData.expectedPathsCount}\n  actual: ${paths.size}" }
        }
    }

    @Test  // This test has been used throughout development to check how long it takes to get all paths through a 4x4 grid.
    fun usagePracticalMaxPathSizes() {
        val paths = mutableListOf<Path>()
        coordinates4x4.forEach { row ->
            row.forEach { coordinate ->
//                paths.addAll(  // paths_count = 408
//                    diceModel.getPathsThroughDiceGrid(coordinates4x4, coordinate.rowIndex, coordinate.columnIndex, maxPathSize = 3)  // .01, .01, .01
//                )
//                paths.addAll(  // paths_count = 2172
//                    diceModel.getPathsThroughDiceGrid(coordinates4x4, coordinate.rowIndex, coordinate.columnIndex, maxPathSize = 4)  // .02, .02, .02
//                )
//                paths.addAll(  // paths_count = 8884
//                    diceModel.getPathsThroughDiceGrid(coordinates4x4, coordinate.rowIndex, coordinate.columnIndex, maxPathSize = 5)  // .03, .03, .02
//                )
                paths.addAll(  // paths_count = 31556
                    diceModel.getPathsThroughDiceGrid(coordinates4x4, coordinate.rowIndex, coordinate.columnIndex, maxPathSize = 6)  // .07, .05, .06, .06
                )
//                paths.addAll(  // paths_count = 99828
//                    diceModel.getPathsThroughDiceGrid(coordinates4x4, coordinate.rowIndex, coordinate.columnIndex, maxPathSize = 7)  // .15, .14, .13, .13
//                )
//                paths.addAll(  // paths_count = 283300
//                    diceModel.getPathsThroughDiceGrid(coordinates4x4, coordinate.rowIndex, coordinate.columnIndex, maxPathSize = 8)  // .20, .22, .21, .22
//                )
//                paths.addAll(  // paths_count = 720284
//                    diceModel.getPathsThroughDiceGrid(coordinates4x4, coordinate.rowIndex, coordinate.columnIndex, maxPathSize = 9)  // .56, .43, .41, .41
//                )
//                paths.addAll(  // paths_count = 1626060
//                    diceModel.getPathsThroughDiceGrid(coordinates4x4, coordinate.rowIndex, coordinate.columnIndex, maxPathSize = 10)  // .89, 1.5, .88, .91
//                )
//                paths.addAll(  // paths_count = 3220708
//                    diceModel.getPathsThroughDiceGrid(coordinates4x4, coordinate.rowIndex, coordinate.columnIndex, maxPathSize = 11)  // 1.9, 1.7, 1.7, 1.8
//                )
//                paths.addAll(  // paths_count = 5530972
//                    diceModel.getPathsThroughDiceGrid(coordinates4x4, coordinate.rowIndex, coordinate.columnIndex, maxPathSize = 12)  // 5.5, 5.3, 5.0, 5.9
//                )
//                paths.addAll(  // paths_count = 8175492
//                    diceModel.getPathsThroughDiceGrid(coordinates4x4, coordinate.rowIndex, coordinate.columnIndex, maxPathSize = 13)  // 9.6, 9.9, 10.7, 10.2
//                )
//                paths.addAll(  // paths_count = 10425684
//                    diceModel.getPathsThroughDiceGrid(coordinates4x4, coordinate.rowIndex, coordinate.columnIndex, maxPathSize = 14)  // 11.7, 11.1, 11.4
//                )
//                paths.addAll(  // paths_count = 11686356
//                    diceModel.getPathsThroughDiceGrid(coordinates4x4, coordinate.rowIndex, coordinate.columnIndex, maxPathSize = 15)  // 12.0, 11.2, 12.3
//                )
//                paths.addAll(  // paths_count = 12029540
//                    diceModel.getPathsThroughDiceGrid(coordinates4x4, coordinate.rowIndex, coordinate.columnIndex, maxPathSize = 16)  // 11.7, 11.6, 12.2
//                )
            }
        }
        assert(paths.size == 31_556) { "If maxPathSize is 6, number of paths should be 31,556 -- was actually '${paths.size}'." }
    }
}


class FunctionGetRequiredCommonWordsCountFromGameCode {
    private val multiPlayerCodeActivity = MultiPlayerCodeActivity()
    private val function = multiPlayerCodeActivity::getRequiredCommonWordsCountFromGameCode

    @Test
    fun hasName() {
        assertEquals("getRequiredCommonWordsCountFromGameCode", function.name)
    }

    @Test
    fun hasParameters() {
        val expectedParams = listOf(
            Parameter(index = 0, name = "code", type = "kotlin.String")
        )
        val actualParams = mutableListOf<Parameter>()
        function.parameters.forEach {
            actualParams.add(
                Parameter(
                    index = it.index,
                    isOptional = it.isOptional,
                    isVararg = it.isVararg,
                    name = it.name,
                    type = it.type.toString()
                )
            )
        }
        assertEquals(expectedParams.size, actualParams.size)
        assertEquals(expectedParams.toString(), actualParams.toString())
    }

    @Test
    fun returnType() {
        assertEquals("kotlin.Int", function.returnType.toString())
    }

    @Test
    fun usage() {
        assertEquals(0, multiPlayerCodeActivity.getRequiredCommonWordsCountFromGameCode("A".repeat(6)))
        assertEquals(1, multiPlayerCodeActivity.getRequiredCommonWordsCountFromGameCode("B".repeat(6)))
        assertEquals(2, multiPlayerCodeActivity.getRequiredCommonWordsCountFromGameCode("C".repeat(6)))
        assertEquals(4, multiPlayerCodeActivity.getRequiredCommonWordsCountFromGameCode("E".repeat(6)))
        assertEquals(25, multiPlayerCodeActivity.getRequiredCommonWordsCountFromGameCode("Z".repeat(6)))
    }
}


class FunctionGetTimerSecondsFromGameCode {
    private val mainActivity = MainActivity()
    private val function = mainActivity::getTimerSecondsFromGameCode

    @Test
    fun hasName() {
        assertEquals("getTimerSecondsFromGameCode", function.name)
    }

    @Test
    fun hasParameters() {
        val expectedParams = listOf(
            Parameter(index = 0, name = "code", type = "kotlin.String", isOptional = true)
        )
        val actualParams = mutableListOf<Parameter>()
        function.parameters.forEach {
            actualParams.add(
                Parameter(
                    index = it.index,
                    isOptional = it.isOptional,
                    isVararg = it.isVararg,
                    name = it.name,
                    type = it.type.toString()
                )
            )
        }
        assertEquals(expectedParams.size, actualParams.size)
        assertEquals(expectedParams.toString(), actualParams.toString())
    }

    @Test
    fun returnType() {
        assertEquals("kotlin.Int", function.returnType.toString())
    }

    @Test
    fun usage() {
        assertEquals(15, mainActivity.getTimerSecondsFromGameCode("B".repeat(6)))
        assertEquals(30, mainActivity.getTimerSecondsFromGameCode("C".repeat(6)))
        assertEquals(60, mainActivity.getTimerSecondsFromGameCode("E".repeat(6)))
        assertEquals(15, mainActivity.getTimerSecondsFromGameCode("b".repeat(6)))
        assertEquals(15, mainActivity.getTimerSecondsFromGameCode("a".repeat(6)))
        assertEquals(5 * 60, mainActivity.getTimerSecondsFromGameCode("Z".repeat(6)))
    }
}


class FunctionSortByPoints {
    private val function = List<Word>::sortByPossiblePoints

    @Test
    fun hasName() {
        assertEquals("sortByPossiblePoints", function.name)
    }

    @Test
    fun hasParameters() {
        val expectedParams = listOf(
            Parameter(index = 0, name = "null", type = "kotlin.collections.List<com.neverads.boggle.feature.Word>")
        )
        val actualParams = mutableListOf<Parameter>()
        function.parameters.forEach {
            actualParams.add(
                Parameter(
                    index = it.index,
                    isOptional = it.isOptional,
                    isVararg = it.isVararg,
                    name = it.name,
                    type = it.type.toString()
                )
            )
        }
        assertEquals(expectedParams.size, actualParams.size)
        assertEquals(expectedParams.toString(), actualParams.toString())
    }

    @Test
    fun returnType() {
        assertEquals(
            "kotlin.collections.List<com.neverads.boggle.feature.Word>",
            function.returnType.toString()
        )
    }

    @Test
    fun usage() {
        assertEquals(emptyList<Word>(), emptyList<Word>().sortByPossiblePoints())
        assertEquals(listOf(Word("one")), listOf(Word("one")).sortByPossiblePoints())
        assertEquals(
            listOf(Word("for"), Word("fore"), Word("forest"), Word("forester")),
            listOf(Word("for"), Word("fore"), Word("forest"), Word("forester")).sortByPossiblePoints()
        )
        assertEquals(
            listOf(Word("for"), Word("fore"), Word("forest"), Word("forester")),
            listOf(Word("forester"), Word("forest"), Word("for"), Word("fore")).sortByPossiblePoints()
        )
        assertEquals(
            listOf(Word("aaa"), Word("aab"), Word("aba"), Word("aaaa")),
            listOf(Word("aab"), Word("aaaa"), Word("aba"), Word("aaa")).sortByPossiblePoints()
        )
    }
}


class FunctionSetTexts {
    private val function = Set<Die>::setTexts

    @Test
    fun hasName() {
        assertEquals("setTexts", function.name)
    }

    @Test
    fun hasParameters() {
        val expectedParams = listOf(
            Parameter(index = 0, name = "null", type = "kotlin.collections.Set<com.neverads.boggle.feature.Die>"),
            Parameter(index = 1, name = "chars", type = "kotlin.CharArray")
        )
        val actualParams = mutableListOf<Parameter>()
        function.parameters.forEach {
            actualParams.add(
                Parameter(
                    index = it.index,
                    isOptional = it.isOptional,
                    isVararg = it.isVararg,
                    name = it.name,
                    type = it.type.toString()
                )
            )
        }
        assertEquals(expectedParams.size, actualParams.size)
        assertEquals(expectedParams.toString(), actualParams.toString())
    }

    @Test
    fun returnType() {
        assertEquals("kotlin.Unit", function.returnType.toString())
    }

    @Test(expected = IllegalArgumentException::class)
    fun badUsage() {
        emptySet<Die>().setTexts(chars = charArrayOf('A'))
    }

    @Test
    fun usage() {
        assertEquals("kotlin.Unit", emptySet<Die>().setTexts(chars = charArrayOf()).toString())
    }
}


class FunctionGenerateGameCode {
    private val mainActivity = MainActivity()
    private val function = mainActivity::generateGameCode

    @Test
    fun hasName() {
        assertEquals("generateGameCode", function.name)
    }

    @Test
    fun hasParameters() {
        val expectedParams = listOf(
            Parameter(index = 0, name = "timerSeconds", type = "kotlin.Int", isOptional = true),
            Parameter(index = 1, name = "requiredCommonWordsCount", type = "kotlin.Int", isOptional = true),
            Parameter(index = 2, name = "dice", type = "kotlin.Array<com.neverads.boggle.feature._Die>", isOptional = true)
        )
        val actualParams = mutableListOf<Parameter>()
        function.parameters.forEach {
            actualParams.add(
                Parameter(
                    index = it.index,
                    isOptional = it.isOptional,
                    isVararg = it.isVararg,
                    name = it.name,
                    type = it.type.toString()
                )
            )
        }
        assertEquals(expectedParams.size, actualParams.size)
        assertEquals(expectedParams.toString(), actualParams.toString())
    }

    @Test
    fun returnType() {
        assertEquals("kotlin.String", function.returnType.toString())
    }

    @Test(expected = IllegalArgumentException::class)
    fun badUsageZeroDice() {
        mainActivity.generateGameCode(0, 0, arrayOf())
    }

    @Test(expected = IllegalArgumentException::class)
    fun badUsageOneDie() {
        mainActivity.generateGameCode(0, 0, arrayOf(_Die(listOf('a'), initialIndex = -1)))
    }

    @Test(expected = IllegalArgumentException::class)
    fun badUsageAlmostEnoughDice() {
        mainActivity.generateGameCode(
            0, 0, arrayOf(
                _Die(listOf('a'), initialIndex = -1), _Die(listOf('a'), initialIndex = -1),
                _Die(listOf('a'), initialIndex = -1), _Die(listOf('a'), initialIndex = -1),
                _Die(listOf('a'), initialIndex = -1), _Die(listOf('a'), initialIndex = -1),
                _Die(listOf('a'), initialIndex = -1), _Die(listOf('a'), initialIndex = -1),
                _Die(listOf('a'), initialIndex = -1), _Die(listOf('a'), initialIndex = -1),
                _Die(listOf('a'), initialIndex = -1), _Die(listOf('a'), initialIndex = -1),
                _Die(listOf('a'), initialIndex = -1), _Die(listOf('a'), initialIndex = -1),
                _Die(listOf('a'), initialIndex = -1)
            )
        )
    }

    @Test(expected = IllegalArgumentException::class)
    fun badUsageTooManyDice() {
        mainActivity.generateGameCode(
            0, 0, arrayOf(
                _Die(listOf('a'), initialIndex = -1), _Die(listOf('a'), initialIndex = -1),
                _Die(listOf('a'), initialIndex = -1), _Die(listOf('a'), initialIndex = -1),
                _Die(listOf('a'), initialIndex = -1), _Die(listOf('a'), initialIndex = -1),
                _Die(listOf('a'), initialIndex = -1), _Die(listOf('a'), initialIndex = -1),
                _Die(listOf('a'), initialIndex = -1), _Die(listOf('a'), initialIndex = -1),
                _Die(listOf('a'), initialIndex = -1), _Die(listOf('a'), initialIndex = -1),
                _Die(listOf('a'), initialIndex = -1), _Die(listOf('a'), initialIndex = -1),
                _Die(listOf('a'), initialIndex = -1), _Die(listOf('a'), initialIndex = -1),
                _Die(listOf('a'), initialIndex = -1)
            )
        )
    }

    @Test
    fun usage() {
        mainActivity.diceModel.dice.forEach { it.currentSide = 0 }
        assertEquals("AAAAAA", mainActivity.generateGameCode(0, 0))
        assertEquals("AAAAAB", mainActivity.generateGameCode(0, 1))
        assertEquals("AAAAAZ", mainActivity.generateGameCode(0, 25))
        assertEquals("AAAAAJ", mainActivity.generateGameCode(0, defaultRequiredCommonWordsCount))
        assertEquals("ABBBBJ", mainActivity.generateGameCode(0, 9))  // explicit value of default
        assertEquals("BAAAAJ", mainActivity.generateGameCode(15, defaultRequiredCommonWordsCount))
        assertEquals("UAAAAJ", mainActivity.generateGameCode(900, defaultRequiredCommonWordsCount))

        mainActivity.diceModel.dice.forEach { it.currentSide = 1 }
        assertEquals("AEEEEJ", mainActivity.generateGameCode(0, defaultRequiredCommonWordsCount))

        mainActivity.diceModel.dice.forEach { it.currentSide = 2 }
        assertEquals("AIIIIJ", mainActivity.generateGameCode(0, defaultRequiredCommonWordsCount))

        mainActivity.diceModel.dice.forEach { it.currentSide = 3 }
        assertEquals("AMMMMJ", mainActivity.generateGameCode(0, defaultRequiredCommonWordsCount))
    }
}


class FunctionGetLetters {
    private val diceModel = DiceModel()
    private val function = diceModel.dice::getLetters

    @Test
    fun hasName() {
        assertEquals("getLetters", function.name)
    }

    @Test
    fun hasParameters() {
        val expectedParams = emptyList<Parameter>()
        val actualParams = mutableListOf<Parameter>()
        function.parameters.forEach {
            actualParams.add(
                Parameter(
                    index = it.index,
                    isOptional = it.isOptional,
                    isVararg = it.isVararg,
                    name = it.name,
                    type = it.type.toString()
                )
            )
        }
        assertEquals(expectedParams.size, actualParams.size)
        assertEquals(expectedParams.toString(), actualParams.toString())
    }

    @Test
    fun returnType() {
        assertEquals("kotlin.CharArray", function.returnType.toString())
    }

    @Test
    fun usageWithRandomSidesOfDice() {
        for (i in 1..10) {
            val newDiceModel = DiceModel()
            val randomLetters = newDiceModel.dice.getLetters()
            assertEquals(16, randomLetters.size)
            randomLetters.forEach { assert(it.isUpperCase()) }
        }
    }
}


class FunctionGetLetterCombinations {
    private val diceModel = DiceModel()
    private val function = diceModel::getLetterCombinations

    @Test
    fun hasName() {
        assertEquals("getLetterCombinations", function.name)
    }

    @Test
    fun hasParameters() {
        val expectedParams = listOf(
            Parameter(index = 0, name = "paths", type = "kotlin.collections.List<com.neverads.boggle.feature.Path>"),
            Parameter(index = 1, name = "letters", type = "kotlin.Array<kotlin.CharArray>", isOptional = true)
        )
        val actualParams = mutableListOf<Parameter>()
        diceModel::getLetterCombinations.parameters.forEach {
            actualParams.add(Parameter(index = it.index, isOptional = it.isOptional, isVararg = it.isVararg, name = it.name, type = it.type.toString()))
        }
        assertEquals(expectedParams.size, actualParams.size)
        assertEquals(expectedParams.toString(), actualParams.toString())
    }

    @Test
    fun returnType() {
        assertEquals("kotlin.collections.Set<kotlin.String>", function.returnType.toString())
    }

    @Test
    fun usage2x2() {
        val coordinatesGrid2x2 = arrayOf(
            arrayOf(coordinate00, coordinate01),
            arrayOf(coordinate10, coordinate11)
        )
        val letters = arrayOf(
            charArrayOf('d', 'd'),
            charArrayOf('a', 'd')
        )
        val expected = setOf("add", "addd", "dad", "dadd", "dda", "ddad", "ddd", "ddda")
        val paths2x2 = mutableListOf<Path>()
        coordinatesGrid2x2.forEach { row ->
            row.forEach { coordinate ->
                paths2x2.addAll(diceModel.getPathsThroughDiceGrid(coordinatesGrid2x2, coordinate.rowIndex, coordinate.columnIndex))
            }
        }
        val actual = diceModel.getLetterCombinations(paths2x2, letters)
        val failureMessage = "with letters '${letters.contentDeepToString()}':\n" +
                "expected combinations: '${expected.joinToString()}'\n" +
                "  actual combinations: '${actual.joinToString()}'"
        assert(expected.size == actual.size) { failureMessage }
        assert(expected.containsAll(actual)) { failureMessage }
    }

    @Test
    fun usage3x3() {
        val coordinatesGrid3x3 = arrayOf(
            arrayOf(coordinate00, coordinate01, coordinate02),
            arrayOf(coordinate10, coordinate11, coordinate12),
            arrayOf(coordinate20, coordinate21, coordinate22)
        )
        val letters = arrayOf(
            charArrayOf('a', 'a', 'a'),
            charArrayOf('a', 'A', 'a'),
            charArrayOf('a', 'A', 'a')
        )
        val expected = setOf("aaa", "aaaa", "aaaaa", "aaaaaa", "aaaaaaa", "aaaaaaaa", "aaaaaaaaa")
        val paths3x3 = mutableListOf<Path>()
        coordinatesGrid3x3.forEach { row ->
            row.forEach { coordinate ->
                paths3x3.addAll(diceModel.getPathsThroughDiceGrid(coordinatesGrid3x3, coordinate.rowIndex, coordinate.columnIndex))
            }
        }
        val actual = diceModel.getLetterCombinations(paths3x3, letters)
        val failureMessage = "with letters '${letters.contentDeepToString()}':\n" +
                "expected combinations: '${expected.joinToString()}'\n" +
                "  actual combinations: '${actual.joinToString()}'"
        assert(expected.size == actual.size) { failureMessage }
        assert(expected.containsAll(actual)) { failureMessage }
    }
}


class FunctionGetPercentComplete {
    private val mainActivity = MainActivity()
    private val function = mainActivity::getPercentComplete

    @Test
    fun hasName() {
        assertEquals("getPercentComplete", function.name)
    }

    @Test
    fun hasParameters() {
        val expectedParams = listOf(
            Parameter(index = 0, name = "remainingSeconds", type = "kotlin.Int", isOptional = true),
            Parameter(index = 1, name = "maxSeconds", type = "kotlin.Int", isOptional = true)
        )
        val actualParams = mutableListOf<Parameter>()
        function.parameters.forEach {
            actualParams.add(
                Parameter(
                    index = it.index,
                    isOptional = it.isOptional,
                    isVararg = it.isVararg,
                    name = it.name,
                    type = it.type.toString()
                )
            )
        }
        assertEquals(expectedParams.size, actualParams.size)
        assertEquals(expectedParams.toString(), actualParams.toString())
    }

    @Test
    fun returnType() {
        assertEquals("kotlin.Float", function.returnType.toString())
    }

    @Test(expected = IllegalArgumentException::class)
    fun badUsage() {
        mainActivity.getPercentComplete(1, 0)
    }

    @Test
    fun usage() {
        assertEquals(1.0f, mainActivity.getPercentComplete(1, 1), 0.00001f)
        assertEquals(2.0f, mainActivity.getPercentComplete(0, 1), 0.00001f)
        assertEquals(1.0f, mainActivity.getPercentComplete(1, 2), 0.00001f)
        assertEquals(0.4f, mainActivity.getPercentComplete(4, 5), 0.00001f)
        assertEquals(0.96f, mainActivity.getPercentComplete(4, 75), 0.00001f)
        assertEquals(0.6666666f, mainActivity.getPercentComplete(3, 6), 0.00001f)
        assertEquals(0.3333333f, mainActivity.getPercentComplete(5, 6), 0.00001f)
        assertEquals(0.7647059f, mainActivity.getPercentComplete(5, 17), 0.00001f)
        assertEquals(0.3333333f, mainActivity.getPercentComplete(5, 6), 0.00001f)
        assertEquals(0.01111114f, mainActivity.getPercentComplete(), 0.00001f)
    }
}


class FunctionGetGameCodeCharForTimer {
    private val function = MainActivity::getGameCodeCharForTimer
    private val mainActivity = MainActivity()

    @Test
    fun hasName() {
        assertEquals("getGameCodeCharForTimer", function.name)
    }

    @Test
    fun hasParameters() {
        val expectedParams = listOf(
            Parameter(index = 0, name = "null", type = "com.neverads.boggle.feature.MainActivity"),
            Parameter(
                index = 1,
                name = "timerSeconds",
                type = "kotlin.Int",
                isOptional = true
            )
        )
        val actualParams = mutableListOf<Parameter>()
        function.parameters.forEach {
            actualParams.add(
                Parameter(
                    index = it.index,
                    isOptional = it.isOptional,
                    isVararg = it.isVararg,
                    name = it.name,
                    type = it.type.toString()
                )
            )
        }
        assertEquals(expectedParams.size, actualParams.size)
        assertEquals(expectedParams.toString(), actualParams.toString())
    }

    @Test
    fun returnType() {
        assertEquals("kotlin.Char", function.returnType.toString())
    }

    @Test
    fun usage() {
        assertEquals('A', mainActivity.getGameCodeCharForTimer(timerSeconds = 0))
        assertEquals('B', mainActivity.getGameCodeCharForTimer(timerSeconds = 1 * mainActivity.timerIncrement))
        assertEquals('M', mainActivity.getGameCodeCharForTimer(timerSeconds = 12 * mainActivity.timerIncrement))
        assertEquals('A', mainActivity.getGameCodeCharForTimer(timerSeconds = -1 * mainActivity.timerIncrement))
        assertEquals('A', mainActivity.getGameCodeCharForTimer(timerSeconds = -27 * mainActivity.timerIncrement))
        assertEquals('U', mainActivity.getGameCodeCharForTimer(timerSeconds = 21 * mainActivity.timerIncrement))
        assertEquals('U', mainActivity.getGameCodeCharForTimer(timerSeconds = 50 * mainActivity.timerIncrement))
    }
}


class FunctionGetAllPathsThroughDiceGrid4x4 {
    private val diceModel = DiceModel()

    @Test
    fun hasName() {
        assertEquals("getAllPathsThroughDiceGrid4x4", diceModel::getAllPathsThroughDiceGrid4x4.name)
    }

    @Test
    fun hasParameters() {
        val expectedParams = listOf(
            Parameter(index = 0, name = "minPathSize", type = "kotlin.Int", isOptional = true),
            Parameter(index = 1, name = "maxPathSize", type = "kotlin.Int", isOptional = true)
        )
        val actualParams = mutableListOf<Parameter>()
        diceModel::getAllPathsThroughDiceGrid4x4.parameters.forEach {
            actualParams.add(
                Parameter(it.index, it.isOptional, it.isVararg, it.name, it.type.toString())
            )
        }
        assertEquals(expectedParams.size, actualParams.size)
        assertEquals(expectedParams.toString(), actualParams.toString())
    }

    @Test
    fun returnType() {
        assertEquals("kotlin.collections.MutableList<com.neverads.boggle.feature.Path>", diceModel::getAllPathsThroughDiceGrid4x4.returnType.toString())
    }

    @Test(expected = IllegalArgumentException::class)
    fun badUsageMexLessThanMin() {
        diceModel.getAllPathsThroughDiceGrid4x4(minPathSize = 1, maxPathSize = 0)
    }

    @Test(expected = IllegalArgumentException::class)
    fun badNegativeMin() {
        diceModel.getAllPathsThroughDiceGrid4x4(minPathSize = -1, maxPathSize = 0)
    }

    @Test
    fun usage() {
        data class TestData(val pathSize: Int, val expectedPathsCount: Int)
        arrayOf(
            TestData(0, 0),
            TestData(1, 16),
            TestData(2, 84),
            TestData(3, 408),
            TestData(4, 1764),
            TestData(5, 6712)
        ).forEach { testData ->
            val actualPaths = diceModel.getAllPathsThroughDiceGrid4x4(minPathSize = testData.pathSize, maxPathSize = testData.pathSize)
            assert(actualPaths.all { it.size == testData.pathSize })
            assert(testData.expectedPathsCount == actualPaths.size) {
                "Paths-count with path-size '${testData.pathSize}' expected to be '${testData.expectedPathsCount}' -- was actually '${actualPaths.size}'."
            }
        }
    }
}


class FunctionUpdateDice {
    @Test
    fun hasName() {
        assertEquals("updateDice", DiceModel()::updateDice.name)
    }

    @Test
    fun hasParameters() {
        val expectedParams = listOf(
            Parameter(index = 0, name = "gameCode", type = "kotlin.String"),
            Parameter(index = 1, isOptional = true, name = "seed", type = "kotlin.Int")
        )
        val actualParams = mutableListOf<Parameter>()
        DiceModel()::updateDice.parameters.forEach {
            actualParams.add(
                Parameter(it.index, it.isOptional, it.isVararg, it.name, it.type.toString())
            )
        }
        assertEquals(expectedParams.size, actualParams.size)
        assertEquals(expectedParams.toString(), actualParams.toString())
    }

    @Test
    fun returnType() {
        assertEquals("kotlin.Unit", DiceModel()::updateDice.returnType.toString())
    }

    @Test(expected = IllegalArgumentException::class)
    fun badUsageNoGameCode() {
        DiceModel().updateDice("")
    }

    @Test(expected = IllegalArgumentException::class)
    fun badUsageBadGameCode() {
        DiceModel().updateDice("b@d6¢")
    }

    @Test
    fun usageChangesDiceLetters() {
        val diceModel = DiceModel()
        val initialSides = diceModel.dice.getDiceSides()
        val initialLetters = diceModel.dice.getLetters()
        diceModel.updateDice("BBBBBB")
        val updatedSides = diceModel.dice.getDiceSides()
        val updatedLetters = diceModel.dice.getLetters()
        assert(initialSides.contentEquals(updatedSides).not()) {
            "expected initial (${initialSides.joinToString()}) to not equal updated (${updatedSides.joinToString()})"
        }
        assert(initialLetters.contentEquals(updatedLetters).not()) {
            "expected initial (${initialLetters.joinToString()}) to not equal updated (${updatedLetters.joinToString()})"
        }
    }

    @Test
    fun usageHasGoodAverage() {
        repeat(100) {
            val maxesAndAveragesVariances =
                listOf(Pair(10, 1.2), Pair(100, 0.7), Pair(1000, 0.5))
            var cumulativeValues = doubleArrayOf()
            for (maxAndAverageVariance in maxesAndAveragesVariances) {
                for (i in 1..maxAndAverageVariance.first) {
                    val myMainActivity = MainActivity()
                    val gameCode = myMainActivity.generateGameCode(120, defaultRequiredCommonWordsCount)
                    myMainActivity.diceModel.updateDice(gameCode)
                    cumulativeValues = cumulativeValues.plus(myMainActivity.diceModel.dice.getDiceSides().average())
                }
                assert(abs(2.5 - cumulativeValues.average()) <= maxAndAverageVariance.second) {
                    val maxAttempts = maxAndAverageVariance.first
                    val avg = cumulativeValues.average()
                    val diff = maxAndAverageVariance.second
                    "in $maxAttempts attempts, expected average '$avg' to be within '$diff' of 2.5"
                }
            }
        }
    }

    @Test
    fun usageWithCurrentDiceSidesChangesCorrectly() {
        val mainActivity = MainActivity()
        mainActivity.gameCode = "AAAAAA"
        mainActivity.diceModel.updateDice(mainActivity.gameCode, 0)
        var expectedDiceSides = intArrayOf(3, 0, 0, 5, 2, 5, 4, 5, 1, 4, 0, 4, 3, 5, 1, 0)
        assert(expectedDiceSides.contentEquals(mainActivity.diceModel.dice.getDiceSides())) {
            "expected [${expectedDiceSides.joinToString()}] != " +
                    "actual [${mainActivity.diceModel.dice.getDiceSides().joinToString()}]"
        }
        var expectedOrderByInitialSides = intArrayOf(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15)
        assert(expectedOrderByInitialSides.contentEquals(mainActivity.diceModel.dice.getInitialIndices())) {
            "expected [${expectedOrderByInitialSides.joinToString()}] != " +
                    "actual [${mainActivity.diceModel.dice.getInitialIndices().joinToString()}]"
        }
        var expectedLetters = "EAASOURYINEVSYIH"
        assert(expectedLetters.toCharArray().contentEquals(mainActivity.diceModel.dice.getLetters())) {
            "expected '$expectedLetters' != actual '${mainActivity.diceModel.dice.getLetters().joinToString("")}'"
        }

        mainActivity.gameCode = mainActivity.generateGameCode(requiredCommonWordsCount = defaultRequiredCommonWordsCount)
        var expectedGameCode = "GIQJJJ"
        assert(expectedGameCode == mainActivity.gameCode) {
            "expected '$expectedGameCode' != actual '${mainActivity.gameCode}'"
        }
        mainActivity.diceModel.updateDice(mainActivity.gameCode, 0)
        expectedDiceSides = intArrayOf(1, 3, 2, 4, 0, 2, 0, 2, 4, 2, 3, 0, 4, 0, 4, 1)
        assert(expectedDiceSides.contentEquals(mainActivity.diceModel.dice.getDiceSides())) {
            "expected [${expectedDiceSides.joinToString()}] != " +
                    "actual [${mainActivity.diceModel.dice.getDiceSides().joinToString()}]"
        }
        expectedOrderByInitialSides = intArrayOf(14, 0, 7, 15, 2, 3, 10, 5, 6, 13, 8, 1, 9, 11, 4, 12)
        assert(expectedOrderByInitialSides.contentEquals(mainActivity.diceModel.dice.getInitialIndices())) {
            "expected [${expectedOrderByInitialSides.joinToString()}] != " +
                    "actual [${mainActivity.diceModel.dice.getInitialIndices().joinToString()}]"
        }
        expectedLetters = "IELRAFEMRRTANETI"
        assert(expectedLetters.toCharArray().contentEquals(mainActivity.diceModel.dice.getLetters())) {
            "expected '$expectedLetters' != actual '${mainActivity.diceModel.dice.getLetters().joinToString("")}'"
        }

        mainActivity.gameCode = mainActivity.generateGameCode(requiredCommonWordsCount = defaultRequiredCommonWordsCount)
        expectedGameCode = "GFMHIJ"
        assert(expectedGameCode == mainActivity.gameCode) {
            "expected '$expectedGameCode' != actual '${mainActivity.gameCode}'"
        }
        mainActivity.diceModel.updateDice(mainActivity.gameCode, 0)
        expectedDiceSides = intArrayOf(0, 1, 0, 2, 0, 0, 1, 2, 0, 2, 0, 1, 0, 0, 2, 0)
        assert(expectedDiceSides.contentEquals(mainActivity.diceModel.dice.getDiceSides())) {
            "expected [${expectedDiceSides.joinToString()}] != " +
                    "actual [${mainActivity.diceModel.dice.getDiceSides().joinToString()}]"
        }
        expectedOrderByInitialSides = intArrayOf(5, 9, 13, 8, 12, 1, 11, 15, 0, 4, 14, 3, 7, 2, 6, 10)
        assert(expectedOrderByInitialSides.contentEquals(mainActivity.diceModel.dice.getInitialIndices())) {
            "expected [${expectedOrderByInitialSides.joinToString()}] != " +
                    "actual [${mainActivity.diceModel.dice.getInitialIndices().joinToString()}]"
        }
        expectedLetters = "CEESEAHNAOHFDAIE"
        assert(expectedLetters.toCharArray().contentEquals(mainActivity.diceModel.dice.getLetters())) {
            "expected '$expectedLetters' != actual '${mainActivity.diceModel.dice.getLetters().joinToString("")}'"
        }

        mainActivity.gameCode = mainActivity.generateGameCode(requiredCommonWordsCount = defaultRequiredCommonWordsCount)
        expectedGameCode = "GBEECJ"
        assert(expectedGameCode == mainActivity.gameCode) {
            "expected '$expectedGameCode' != actual '${mainActivity.gameCode}'"
        }
        mainActivity.diceModel.updateDice(mainActivity.gameCode, 0)
        expectedDiceSides = intArrayOf(2, 0, 3, 4, 2, 5, 1, 2, 0, 0, 3, 4, 0, 1, 3, 3)
        assert(expectedDiceSides.contentEquals(mainActivity.diceModel.dice.getDiceSides())) {
            "expected [${expectedDiceSides.joinToString()}] != " +
                    "actual [${mainActivity.diceModel.dice.getDiceSides().joinToString()}]"
        }
        expectedOrderByInitialSides = intArrayOf(6, 10, 8, 9, 13, 11, 12, 15, 1, 0, 14, 4, 2, 3, 7, 5)
        assert(expectedOrderByInitialSides.contentEquals(mainActivity.diceModel.dice.getInitialIndices())) {
            "expected [${expectedOrderByInitialSides.joinToString()}] != " +
                    "actual [${mainActivity.diceModel.dice.getInitialIndices().joinToString()}]"
        }
        expectedLetters = "IETNRWINAANTAFRO"
        assert(expectedLetters.toCharArray().contentEquals(mainActivity.diceModel.dice.getLetters())) {
            "expected '$expectedLetters' != actual '${mainActivity.diceModel.dice.getLetters().joinToString("")}'"
        }

        mainActivity.gameCode = mainActivity.generateGameCode(requiredCommonWordsCount = defaultRequiredCommonWordsCount)
        expectedGameCode = "GBMMIJ"
        assert(expectedGameCode == mainActivity.gameCode) {
            "expected '$expectedGameCode' != actual '${mainActivity.gameCode}'"
        }
        mainActivity.diceModel.updateDice(mainActivity.gameCode, 0)
        expectedDiceSides = intArrayOf(2, 4, 0, 0, 3, 5, 0, 5, 2, 0, 2, 1, 1, 5, 2, 2)
        assert(expectedDiceSides.contentEquals(mainActivity.diceModel.dice.getDiceSides())) {
            "expected [${expectedDiceSides.joinToString()}] != " +
                    "actual [${mainActivity.diceModel.dice.getDiceSides().joinToString()}]"
        }
        expectedOrderByInitialSides = intArrayOf(10, 15, 2, 9, 13, 5, 1, 12, 8, 0, 4, 11, 3, 7, 14, 6)
        assert(expectedOrderByInitialSides.contentEquals(mainActivity.diceModel.dice.getInitialIndices())) {
            "expected [${expectedOrderByInitialSides.joinToString()}] != " +
                    "actual [${mainActivity.diceModel.dice.getInitialIndices().joinToString()}]"
        }
        expectedLetters = "IRAETUATSAOHFYMI"
        assert(expectedLetters.toCharArray().contentEquals(mainActivity.diceModel.dice.getLetters())) {
            "expected '$expectedLetters' != actual '${mainActivity.diceModel.dice.getLetters().joinToString("")}'"
        }

        mainActivity.gameCode = mainActivity.generateGameCode(requiredCommonWordsCount = defaultRequiredCommonWordsCount)
        expectedGameCode = "GBOFOJ"
        assert(expectedGameCode == mainActivity.gameCode) {
            "expected '$expectedGameCode' != actual '${mainActivity.gameCode}'"
        }
        mainActivity.diceModel.updateDice(mainActivity.gameCode, 0)
        expectedDiceSides = intArrayOf(0, 2, 4, 0, 0, 0, 4, 1, 3, 4, 2, 4, 4, 5, 4, 1)
        assert(expectedDiceSides.contentEquals(mainActivity.diceModel.dice.getDiceSides())) {
            "expected [${expectedDiceSides.joinToString()}] != " +
                    "actual [${mainActivity.diceModel.dice.getDiceSides().joinToString()}]"
        }
        expectedOrderByInitialSides = intArrayOf(12, 13, 15, 2, 0, 1, 5, 3, 4, 8, 6, 7, 11, 9, 10, 14)
        assert(expectedOrderByInitialSides.contentEquals(mainActivity.diceModel.dice.getInitialIndices())) {
            "expected [${expectedOrderByInitialSides.joinToString()}] != " +
                    "actual [${mainActivity.diceModel.dice.getInitialIndices().joinToString()}]"
        }
        expectedLetters = "ERRAAATFTTIVVWSI"
        assert(expectedLetters.toCharArray().contentEquals(mainActivity.diceModel.dice.getLetters())) {
            "expected '$expectedLetters' != actual '${mainActivity.diceModel.dice.getLetters().joinToString("")}'"
        }

        mainActivity.gameCode = mainActivity.generateGameCode(requiredCommonWordsCount = defaultRequiredCommonWordsCount)
        expectedGameCode = "GBNRHJ"
        assert(expectedGameCode == mainActivity.gameCode) {
            "expected '$expectedGameCode' != actual '${mainActivity.gameCode}'"
        }
        mainActivity.diceModel.updateDice(mainActivity.gameCode, 0)
        expectedDiceSides = intArrayOf(4, 0, 0, 2, 3, 0, 5, 0, 4, 0, 0, 4, 4, 3, 2, 0)
        assert(expectedDiceSides.contentEquals(mainActivity.diceModel.dice.getDiceSides())) {
            "expected [${expectedDiceSides.joinToString()}] != " +
                    "actual [${mainActivity.diceModel.dice.getDiceSides().joinToString()}]"
        }
        expectedOrderByInitialSides = intArrayOf(7, 11, 3, 10, 14, 6, 13, 2, 9, 15, 1, 5, 12, 4, 8, 0)
        assert(expectedOrderByInitialSides.contentEquals(mainActivity.diceModel.dice.getInitialIndices())) {
            "expected [${expectedOrderByInitialSides.joinToString()}] != " +
                    "actual [${mainActivity.diceModel.dice.getInitialIndices().joinToString()}]"
        }
        expectedLetters = "VEAINDYANHATSTSA"
        assert(expectedLetters.toCharArray().contentEquals(mainActivity.diceModel.dice.getLetters())) {
            "expected '$expectedLetters' != actual '${mainActivity.diceModel.dice.getLetters().joinToString("")}'"
        }

        mainActivity.gameCode = mainActivity.generateGameCode(requiredCommonWordsCount = defaultRequiredCommonWordsCount)
        expectedGameCode = "GALIMJ"
        assert(expectedGameCode == mainActivity.gameCode) {
            "expected '$expectedGameCode' != actual '${mainActivity.gameCode}'"
        }
        mainActivity.diceModel.updateDice(mainActivity.gameCode, 0)
        expectedDiceSides = intArrayOf(0, 0, 3, 3, 1, 0, 5, 3, 2, 4, 0, 1, 0, 2, 2, 4)
        assert(expectedDiceSides.contentEquals(mainActivity.diceModel.dice.getDiceSides())) {
            "expected [${expectedDiceSides.joinToString()}] != " +
                    "actual [${mainActivity.diceModel.dice.getDiceSides().joinToString()}]"
        }
        expectedOrderByInitialSides = intArrayOf(15, 2, 13, 0, 5, 1, 3, 8, 4, 6, 11, 7, 9, 14, 10, 12)
        assert(expectedOrderByInitialSides.contentEquals(mainActivity.diceModel.dice.getInitialIndices())) {
            "expected [${expectedOrderByInitialSides.joinToString()}] != " +
                    "actual [${mainActivity.diceModel.dice.getInitialIndices().joinToString()}]"
        }
        expectedLetters = "HATEIASTOREEEMIS"
        assert(expectedLetters.toCharArray().contentEquals(mainActivity.diceModel.dice.getLetters())) {
            "expected '$expectedLetters' != actual '${mainActivity.diceModel.dice.getLetters().joinToString("")}'"
        }

    }

    @Test
    fun usageAsMultiplayer() {
        val player1 = MainActivity()
        val player2 = MainActivity()
        assertNotEquals(player1, player2)

        player1.gameCode = player1.generateGameCode(requiredCommonWordsCount = defaultRequiredCommonWordsCount)
        player2.gameCode = player2.generateGameCode(requiredCommonWordsCount = defaultRequiredCommonWordsCount)
        assertNotEquals(player1.gameCode, player2.gameCode)

        player2.gameCode = player1.gameCode
        assertEquals(player1.gameCode, player2.gameCode)

        repeat(50) {
            player1.diceModel.updateDice(player1.gameCode)
            player2.diceModel.updateDice(player2.gameCode)
            assertEquals(
                player1.diceModel.dice.getLetters().joinToString(""),
                player2.diceModel.dice.getLetters().joinToString("")
            )

            player1.gameCode = player1.generateGameCode(requiredCommonWordsCount = defaultRequiredCommonWordsCount)
            player2.gameCode = player2.generateGameCode(requiredCommonWordsCount = defaultRequiredCommonWordsCount)
            assertEquals(player1.gameCode, player2.gameCode)
        }
    }

    @Test
    fun usageIsPseudoRandom() {
        repeat(100) {
            val allDiceLetters = mutableListOf<String>()
            repeat(365) { outerLoop ->
                val mainActivity = MainActivity()
                repeat(100) {
                    mainActivity.gameCode = mainActivity.generateGameCode(requiredCommonWordsCount = defaultRequiredCommonWordsCount)
                    mainActivity.diceModel.updateDice(gameCode = mainActivity.gameCode, seed = outerLoop)
                    val currentLetters = mainActivity.diceModel.dice.getLetters().joinToString("")
//                    assert(allDiceLetters.contains(currentLetters).not()) {
//                        "duplicate '$currentLetters' found after '${allDiceLetters.size}' loops with game-code '${mainActivity.gameCode}'"
//                    }
                    allDiceLetters.add(currentLetters)
                }
            }
            val distinct = allDiceLetters.distinct()
            val duplicatesCount = allDiceLetters.size - distinct.size
            assert(duplicatesCount == 0) {
                "expected no duplicates, but found '$duplicatesCount' duplicate(s)"
            }
        }
    }
}


class FunctionIsWordInDictionary {
    private val mainActivity = MainActivity()
    private val function = mainActivity::isWordInDictionary

    @Test
    fun hasName() {
        assertEquals("isWordInDictionary", function.name)
    }

    @Test
    fun hasParameters() {
        val expectedParams = listOf(
            Parameter(index = 0, name = "word", type = "kotlin.String")
        )
        val actualParams = mutableListOf<Parameter>()
        function.parameters.forEach {
            actualParams.add(
                Parameter(it.index, it.isOptional, it.isVararg, it.name, it.type.toString())
            )
        }
        assertEquals(expectedParams.size, actualParams.size)
        assertEquals(expectedParams.toString(), actualParams.toString())
    }

    @Test
    fun returnType() {
        assertEquals("kotlin.Boolean", function.returnType.toString())
    }
}


// extensions
class FunctionArrayOfDieGetInitialIndices {
    private var dice = DiceModel().dice
    private val function = dice::getInitialIndices

    @Test
    fun hasName() {
        assertEquals("getInitialIndices", function.name)
    }

    @Test
    fun hasParameters() {
        val expectedParams = emptyList<Parameter>()
        val actualParams = mutableListOf<Parameter>()
        function.parameters.forEach {
            actualParams.add(
                Parameter(
                    index = it.index,
                    isOptional = it.isOptional,
                    isVararg = it.isVararg,
                    name = it.name,
                    type = it.type.toString()
                )
            )
        }
        assertEquals(expectedParams.size, actualParams.size)
        assertEquals(expectedParams.toString(), actualParams.toString())
    }

    @Test
    fun returnType() {
        assertEquals("kotlin.IntArray", function.returnType.toString())
    }

    @Test
    fun usage() {
        val letters = listOf('a', 'b', 'c', 'd', 'e', 'f')
        dice = arrayOf(
            _Die(letters = letters, currentSide = 0, initialIndex = 0),
            _Die(letters = letters, currentSide = 1, initialIndex = 1),
            _Die(letters = letters, currentSide = 2, initialIndex = 2),
            _Die(letters = letters, currentSide = 3, initialIndex = 3)
        )
        assert(dice.getInitialIndices().contentEquals(intArrayOf(0, 1, 2, 3)))

        dice = arrayOf(
            _Die(letters = letters, currentSide = 3, initialIndex = 3),
            _Die(letters = letters, currentSide = 0, initialIndex = 0),
            _Die(letters = letters, currentSide = 2, initialIndex = 2),
            _Die(letters = letters, currentSide = 1, initialIndex = 1)
        )
        assert(dice.getInitialIndices().contentEquals(intArrayOf(3, 0, 2, 1)))
    }
}


class FunctionArrayOfDieGetSum {
    private var dice = DiceModel().dice
    private val function = dice::getSumOfDiceSides

    @Test
    fun hasName() {
        assertEquals("getSumOfDiceSides", function.name)
    }

    @Test
    fun hasParameters() {
        val expectedParams = listOf(
            Parameter(index = 0, name = "indicesOfSides", type = "kotlin.IntArray")
        )
        val actualParams = mutableListOf<Parameter>()
        function.parameters.forEach {
            actualParams.add(
                Parameter(
                    index = it.index,
                    isOptional = it.isOptional,
                    isVararg = it.isVararg,
                    name = it.name,
                    type = it.type.toString()
                )
            )
        }
        assertEquals(expectedParams.size, actualParams.size)
        assertEquals(expectedParams.toString(), actualParams.toString())
    }

    @Test
    fun returnType() {
        assertEquals("kotlin.Int", function.returnType.toString())
    }

    @Test(expected = IllegalArgumentException::class)
    fun badUsageMaxIndex() {
        assertEquals(0, dice.getSumOfDiceSides(intArrayOf(0, 15, 16)))
    }

    @Test(expected = IllegalArgumentException::class)
    fun badUsageMinIndex() {
        assertEquals(0, dice.getSumOfDiceSides(intArrayOf(-1, 0, 15)))
    }

    @Test
    fun usage() {
        val letters = listOf('a', 'b', 'c', 'd', 'e', 'f')
        dice = arrayOf(
            _Die(letters = letters, currentSide = 0, initialIndex = 0),
            _Die(letters = letters, currentSide = 1, initialIndex = 1),
            _Die(letters = letters, currentSide = 2, initialIndex = 2),
            _Die(letters = letters, currentSide = 3, initialIndex = 3)
        )
        assertEquals(0, dice.getSumOfDiceSides(intArrayOf(0)))
        assertEquals(3, dice.getSumOfDiceSides(intArrayOf(0, 1, 2)))
        assertEquals(1, dice.getSumOfDiceSides(intArrayOf(0, 1)))
        assertEquals(0, dice.getSumOfDiceSides(intArrayOf(0)))
        assertEquals(3, dice.getSumOfDiceSides(intArrayOf(1, 2)))
        assertEquals(4, dice.getSumOfDiceSides(intArrayOf(1, 3)))
        assertEquals(6, dice.getSumOfDiceSides(intArrayOf(1, 2, 3)))
    }
}


class FunctionArrayOfDieGetSortedByInitialIndex {
    private var dice = DiceModel().dice
    private val function = dice::getSortedByInitialIndex

    @Test
    fun hasName() {
        assertEquals("getSortedByInitialIndex", function.name)
    }

    @Test
    fun hasParameters() {
        val expectedParams = emptyList<Parameter>()
        val actualParams = mutableListOf<Parameter>()
        function.parameters.forEach {
            actualParams.add(
                Parameter(
                    index = it.index,
                    isOptional = it.isOptional,
                    isVararg = it.isVararg,
                    name = it.name,
                    type = it.type.toString()
                )
            )
        }
        assertEquals(expectedParams.size, actualParams.size)
        assertEquals(expectedParams.toString(), actualParams.toString())
    }

    @Test
    fun returnType() {
        assertEquals("kotlin.Array<com.neverads.boggle.feature._Die>", function.returnType.toString())
    }

    @Test
    fun usage() {
        val testDice = arrayOf(
            _Die(listOf('b', 'b', 'b', 'b', 'b', 'b'), 1, 0),
            _Die(listOf('a', 'a', 'a', 'a', 'a', 'a'), 0, 0)
        )
        val expected = arrayOf(
            _Die(listOf('a', 'a', 'a', 'a', 'a', 'a'), 0, 0),
            _Die(listOf('b', 'b', 'b', 'b', 'b', 'b'), 1, 0)
        )
        assert(testDice.getSortedByInitialIndex().contentEquals(expected))
    }
}


class FunctionIsValidBoggleWord {
    private val function = String::isValidBoggleWord

    @Test
    fun hasName() {
        assertEquals("isValidBoggleWord", function.name)
    }

    @Test
    fun hasParameters() {
        val expectedParams = listOf(
            Parameter(index = 0, name = "null", type = "kotlin.String")
        )
        val actualParams = mutableListOf<Parameter>()
        function.parameters.forEach {
            actualParams.add(
                Parameter(
                    index = it.index,
                    isOptional = it.isOptional,
                    isVararg = it.isVararg,
                    name = it.name,
                    type = it.type.toString()
                )
            )
        }
        assertEquals(expectedParams.size, actualParams.size)
        assertEquals(expectedParams.toString(), actualParams.toString())
    }

    @Test
    fun returnType() {
        assertEquals("kotlin.Boolean", function.returnType.toString())
    }

    @Test
    fun usage() {
        assertFalse("no".isValidBoggleWord())
        assert("yes".isValidBoggleWord())
        assert("aaa".isValidBoggleWord())
        assert("xyz".isValidBoggleWord())
        assert("four".isValidBoggleWord())
        assert("whatchamacallits".isValidBoggleWord())
        assertFalse("seventeenletterss".isValidBoggleWord())
        assert("inconsequentially".isValidBoggleWord())
        assertFalse("f@lse".isValidBoggleWord())
        assertFalse("123^& _:.€¡".isValidBoggleWord())
        assertFalse("Nope".isValidBoggleWord())
    }
}


class FunctionListOfStringsAsDictionaryWords {
    private val function = List<String>::asDictionaryWords
    private val strings = listOf("one", "two", "three")

    @Test
    fun hasName() {
        assertEquals("asDictionaryWords", function.name)
    }

    @Test
    fun hasParameters() {
        val expectedParams = listOf(
            Parameter(index = 0, name = "areCustom", type = "kotlin.Boolean"),
            Parameter(index = 1, name = "removedWords", type = "kotlin.collections.Set<kotlin.String>")
        )
        val actualParams = mutableListOf<Parameter>()
        strings::asDictionaryWords.parameters.forEach {
            actualParams.add(
                Parameter(
                    index = it.index,
                    isOptional = it.isOptional,
                    isVararg = it.isVararg,
                    name = it.name,
                    type = it.type.toString()
                )
            )
        }
        assertEquals(expectedParams.size, actualParams.size)
        assertEquals(expectedParams.toString(), actualParams.toString())
    }

    @Test
    fun returnType() {
        assertEquals(
            "kotlin.collections.List<com.neverads.boggle.feature.DictionaryWord>",
            function.returnType.toString()
        )
    }

    @Test
    fun usageWithoutRemovedWords() {
        val customWords = listOf(
            DictionaryWord("one", isCustom = true),
            DictionaryWord("two", isCustom = true),
            DictionaryWord("three", isCustom = true)
        )
        assert(strings.asDictionaryWords(areCustom = true, removedWords = emptySet()) == customWords)
        val nonCustomWords = listOf(
            DictionaryWord("one", isCustom = false),
            DictionaryWord("two", isCustom = false),
            DictionaryWord("three", isCustom = false)
        )
        assert(strings.asDictionaryWords(areCustom = false, removedWords = emptySet()) == nonCustomWords)
    }

    @Test
    fun usageWithRemovedWords() {
        val dictionaryWords = mutableListOf(
            DictionaryWord("one", isCustom = true, isRemoved = false),
            DictionaryWord("two", isCustom = true, isRemoved = true),
            DictionaryWord("three", isCustom = true, isRemoved = true)
        )
        assert(strings.asDictionaryWords(areCustom = true, removedWords = setOf("two", "three")) == dictionaryWords)
        dictionaryWords[1] = DictionaryWord("two", isCustom = true, isRemoved = false)
        assert(strings.asDictionaryWords(areCustom = true, removedWords = setOf("three")) == dictionaryWords)
    }
}


class FunctionListOfDictionaryWordAsSortedSetOfStrings {
    private val function = List<DictionaryWord>::asSortedSetOfStrings
    private val dictionaryWords = listOf(
        DictionaryWord(word = "star", isCustom = true),
        DictionaryWord(word = "no_star", isCustom = false)
    )

    @Test
    fun hasName() {
        assertEquals("asSortedSetOfStrings", function.name)
    }

    @Test
    fun hasParameters() {
        val expectedParams = emptyList<Parameter>()
        val actualParams = mutableListOf<Parameter>()
        dictionaryWords::asSortedSetOfStrings.parameters.forEach {
            actualParams.add(
                Parameter(
                    index = it.index,
                    isOptional = it.isOptional,
                    isVararg = it.isVararg,
                    name = it.name,
                    type = it.type.toString()
                )
            )
        }
        assertEquals(expectedParams.size, actualParams.size)
        assertEquals(expectedParams.toString(), actualParams.toString())
    }

    @Test
    fun returnType() {
        assertEquals("kotlin.collections.Set<kotlin.String>", function.returnType.toString())
    }

    @Test
    fun usage() {
        assert(dictionaryWords.asSortedSetOfStrings() == setOf("star", "no_star"))
    }
}


class FunctionListOfDictionaryWordContainsWord {
    private val function = List<DictionaryWord>::containsWord
    private val dictionaryWords = listOf(
        DictionaryWord(word = "star", isCustom = true),
        DictionaryWord(word = "no_star", isCustom = false)
    )

    @Test
    fun hasName() {
        assertEquals("containsWord", function.name)
    }

    @Test
    fun hasParameters() {
        val expectedParams = listOf(
            Parameter(
                index = 0,
                name = "word",
                type = "kotlin.String"
            )
        )
        val actualParams = mutableListOf<Parameter>()
        dictionaryWords::containsWord.parameters.forEach {
            actualParams.add(
                Parameter(
                    index = it.index,
                    isOptional = it.isOptional,
                    isVararg = it.isVararg,
                    name = it.name,
                    type = it.type.toString()
                )
            )
        }
        assertEquals(expectedParams.size, actualParams.size)
        assertEquals(expectedParams.toString(), actualParams.toString())
    }

    @Test
    fun returnType() {
        assertEquals("kotlin.Boolean", function.returnType.toString())
    }

    @Test
    fun usage() {
        assert(dictionaryWords.containsWord("star"))
        assert(dictionaryWords.containsWord("no_star"))
        assertFalse(dictionaryWords.containsWord("not_in_list"))
    }
}


class FunctionListOfDictionaryWordIndexOfWord {
    private val function = List<DictionaryWord>::indexOfWord
    private val dictionaryWords = listOf(
        DictionaryWord(word = "star", isCustom = true),
        DictionaryWord(word = "no_star", isCustom = false)
    )

    @Test
    fun hasName() {
        assertEquals("indexOfWord", function.name)
    }

    @Test
    fun hasParameters() {
        val expectedParams = listOf(
            Parameter(
                index = 0,
                name = "word",
                type = "kotlin.String"
            )
        )
        val actualParams = mutableListOf<Parameter>()
        dictionaryWords::indexOfWord.parameters.forEach {
            actualParams.add(
                Parameter(
                    index = it.index,
                    isOptional = it.isOptional,
                    isVararg = it.isVararg,
                    name = it.name,
                    type = it.type.toString()
                )
            )
        }
        assertEquals(expectedParams.size, actualParams.size)
        assertEquals(expectedParams.toString(), actualParams.toString())
    }

    @Test
    fun returnType() {
        assertEquals("kotlin.Int", function.returnType.toString())
    }

    @Test
    fun usage() {
        assert(dictionaryWords.indexOfWord("fail") == -1)
        assert(dictionaryWords.indexOfWord("star") == 0)
        assert(dictionaryWords.indexOfWord("") == -1)
        assert(dictionaryWords.indexOfWord("no_star") == 1)
    }
}


class FunctionListOfDictionaryWordStartingWithLetter {
    private val function = List<DictionaryWord>::startingWithLetter
    private val star = DictionaryWord(word = "star", isCustom = true)
    private val noStar = DictionaryWord(word = "no_star", isCustom = false)
    private val dictionaryWords = listOf(star, noStar)

    @Test
    fun hasName() {
        assertEquals("startingWithLetter", function.name)
    }

    @Test
    fun hasParameters() {
        val expectedParams = listOf(
            Parameter(
                index = 0,
                name = "letter",
                type = "kotlin.Char?"
            )
        )
        val actualParams = mutableListOf<Parameter>()
        dictionaryWords::startingWithLetter.parameters.forEach {
            actualParams.add(
                Parameter(
                    index = it.index,
                    isOptional = it.isOptional,
                    isVararg = it.isVararg,
                    name = it.name,
                    type = it.type.toString()
                )
            )
        }
        assertEquals(expectedParams.size, actualParams.size)
        assertEquals(expectedParams.toString(), actualParams.toString())
    }

    @Test
    fun returnType() {
        assertEquals(
            "kotlin.collections.List<com.neverads.boggle.feature.DictionaryWord>",
            function.returnType.toString()
        )
    }

    @Test
    fun usage() {
        assert(emptyList<DictionaryWord>().startingWithLetter(' ') == emptyList<DictionaryWord>())
        assert(dictionaryWords.startingWithLetter('n') == listOf(noStar))
        assert(dictionaryWords.startingWithLetter('s') == listOf(star))
        val manyStars = listOf(star, star, star, noStar, star, star, noStar)
        assert(manyStars.startingWithLetter('s') == listOf(star, star, star, star, star))
        assert(manyStars.startingWithLetter('n') == listOf(noStar, noStar))
        assert(manyStars.startingWithLetter(' ') == listOf<DictionaryWord>())
        assert(manyStars.startingWithLetter(null) == listOf<DictionaryWord>())
    }
}


class FunctionCharArrayToTwoDimensionalArray {
    private val function = CharArray::toTwoDimensionalArray

    @Test
    fun hasName() {
        assertEquals("toTwoDimensionalArray", function.name)
    }

    @Test
    fun hasParameters() {
        val expectedParams = emptyList<Parameter>()
        val actualParams = mutableListOf<Parameter>()
        charArrayOf()::toTwoDimensionalArray.parameters.forEach {
            actualParams.add(Parameter(index = it.index, isOptional = it.isOptional, isVararg = it.isVararg, name = it.name, type = it.type.toString()))
        }
        assertEquals(expectedParams.size, actualParams.size)
        assertEquals(expectedParams.toString(), actualParams.toString())
    }

    @Test
    fun returnType() {
        assertEquals("kotlin.Array<kotlin.CharArray>", function.returnType.toString())
    }

    @Test
    fun usage() {
        @Suppress("ArrayInDataClass")
        data class TestData(val oneDimensional: CharArray, val twoDimensional: Array<CharArray>)

        val testData = listOf(
            TestData(
                oneDimensional = charArrayOf('a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p'),
                twoDimensional = arrayOf(
                    charArrayOf('a', 'b', 'c', 'd'),
                    charArrayOf('e', 'f', 'g', 'h'),
                    charArrayOf('i', 'j', 'k', 'l'),
                    charArrayOf('m', 'n', 'o', 'p')
                )
            ),
            TestData(
                oneDimensional = charArrayOf('b', 'c', 'd', 'a', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p'),
                twoDimensional = arrayOf(
                    charArrayOf('b', 'c', 'd', 'a'),
                    charArrayOf('e', 'f', 'g', 'h'),
                    charArrayOf('i', 'j', 'k', 'l'),
                    charArrayOf('m', 'n', 'o', 'p')
                )
            )
        )

        testData.forEach { _testData ->
            val expected = _testData.twoDimensional
            val actual = _testData.oneDimensional.toTwoDimensionalArray()
            val failureMessage = "with one-dimensional array: [${_testData.oneDimensional.joinToString()}]\n" +
                    "expected: ${expected.contentDeepToString()}\n" +
                    "  actual: ${actual.contentDeepToString()}"
            assert(expected.contentDeepEquals(actual)) { failureMessage }
        }
    }

    @Test(expected = IllegalArgumentException::class)
    fun badUsageEmpty() {
        charArrayOf().toTwoDimensionalArray()
    }

    @Test(expected = IllegalArgumentException::class)
    fun badUsageSize1() {
        charArrayOf('a').toTwoDimensionalArray()
    }

    @Test(expected = IllegalArgumentException::class)
    fun badUsageSize2() {
        charArrayOf('a', 'b').toTwoDimensionalArray()
    }

    @Test(expected = IllegalArgumentException::class)
    fun badUsageSize3() {
        charArrayOf('a', 'b', 'c').toTwoDimensionalArray()
    }

    @Test(expected = IllegalArgumentException::class)
    fun badUsageSize4() {
        charArrayOf('a', 'b', 'c', 'd').toTwoDimensionalArray()
    }

    @Test(expected = IllegalArgumentException::class)
    fun badUsageSize7() {
        charArrayOf('a', 'b', 'c', 'd', 'e', 'f', 'g').toTwoDimensionalArray()
    }
}
