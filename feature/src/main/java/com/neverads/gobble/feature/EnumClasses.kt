package com.neverads.gobble.feature

enum class WordFinishedMessages {
    DUPLICATE,
    NOT_IN_DICTIONARY,
    SUCCESS,
    TOO_SHORT
}

enum class ScoringStyles {
    CLASSIC,
    SIMPLIFIED
}
