package com.neverads.gobble.feature


class Path(val coordinates: Array<Coordinate>) : List<Coordinate> {
    init {
        require(coordinates.isNotEmpty()) {
            "A path must be made up of one or more coordinates."
        }
        require(coordinates.size == coordinates.distinct().size) {
            "Each coordinate must be unique in path '${this.coordinates.joinToString()}'."
        }
        coordinates.forEachIndexed { index, coordinate ->
            if (index == 0) {
                return@forEachIndexed
            } else {
                val previousCoordinate = coordinates[index - 1]
                require(previousCoordinate.rowIndex - coordinate.rowIndex in -1..1 && previousCoordinate.columnIndex - coordinate.columnIndex in -1..1) {
                    "'$previousCoordinate' (0-index '${index - 1}') & '$coordinate' (0-index '$index') " +
                            "are not adjacent to each other in path '${this.coordinates.joinToString()}'."
                }
            }
        }
    }

    override fun get(index: Int): Coordinate {
        return coordinates[index]
    }

    override val size: Int
        get() = this.coordinates.size

    override fun contains(element: Coordinate): Boolean {
        TODO("not implemented")
    }

    override fun containsAll(elements: Collection<Coordinate>): Boolean {
        TODO("not implemented")
    }

    override fun indexOf(element: Coordinate): Int {
        TODO("not implemented")
    }

    override fun isEmpty(): Boolean {
        TODO("not implemented")
    }

    override fun iterator(): Iterator<Coordinate> {
        TODO("not implemented")
    }

    override fun lastIndexOf(element: Coordinate): Int {
        TODO("not implemented")
    }

    override fun listIterator(): ListIterator<Coordinate> {
        TODO("not implemented")
    }

    override fun listIterator(index: Int): ListIterator<Coordinate> {
        TODO("not implemented")
    }

    override fun subList(fromIndex: Int, toIndex: Int): List<Coordinate> {
        TODO("not implemented")
    }
}
