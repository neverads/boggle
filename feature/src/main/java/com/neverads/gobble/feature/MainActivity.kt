package com.neverads.gobble.feature

import android.animation.ObjectAnimator
import android.annotation.SuppressLint
import android.app.Activity
import android.app.AlertDialog
import android.content.ClipboardManager
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.graphics.Color
import android.graphics.Typeface
import android.net.Uri
import android.os.*
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.view.Menu
import android.view.MenuItem
import android.view.MotionEvent
import android.view.View
import android.view.animation.AccelerateInterpolator
import android.view.animation.DecelerateInterpolator
import android.widget.CheckBox
import com.neverads.boggle.feature.R
import kotlinx.android.synthetic.main.activity_main.*
import java.io.FileNotFoundException
import java.util.*
import kotlin.math.roundToInt


const val request_code_game_code_activity = 42
const val request_code_scoring_activity = 9
const val request_code_settings_activity = 243
const val request_code_other_activity = 6149
const val default_missed_words_max_length = 5
const val default_min_word_length = 3
const val default_should_use_dice_edge_background = true
const val default_dice_text_size = 22
const val default_dice_text_boldness = 1  // medium
const val timerIncrement = 15
const val maxPossibleTimer = 5 * 60
internal var fontMedium: Typeface? = null
var gameCode = ""
var isPaused = false

class MainActivity : AppCompatActivity() {
    val diceModel = DiceModel()
    var previousGameCode = ""
    private var countedWords = mutableListOf<String>()
    private val letterCombinationsInCommonWords = mutableSetOf<String>()
    private val maxTimerPossibilities = intArrayOf(15, 30, 45, 60, 75, 90, 105, 120, 150, 180, 240, 300)
    private val missedWords = mutableListOf<String>()
    private val selectedWords = mutableListOf<String>()
    private val usedGameCodes = mutableListOf<String>()
    private var canDisplaySimplePauseAlert = true
    private var diceViewsWithIndexes = emptySet<Die>()
    private var isGameInProgress = false
    private var isInfiniteTimerMode = false
    private var lines = emptySet<Line>()
    private var maxTimerSeconds = 90
    private var minWordLength = 3
    private var shouldResetLastDieAvailability = false
    private var shouldShowDictionaryWarning = true
    private var shouldShowScoreAlert = false
    private var shouldUseCustomDictionary = true
    private var timerSeconds = maxTimerSeconds
    private var usedDice = mutableListOf<Die>()

    @SuppressLint("ClickableViewAccessibility")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        image_button_info_just_weather.visibility = getJustWeatherInfoButtonVisibility()

        val shouldDisplayStartGameHelp = getPreferenceShouldDisplayStartGameHelp()
        val shortDuration = 1000L
        val longDuration = shortDuration * 2
        if (shouldDisplayStartGameHelp) {
            textViewWordFinishedMessage.animate()
                .alpha(1f)
                .setStartDelay(longDuration)
                .setDuration(longDuration)
                .setInterpolator(DecelerateInterpolator())
                .withEndAction {
                    textViewWordFinishedMessage.animate()
                        .alpha(0f)
                        .setInterpolator(AccelerateInterpolator())
                        .withEndAction {
                            textViewWordFinishedMessage.text = getString(R.string.message_help_start_game)
                            textViewWordFinishedMessage.animate()
                                .alpha(1f)
                                .setStartDelay(0)
                                .setInterpolator(DecelerateInterpolator())
                                .start()
                        }
                        .start()
                }
                .start()
        }

        diceViewsWithIndexes = setOf(
            Die(die_0_0, 0, 0),
            Die(die_0_1, 0, 1),
            Die(die_0_2, 0, 2),
            Die(die_0_3, 0, 3),
            Die(die_1_0, 1, 0),
            Die(die_1_1, 1, 1),
            Die(die_1_2, 1, 2),
            Die(die_1_3, 1, 3),
            Die(die_2_0, 2, 0),
            Die(die_2_1, 2, 1),
            Die(die_2_2, 2, 2),
            Die(die_2_3, 2, 3),
            Die(die_3_0, 3, 0),
            Die(die_3_1, 3, 1),
            Die(die_3_2, 3, 2),
            Die(die_3_3, 3, 3)
        )
        lines = setOf(
            Line(imageViewDiagonalLine_0_0__1_1, diceViewsWithIndexes.getConnectedDice(Pair(0, 0), Pair(1, 1))),
            Line(imageViewDiagonalLine_0_1__1_0, diceViewsWithIndexes.getConnectedDice(Pair(0, 1), Pair(1, 0))),
            Line(imageViewDiagonalLine_0_1__1_2, diceViewsWithIndexes.getConnectedDice(Pair(0, 1), Pair(1, 2))),
            Line(imageViewDiagonalLine_0_2__1_1, diceViewsWithIndexes.getConnectedDice(Pair(0, 2), Pair(1, 1))),
            Line(imageViewDiagonalLine_0_2__1_3, diceViewsWithIndexes.getConnectedDice(Pair(0, 2), Pair(1, 3))),
            Line(imageViewDiagonalLine_0_3__1_2, diceViewsWithIndexes.getConnectedDice(Pair(0, 3), Pair(1, 2))),
            Line(imageViewDiagonalLine_1_0__2_1, diceViewsWithIndexes.getConnectedDice(Pair(1, 0), Pair(2, 1))),
            Line(imageViewDiagonalLine_1_1__2_0, diceViewsWithIndexes.getConnectedDice(Pair(1, 1), Pair(2, 0))),
            Line(imageViewDiagonalLine_1_1__2_2, diceViewsWithIndexes.getConnectedDice(Pair(1, 1), Pair(2, 2))),
            Line(imageViewDiagonalLine_1_2__2_1, diceViewsWithIndexes.getConnectedDice(Pair(1, 2), Pair(2, 1))),
            Line(imageViewDiagonalLine_1_2__2_3, diceViewsWithIndexes.getConnectedDice(Pair(1, 2), Pair(2, 3))),
            Line(imageViewDiagonalLine_1_3__2_2, diceViewsWithIndexes.getConnectedDice(Pair(1, 3), Pair(2, 2))),
            Line(imageViewDiagonalLine_2_0__3_1, diceViewsWithIndexes.getConnectedDice(Pair(2, 0), Pair(3, 1))),
            Line(imageViewDiagonalLine_2_1__3_0, diceViewsWithIndexes.getConnectedDice(Pair(2, 1), Pair(3, 0))),
            Line(imageViewDiagonalLine_2_1__3_2, diceViewsWithIndexes.getConnectedDice(Pair(2, 1), Pair(3, 2))),
            Line(imageViewDiagonalLine_2_2__3_1, diceViewsWithIndexes.getConnectedDice(Pair(2, 2), Pair(3, 1))),
            Line(imageViewDiagonalLine_2_2__3_3, diceViewsWithIndexes.getConnectedDice(Pair(2, 2), Pair(3, 3))),
            Line(imageViewDiagonalLine_2_3__3_2, diceViewsWithIndexes.getConnectedDice(Pair(2, 3), Pair(3, 2))),

            Line(imageViewStraightLine_0_0__0_1, diceViewsWithIndexes.getConnectedDice(Pair(0, 0), Pair(0, 1))),
            Line(imageViewStraightLine_0_1__0_2, diceViewsWithIndexes.getConnectedDice(Pair(0, 1), Pair(0, 2))),
            Line(imageViewStraightLine_0_2__0_3, diceViewsWithIndexes.getConnectedDice(Pair(0, 2), Pair(0, 3))),
            Line(imageViewStraightLine_0_0__1_0, diceViewsWithIndexes.getConnectedDice(Pair(0, 0), Pair(1, 0))),
            Line(imageViewStraightLine_0_1__1_1, diceViewsWithIndexes.getConnectedDice(Pair(0, 1), Pair(1, 1))),
            Line(imageViewStraightLine_0_2__1_2, diceViewsWithIndexes.getConnectedDice(Pair(0, 2), Pair(1, 2))),
            Line(imageViewStraightLine_0_3__1_3, diceViewsWithIndexes.getConnectedDice(Pair(0, 3), Pair(1, 3))),
            Line(imageViewStraightLine_1_0__1_1, diceViewsWithIndexes.getConnectedDice(Pair(1, 0), Pair(1, 1))),
            Line(imageViewStraightLine_1_1__1_2, diceViewsWithIndexes.getConnectedDice(Pair(1, 1), Pair(1, 2))),
            Line(imageViewStraightLine_1_2__1_3, diceViewsWithIndexes.getConnectedDice(Pair(1, 2), Pair(1, 3))),
            Line(imageViewStraightLine_1_0__2_0, diceViewsWithIndexes.getConnectedDice(Pair(1, 0), Pair(2, 0))),
            Line(imageViewStraightLine_1_1__2_1, diceViewsWithIndexes.getConnectedDice(Pair(1, 1), Pair(2, 1))),
            Line(imageViewStraightLine_1_2__2_2, diceViewsWithIndexes.getConnectedDice(Pair(1, 2), Pair(2, 2))),
            Line(imageViewStraightLine_1_3__2_3, diceViewsWithIndexes.getConnectedDice(Pair(1, 3), Pair(2, 3))),
            Line(imageViewStraightLine_2_0__2_1, diceViewsWithIndexes.getConnectedDice(Pair(2, 0), Pair(2, 1))),
            Line(imageViewStraightLine_2_1__2_2, diceViewsWithIndexes.getConnectedDice(Pair(2, 1), Pair(2, 2))),
            Line(imageViewStraightLine_2_2__2_3, diceViewsWithIndexes.getConnectedDice(Pair(2, 2), Pair(2, 3))),
            Line(imageViewStraightLine_2_0__3_0, diceViewsWithIndexes.getConnectedDice(Pair(2, 0), Pair(3, 0))),
            Line(imageViewStraightLine_2_1__3_1, diceViewsWithIndexes.getConnectedDice(Pair(2, 1), Pair(3, 1))),
            Line(imageViewStraightLine_2_2__3_2, diceViewsWithIndexes.getConnectedDice(Pair(2, 2), Pair(3, 2))),
            Line(imageViewStraightLine_2_3__3_3, diceViewsWithIndexes.getConnectedDice(Pair(2, 3), Pair(3, 3))),
            Line(imageViewStraightLine_3_0__3_1, diceViewsWithIndexes.getConnectedDice(Pair(3, 0), Pair(3, 1))),
            Line(imageViewStraightLine_3_1__3_2, diceViewsWithIndexes.getConnectedDice(Pair(3, 1), Pair(3, 2))),
            Line(imageViewStraightLine_3_2__3_3, diceViewsWithIndexes.getConnectedDice(Pair(3, 2), Pair(3, 3)))
        )

        diceGrid.setOnTouchListener { _, event ->
            if (isGameInProgress) {
                if (event.isWithinView(view = diceGrid)) {
                    if (event.action == MotionEvent.ACTION_UP) {
                        processFinishedWord()
                        diceViewsWithIndexes.forEach { it.textView.alpha = 1.0f }
                        diceViewsWithIndexes.setIsAvailable(isAvailable = true)
                        usedDice = mutableListOf()
                        lines.setInvisible()
                    } else if (event.action in listOf(MotionEvent.ACTION_DOWN, MotionEvent.ACTION_MOVE)) {
                        val activeDie = diceViewsWithIndexes.getDieWithEvent(event = event)
                        if (activeDie != null) {
                            val vibrator = this.getSystemService(Context.VIBRATOR_SERVICE) as Vibrator
                            if (usedDice.isEmpty() || (activeDie.isAvailable && activeDie.isAdjacentToDie(usedDice.last()))) {
                                vibrate(VibrationReason.AddLetter, vibrator)
                                activeDie.textView.alpha = 0.5f
                                activeDie.isAvailable = false
                                shouldResetLastDieAvailability = false
                                if (usedDice.isNotEmpty()) {
                                    val color: Int
                                    if (usedDice.size == 1) {
                                        val teal = Color.rgb(30, 133, 119)
                                        color = teal
                                    } else if (usedDice.size == 2) {
                                        val sapphire = Color.rgb(50, 110, 220)
                                        color = sapphire
                                    } else if (usedDice.size == 3) {
                                        val violet = Color.rgb(110, 80, 230)
                                        color = violet
                                    } else if (usedDice.size == 4) {
                                        val purple = Color.rgb(150, 90, 210)
                                        color = purple
                                    } else if (usedDice.size == 5) {
                                        val burgundy = Color.rgb(180, 30, 140)
                                        color = burgundy
                                    } else {
                                        val red = Color.rgb(230, 70, 50)
                                        color = red
                                    }
                                    val lineToConnect = lines.getByConnectingDice(usedDice.last(), activeDie).imageView
                                    lineToConnect.setColorFilter(color)
                                    lineToConnect.visibility = View.VISIBLE
                                }
                                usedDice.add(activeDie)
                            } else if (usedDice.size >= 2 && activeDie != usedDice.last() && usedDice.contains(activeDie)) {
                                vibrate(VibrationReason.AddLetter, vibrator)

                                val indexOfActiveDie = usedDice.indexOf(activeDie)
                                val lastIndex = usedDice.size - 1
                                for (i in lastIndex downTo indexOfActiveDie + 1) {
                                    val die1 = usedDice[i]
                                    val die2 = usedDice[i - 1]
                                    lines.getByConnectingDice(die1, die2).imageView.visibility = View.INVISIBLE
                                    die1.textView.alpha = 1.0f
                                    die1.isAvailable = true
                                    usedDice = usedDice.dropLast(1).toMutableList()
                                }
                            }
                        }
                    }
                } else {
                    diceViewsWithIndexes.forEach { it.textView.alpha = 1.0f }
                    diceViewsWithIndexes.setIsAvailable(isAvailable = true)
                    usedDice = mutableListOf()
                    lines.setInvisible()
                }
            }
            true
        }
        imageButtonTimerUpArrow.setOnClickListener {
            if (maxTimerSeconds < maxPossibleTimer) {
                increaseMaxTimer()
            } else {
                enableInfiniteTimerMode()
            }
        }
        imageButtonTimerDownArrow.setOnClickListener {
            if (isInfiniteTimerMode) {
                disableInfiniteTimerMode()
                setTimerText(maxTimerSeconds)
            } else if (maxTimerSeconds > timerIncrement) {
                decreaseMaxTimer()
            }
        }
        buttonTimer.setOnClickListener {
            incrementTimerButtonTaps()
            if (shouldDisplayStartGameHelp) {
                setPreferenceShouldDisplayStartGameHelpAsFalse()
            }
            if (isGameInProgress) {
                showUnfinishedGameAlertDialog()
            } else if (shouldShowScoreAlert) {
                shouldShowScoreAlert = false
                showScoreAlertDialog()
            } else {
                startNewGameAndSetUpUI()
            }
        }

        fabScoring.setOnClickListener {
            shouldShowScoreAlert = false
            showScoringScreen()
        }
        image_button_info_just_weather.setOnClickListener {
            val checkBoxView = View.inflate(this, R.layout.checkbox, null)
            val checkBox = checkBoxView.findViewById<View>(R.id.checkbox) as CheckBox
            checkBox.text = "Don't show this again"
            checkBox.setOnCheckedChangeListener { _, isChecked ->
                getSharedPreferences(getString(R.string.preferences), Context.MODE_PRIVATE)
                    .edit()
                    .putBoolean(getString(R.string.preference_should_display_just_weather_info), isChecked.not())
                    .apply()
            }

            AlertDialog.Builder(this)
                .setTitle("\uD83C\uDF89 We made another app!")
                .setMessage(
                    "\n(Sorry to interrupt your games, but we honestly thought you might like to know about this.)\n\n" +
                            "It's a simple weather forecast app called \"Just Weather.\"\n\n" +
                            "• Free\n" +
                            "• No ads\n" +
                            "• No data collection\n" +
                            "• Open source\n\n" +
                            "Wanna check it out?"
                )
                .setView(checkBoxView)
                .setNegativeButton("\uD83D\uDE12 nope", null)
                .setPositiveButton("\uD83D\uDE03 sure!") { _, _ ->
                    val intent = Intent()
                    intent.action = Intent.ACTION_VIEW
                    intent.addCategory(Intent.CATEGORY_BROWSABLE)
                    intent.data = Uri.parse(getString(R.string.about_text_link_just_weather))
                    startActivity(intent)
                }
                .setOnDismissListener {
                    image_button_info_just_weather.visibility = getJustWeatherInfoButtonVisibility()
                }
                .create()
                .show()
        }

        vibeConnectLetters = getSharedPreferences(getString(R.string.preferences), Context.MODE_PRIVATE)
            .getInt(getString(R.string.preference_vibe_connect_letters), vibeConnectLettersDefault)
        vibeCompleteAWord = getSharedPreferences(getString(R.string.preferences), Context.MODE_PRIVATE)
            .getInt(getString(R.string.preference_vibe_complete_a_word), vibeCompleteAWordDefault)
        vibeEndOfGame = getSharedPreferences(getString(R.string.preferences), Context.MODE_PRIVATE)
            .getInt(getString(R.string.preference_vibe_end_of_game), vibeEndOfGameDefault)

        maxTimerSeconds = getSharedPreferences(getString(R.string.preferences), Context.MODE_PRIVATE)
            .getInt(getString(R.string.preference_max_timer), maxTimerSeconds)
        setTimerText(maxTimerSeconds)

        if (gameCode.isEmpty()) {
            val delayInMillis: Long = 200
            Thread.sleep(delayInMillis)
            gameCode = generateGameCode()
            Thread.sleep(delayInMillis)
        }

        skipBadGames()
        buttonTimer.isEnabled = true
        buttonTimer.alpha = 1f

        fontMedium = die_0_0.typeface
        diceViewsWithIndexes.setTexts(getString(R.string.title_dice_default).toCharArray())
    }

    override fun onPause() {
        super.onPause()
        if (isGameInProgress) {
            isPaused = true
        }
    }

    override fun onResume() {
        super.onResume()

        val preferences = getSharedPreferences(getString(R.string.preferences), Context.MODE_PRIVATE)

        val hasDiceEdge = preferences.getBoolean(getString(R.string.preference_dice_style_edge), default_should_use_dice_edge_background)
        val textSize = preferences.getInt(getString(R.string.preference_dice_style_text_size), default_dice_text_size)
        val boldness = preferences.getInt(getString(R.string.preference_dice_style_boldness), default_dice_text_boldness)
        val typeface = when (boldness) {
            0 -> Typeface.SANS_SERIF
            2 -> Typeface.DEFAULT_BOLD
            else -> fontMedium
        }
        diceViewsWithIndexes.forEach { die ->
            die.textView.setBackgroundResource(if (hasDiceEdge) R.drawable.dice_background else R.drawable.rounded_rectangle)
            die.textView.textSize = textSize + 8f
            die.textView.typeface = typeface
        }

        minWordLength = preferences.getInt(getString(R.string.preference_min_word_length), default_min_word_length)
        shouldUseCustomDictionary = preferences.getBoolean(getString(R.string.preference_should_use_custom_dictionary), true)
        if (isPaused && canDisplaySimplePauseAlert && isInfiniteTimerMode.not()) {
            AlertDialog.Builder(this)
                .setTitle("Game Paused")
                .setPositiveButton("resume", null)
                .setOnDismissListener { isPaused = false }
                .create()
                .show()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        canDisplaySimplePauseAlert = false
        return when (item.itemId) {
            R.id.action_about -> {
                val aboutIntent = Intent(this, AboutActivity::class.java)
                startActivityForResult(aboutIntent, request_code_other_activity)
                true
            }
            R.id.action_toggle_infinite_mode -> {
                if (isInfiniteTimerMode) {
                    disableInfiniteTimerMode()
                    setTimerText(maxTimerSeconds)
                } else {
                    enableInfiniteTimerMode()
                    buttonTimer.text = "∞"
                }
                true
            }
            R.id.action_high_scores -> {
                isPaused = true
                var charForMatchingTimer = 'G'
                if (previousGameCode.isNotEmpty()) {
                    charForMatchingTimer = previousGameCode.first()
                } else if (buttonTimer.text != "∞") {
                    charForMatchingTimer = getGameCodeCharForTimer(maxTimerSeconds)
                }
                showHighScores(
                    getSharedPreferences(getString(R.string.preferences), Context.MODE_PRIVATE),
                    getString(R.string.preference_high_scores),
                    getString(R.string.preference_is_scoring_style_classic),
                    charForMatchingTimer,
                    getString(R.string.high_scores),
                    this
                )
                true
            }
            R.id.action_how_to -> {
                val howToIntent = Intent(this, HowToActivity::class.java)
                startActivityForResult(howToIntent, request_code_other_activity)
                true
            }
            R.id.action_multi_player -> {
                val multiPlayerIntent = Intent(this, MultiPlayerCodeActivity::class.java)
                multiPlayerIntent.putExtra(getString(R.string.game_code_intent_label), gameCode)
                startActivityForResult(multiPlayerIntent, request_code_game_code_activity)
                true
            }
            R.id.action_your_dictionary -> {
                val yourDictionaryIntent = Intent(this, YourDictionaryActivity::class.java)
                startActivityForResult(yourDictionaryIntent, request_code_other_activity)
                true
            }
            R.id.action_settings -> {
                val settingsIntent = Intent(this, SettingsActivity::class.java)
                settingsIntent.putExtra(getString(R.string.game_code_intent_label), gameCode)
                settingsIntent.putExtra(getString(R.string.is_game_in_progress_intent_label), isGameInProgress)
                startActivityForResult(settingsIntent, request_code_settings_activity)
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode in listOf(request_code_game_code_activity, request_code_other_activity)) {
            if (resultCode == Activity.RESULT_CANCELED) {
                canDisplaySimplePauseAlert = true
            } else if (resultCode == Activity.RESULT_OK) {
                gameCode = data?.getStringExtra(getString(R.string.game_code_intent_label))!!.toUpperCase()
                maxTimerSeconds = getTimerSecondsFromGameCode()
                if (isGameInProgress.not()) {
                    setTimerText(maxTimerSeconds)
                }
                getSharedPreferences(getString(R.string.preferences), Context.MODE_PRIVATE)
                    .edit()
                    .putInt(getString(R.string.preference_max_timer), maxTimerSeconds)
                    .apply()
                if (isGameInProgress) {
                    AlertDialog.Builder(this)
                        .setTitle("Game Paused")
                        .setMessage("Would you like to continue your previous game, or start a new one?")
                        .setPositiveButton("start new") { _, _ -> startNewGameAndSetUpUI() }
                        .setNeutralButton("continue previous", null)
                        .setOnDismissListener {
                            isPaused = false
                            canDisplaySimplePauseAlert = true
                        }
                        .create()
                        .show()
                }
            }
        } else if ((requestCode == request_code_scoring_activity) && (resultCode == Activity.RESULT_CANCELED) && data != null) {
            countedWords = data.getStringArrayExtra(getString(R.string.counted_words_list_intent_label)).toMutableList()
        } else if ((requestCode == request_code_settings_activity) && (resultCode == Activity.RESULT_OK)) {
            canDisplaySimplePauseAlert = true
            val newGameCode = data?.getStringExtra(getString(R.string.game_code_intent_label))!!.toUpperCase()
            val isLessDifficult = newGameCode.last() > gameCode.last()
            gameCode = newGameCode
            if (isLessDifficult) {
                val timerText = buttonTimer.text
                buttonTimer.isEnabled = false
                buttonTimer.alpha = 0.5f
                buttonTimer.text = ""
                progressBarTimer.visibility = View.VISIBLE
                diceModel.updateDice(gameCode)
                val handler = Handler()
                Thread(object : Runnable {
                    override fun run() {
                        skipBadGames()
                        handler.post {
                            run {
                                buttonTimer.isEnabled = true
                                buttonTimer.alpha = 1f
                                progressBarTimer.visibility = View.INVISIBLE
                                buttonTimer.text = timerText
                            }
                        }
                    }
                }).start()
            }
        }
    }

    private fun decrementTimer() {
        timerSeconds--
        setTimerText(timerSeconds)
    }

    private fun processFinishedWord() {
        val vibrator = this.getSystemService(Context.VIBRATOR_SERVICE) as Vibrator
        val currentWord = usedDice.getWord()
        if (currentWord.length < 2 || currentWord == "qu") {
            return
        } else if (currentWord in selectedWords) {
            vibrate(VibrationReason.FinishWordFailure, vibrator)
            displayFadingMessage(WordFinishedMessages.DUPLICATE)
        } else if (currentWord.length < minWordLength) {
            vibrate(VibrationReason.FinishWordFailure, vibrator)
            displayFadingMessage(WordFinishedMessages.TOO_SHORT)
        } else if (isWordInDictionary(currentWord).not()) {
            vibrate(VibrationReason.FinishWordFailure, vibrator)
            displayFadingMessage(WordFinishedMessages.NOT_IN_DICTIONARY)
        } else {
            vibrate(VibrationReason.FinishWordSuccess, vibrator)
            displayFadingMessage(WordFinishedMessages.SUCCESS)
            selectedWords.add(index = 0, element = currentWord)
            if (isInfiniteTimerMode) {
                countedWords.add(currentWord)
            }
            textFieldWordList.text = selectedWords.joinToString("\n")
            scrollForWordList.smoothScrollTo(0, 0)
        }
    }

    private fun displayFadingMessage(message: WordFinishedMessages) {
        val currentWord = usedDice.getWord()
        var imageVisibility: Int = View.GONE
        var textColor: Int = R.color.colorWordFailText
        var duration: Long = 3000
        val text: String

        when (message) {
            WordFinishedMessages.DUPLICATE -> text = getString(R.string.message_finished_word_duplicate, currentWord)
            WordFinishedMessages.TOO_SHORT -> text = "'$currentWord'" + getString(R.string.message_finished_word_too_short, minWordLength)
            WordFinishedMessages.NOT_IN_DICTIONARY -> text = getString(R.string.message_finished_word_not_in_dict, currentWord)
            else -> {
                imageVisibility = View.VISIBLE
                textColor = R.color.colorPrimary
                text = currentWord
                duration = 2000
            }
        }

        imageViewWordFinished.visibility = imageVisibility
        textViewWordFinishedMessage.setTextColor(ContextCompat.getColor(this, textColor))
        textViewWordFinishedMessage.text = text

        imageViewWordFinished.alpha = 1.0f
        textViewWordFinishedMessage.alpha = 1.0f
        arrayOf(imageViewWordFinished, textViewWordFinishedMessage).forEach { view ->
            view.animate()
                .alpha(0.0f)
                .setStartDelay(0)
                .setDuration(duration)
                .setInterpolator(AccelerateInterpolator())
                .start()
        }
    }

    private fun startNewGame(shouldCreateNewTimer: Boolean) {
        isGameInProgress = true
        usedDice = mutableListOf()
        clearWords()
        diceViewsWithIndexes.setTexts(chars = diceModel.dice.getLetters())
        timerSeconds = maxTimerSeconds
        if (isInfiniteTimerMode) {
            buttonTimer.text = "∞"
        } else {
            setTimerText(timerSeconds)
        }
        if (shouldCreateNewTimer) {
            createTimer()
        }
    }

    private fun createTimer() {
        val oneSecondInMillis = 1000L
        Handler().apply {
            val runnable = object : Runnable {
                override fun run() {
                    if (timerSeconds > 0) {
                        if (timerSeconds == 1) {
                            val vibrator = this@MainActivity.getSystemService(Context.VIBRATOR_SERVICE) as Vibrator
                            vibrate(VibrationReason.EndOfGame, vibrator)
                            stopGame()
                        }
                        if (isPaused.not()) {
                            decrementTimer()
                        }
                        animateTimerProgressBar()
                        postDelayed(this, oneSecondInMillis)
                    }
                }
            }
            postDelayed(runnable, oneSecondInMillis)
        }
    }

    private fun clearWords() {
        listOf(selectedWords, countedWords, missedWords).forEach { it.clear() }
        textFieldWordList.text = ""
    }

    private fun decreaseMaxTimer() {
        changeMaxTimer(multiplier = -1)
    }

    private fun increaseMaxTimer() {
        changeMaxTimer(multiplier = 1)
    }

    private fun resetGame() {
        lines.setInvisible()
        usedDice = mutableListOf()
        setIsEnabledForTimerButtons(isEnabled = true)
        isGameInProgress = false
        val shouldDisplayAlert = getSharedPreferences(getString(R.string.preferences), Context.MODE_PRIVATE)
            .getBoolean(getString(R.string.preference_should_display_skip_score_screen_modal), true)
        shouldShowScoreAlert = selectedWords.isNotEmpty() && shouldDisplayAlert
    }

    private fun setIsEnabledForTimerButtons(isEnabled: Boolean) {
        val isVisible = if (isEnabled) View.VISIBLE else View.INVISIBLE
        imageButtonTimerUpArrow.visibility = isVisible
        imageButtonTimerDownArrow.visibility = isVisible
        imageButtonTimerUpArrow.isEnabled = isEnabled
        imageButtonTimerDownArrow.isEnabled = isEnabled
    }

    private fun showScoreAlertDialog() {
        val checkBoxView = View.inflate(this, R.layout.checkbox, null)
        val checkBox = checkBoxView.findViewById<View>(R.id.checkbox) as CheckBox
        checkBox.text = "Don't show this again"
        checkBox.setOnCheckedChangeListener { _, isChecked ->
            getSharedPreferences(getString(R.string.preferences), Context.MODE_PRIVATE)
                .edit()
                .putBoolean(getString(R.string.preference_should_display_skip_score_screen_modal), isChecked.not())
                .apply()
        }

        AlertDialog.Builder(this)
            .setIcon(R.drawable.ic_scoring_black_24dp)
            .setTitle("Are you sure you want to start a new game?")
            .setMessage("\nYou haven't checked the score screen yet.")
            .setView(checkBoxView)
            .setNeutralButton("see score") { _, _ -> showScoringScreen() }
            .setNegativeButton("cancel", null)
            .setPositiveButton("new game") { _, _ -> startNewGameAndSetUpUI() }
            .create()
            .show()
    }

    private fun showUnfinishedGameAlertDialog() {
        isPaused = true
        AlertDialog.Builder(this)
            .setIcon(R.drawable.ic_warning)
            .setTitle("Start a new game, or just stop the current one?")
            .setMessage("Your current game will be lost.")
            .setNegativeButton("cancel", null)
            .setPositiveButton("new game") { _, _ -> startNewGameAndSetUpUI() }
            .setNeutralButton("stop game") { _, _ ->
                timerSeconds = 0
                setTimerText(timerSeconds)
                stopGame()
            }
            .setOnDismissListener { isPaused = false }
            .create()
            .show()
    }

    private fun changeMaxTimer(multiplier: Int) {
        maxTimerSeconds += timerIncrement * multiplier
        while (maxTimerPossibilities.contains(maxTimerSeconds).not()) {
            maxTimerSeconds += timerIncrement * multiplier
        }
        setTimerText(maxTimerSeconds)

        getSharedPreferences(getString(R.string.preferences), Context.MODE_PRIVATE)
            .edit()
            .putInt(getString(R.string.preference_max_timer), maxTimerSeconds)
            .apply()
        gameCode = gameCode.replaceRange(0 until 1, getGameCodeCharForTimer().toString())
    }

    fun generateGameCode(
        timerSeconds: Int = maxTimerSeconds,
        requiredCommonWordsCount: Int = getRequiredCommonWordsCount(),
        dice: Array<_Die> = diceModel.dice
    ): String {
        var newGameCode = ""
        var loopCount = 0
        while (newGameCode.isEmpty() || usedGameCodes.contains(newGameCode)) {
            require(dice.size == 16)
            val timerChar = getGameCodeCharForTimer(timerSeconds)
            val baseChar = 'A' + loopCount
            val row0Sum = baseChar + dice.getSortedByInitialIndex().getDiceSides().copyOfRange(0, 4).sum()
            val row1Sum = baseChar + dice.getSortedByInitialIndex().getDiceSides().copyOfRange(4, 8).sum()
            val row2Sum = baseChar + dice.getSortedByInitialIndex().getDiceSides().copyOfRange(8, 12).sum()
            val row3Sum = baseChar + dice.getSortedByInitialIndex().getDiceSides().copyOfRange(12, 16).sum()
            val difficultyChar = 'A' + requiredCommonWordsCount
            newGameCode = "$timerChar$row0Sum$row1Sum$row2Sum$row3Sum$difficultyChar"
            loopCount++
        }

        val newGameCodeCore = newGameCode.substring(1 until newGameCode.lastIndex)
        val badGameCodeCore = "MLKH"  // tested millions of game-codes, and this is the only bad one
        if (newGameCodeCore == badGameCodeCore) {
            val replacementGameCodeCore = "MLKI"
            newGameCode = newGameCode.replace(newGameCodeCore, replacementGameCodeCore)
        }

        usedGameCodes.add(newGameCode)
        return newGameCode
    }

    private fun startNewGameAndSetUpUI() {
        previousGameCode = gameCode
        textViewWordFinishedMessage.alpha = 0f
        letterCombinationsInCommonWords.clear()
        runOnUiThread {
            val delayInMillis: Long = 50
            Thread.sleep(delayInMillis)
            diceModel.updateDice(gameCode)
            Thread.sleep(delayInMillis)
            gameCode = generateGameCode()
            Thread.sleep(delayInMillis)
            if (isGameInProgress.not()) {
                animateTimerProgressBar(0.0F, 300L)
            }
            startNewGame(shouldCreateNewTimer = isGameInProgress.not() && isInfiniteTimerMode.not())
            diceViewsWithIndexes.setTextColor(color = ContextCompat.getColor(this, R.color.colorNearWhite))
            diceViewsWithIndexes.forEach {
                it.textView.alpha = 1.0f
            }
            if (isInfiniteTimerMode.not()) {
                setIsEnabledForTimerButtons(isEnabled = false)
                fabScoring.hide()
            }
        }

        AsyncTask.execute {
            val maxPathSize = getSharedPreferences(getString(R.string.preferences), Context.MODE_PRIVATE)
                .getInt(getString(R.string.preference_missed_words_max_length), default_missed_words_max_length)
            if (maxPathSize >= 3) {
                if (isGameInProgress) {
                    val pathsThroughGrid4x4 = diceModel.getAllPathsThroughDiceGrid4x4(maxPathSize = maxPathSize)
                    if (isGameInProgress) {
                        val letterCombinations = diceModel.getLetterCombinations(pathsThroughGrid4x4)
                        letterCombinationsInCommonWords.addAll(filterForCommonWords(letterCombinations.toTypedArray(), isSkippable = true))
                    }
                }
            }

            skipBadGames()
        }
    }

    private fun showScoringScreen() {
        if (isInfiniteTimerMode) {
            missedWords.clear()
            missedWords.addAll(letterCombinationsInCommonWords - selectedWords)
        }
        val scoringIntent = Intent(this, ScoringActivity::class.java)
        scoringIntent.putExtra(getString(R.string.selected_words_list_intent_label), selectedWords.toTypedArray())
        scoringIntent.putExtra(getString(R.string.counted_words_list_intent_label), countedWords.toTypedArray())
        scoringIntent.putExtra(getString(R.string.missed_words_list_intent_label), missedWords.toTypedArray())
        val lettersFromUI = diceViewsWithIndexes.map { it.textView.text.first() }.toCharArray()
        scoringIntent.putExtra(getString(R.string.dice_letters_intent_label), lettersFromUI)
        scoringIntent.putExtra(getString(R.string.game_code_intent_label), previousGameCode)
        scoringIntent.putExtra(getString(R.string.is_infinite_mode_intent_label), buttonTimer.text == "∞")
        startActivityForResult(scoringIntent, request_code_scoring_activity)
    }

    fun getGameCodeCharForTimer(timerSeconds: Int = maxTimerSeconds): Char {
        var _timerSeconds = timerSeconds
        if (timerSeconds < 0) {
            _timerSeconds = 0
        } else if (timerSeconds > maxPossibleTimer) {
            _timerSeconds = maxPossibleTimer
        }
        val charIncrement = _timerSeconds / timerIncrement
        return 'A' + charIncrement
    }

    private fun animateTimerProgressBar(newPercent: Float = getPercentComplete(), durationInMillis: Long = 1_200L) {
        ObjectAnimator.ofInt(
            timerProgressBar, getString(R.string.progress), (timerProgressBar.max * newPercent).roundToInt()
        )
            .setDuration(durationInMillis)
            .start()
    }

    fun getPercentComplete(remainingSeconds: Int = timerSeconds, maxSeconds: Int = maxTimerSeconds): Float {
        require(maxSeconds >= remainingSeconds)
        return 1 - ((remainingSeconds.toFloat() - 1) / maxSeconds.toFloat())
    }

    private fun filterForCommonWords(possibleWords: Array<String>, isSkippable: Boolean = false): Set<String> {
        if (possibleWords.isEmpty()) {
            return emptySet()
        }

        val wordsInDictionary = mutableSetOf<String>()
        val fileName = getString(R.string.file_name_common_words)
        val commonWords = mutableListOf<String>()
        try {
            assets.open(fileName).bufferedReader().useLines { lines ->
                commonWords.addAll(lines)
            }
        } catch (e: Exception) {
            return emptySet()
        }
        val possibleWordsFoundInDictionary = possibleWords.filter { possibleWord: String ->
            if (isSkippable && isGameInProgress.not()) {
                false
            } else if (shouldUseCustomDictionary) {
                val preferences = getSharedPreferences(getString(R.string.preferences), Context.MODE_PRIVATE)
                val addedWords = preferences.getStringSet(getString(R.string.preference_custom_dict_added_words), emptySet())!!
                val removedWords = preferences.getStringSet(getString(R.string.preference_custom_dict_removed_words), emptySet())!!
                possibleWord in addedWords || (possibleWord !in removedWords && possibleWord in commonWords)
            } else {
                possibleWord in commonWords
            }
        }
        wordsInDictionary.addAll(possibleWordsFoundInDictionary)
        return wordsInDictionary
    }

    fun isWordInDictionary(word: String): Boolean {
        require(word.isNotEmpty())
        if (word in letterCombinationsInCommonWords) {
            return if (shouldUseCustomDictionary) {
                val removedWords = getSharedPreferences(getString(R.string.preferences), Context.MODE_PRIVATE)
                    .getStringSet(getString(R.string.preference_custom_dict_removed_words), emptySet<String>())!!
                removedWords.contains(word).not()
            } else {
                true
            }
        } else {
            if (shouldUseCustomDictionary) {
                val preferences = getSharedPreferences(getString(R.string.preferences), Context.MODE_PRIVATE)
                val addedWords = preferences.getStringSet(getString(R.string.preference_custom_dict_added_words), emptySet<String>())!!
                if (addedWords.contains(word)) return true
                val removedWords = preferences.getStringSet(getString(R.string.preference_custom_dict_removed_words), emptySet<String>())!!
                if (removedWords.contains(word)) return false
            }
            val fileName = getString(R.string.file_name_dictionary, word[0])
            var isInStandardDict = true
            try {
                assets.open(fileName).bufferedReader().useLines { lines ->
                    isInStandardDict = lines.contains(word)
                }
            } catch (e: FileNotFoundException) {
                if (shouldShowDictionaryWarning) {
                    AlertDialog.Builder(this)
                        .setIcon(R.drawable.ic_warning)
                        .setTitle(R.string.alert_title_dictionary_file_error)
                        .setMessage(getString(R.string.message_dictionary_file_not_found, word[0]))
                        .setPositiveButton("ok", null)
                        .setNeutralButton("don't show again") { _, _ -> shouldShowDictionaryWarning = false }
                        .create()
                        .show()
                }
            } catch (e: Exception) {
                if (shouldShowDictionaryWarning) {
                    AlertDialog.Builder(this)
                        .setIcon(R.drawable.ic_warning)
                        .setTitle(R.string.alert_title_dictionary_file_error)
                        .setMessage(getString(R.string.message_dictionary_file_general_error, e.message))
                        .setPositiveButton("ok", null)
                        .setNeutralButton("don't show again") { _, _ -> shouldShowDictionaryWarning = false }
                        .create()
                        .show()
                }
            }
            return isInStandardDict
        }
    }

    private fun enableInfiniteTimerMode() {
        require(isInfiniteTimerMode.not())
        runOnUiThread {
            isInfiniteTimerMode = true
            imageButtonTimerUpArrow.visibility = View.INVISIBLE
            buttonTimer.isEnabled = false
            buttonTimer.alpha = 0.5f
            startNewGameAndSetUpUI()
            fabScoring.show()
        }
    }

    private fun disableInfiniteTimerMode() {
        require(isInfiniteTimerMode)
        runOnUiThread {
            fabScoring.hide()
            diceViewsWithIndexes.forEach { it.textView.alpha = 0.5f }
            resetGame()
            isPaused = false
            shouldShowScoreAlert = false
            clearWords()
            buttonTimer.alpha = 1f
            buttonTimer.isEnabled = true
            imageButtonTimerUpArrow.visibility = View.VISIBLE
            isInfiniteTimerMode = false
        }
    }

    private fun stopGame() {
        val isClassicScoring = getSharedPreferences(getString(R.string.preferences), Context.MODE_PRIVATE)
            .getBoolean(getString(R.string.preference_is_scoring_style_classic), false)
        val scoring = when {
            isClassicScoring -> ScoringStyles.CLASSIC
            else -> ScoringStyles.SIMPLIFIED
        }
        val totalPoints = selectedWords.map { Word(it) }.getTotalPoints(scoring)
        if (totalPoints > 0) {
            val possibleWords = (selectedWords + letterCombinationsInCommonWords).toSet()
            val commonWordsPoints = possibleWords.map { Word(it) }.getTotalPoints(scoring)
            val scoreSuffix = if (getPreferenceShouldShowPossibleScore()) "  ($commonWordsPoints possible)" else ""
            val totalText = "${getString(R.string.score_total_text)}$totalPoints$scoreSuffix"
            textViewWordFinishedMessage.text = totalText
            imageViewWordFinished.visibility = View.GONE
            textViewWordFinishedMessage.setTextColor(ContextCompat.getColor(this, R.color.colorPrimary))
            textViewWordFinishedMessage.animate()
                .alpha(1f)
                .setDuration(200)
                .setStartDelay(0)
                .setInterpolator(AccelerateInterpolator())
                .withEndAction {
                    textViewWordFinishedMessage.animate()
                        .alpha(0f)
                        .setStartDelay(2000)
                        .setDuration(5000)
                        .start()
                }
                .start()
        }

        countedWords.clear()
        countedWords.addAll(selectedWords)
        missedWords.clear()
        missedWords.addAll(letterCombinationsInCommonWords - selectedWords)
        processHighScores()
        resetGame()
        diceViewsWithIndexes.forEach { it.textView.alpha = 0.5f }
        fabScoring.show()
    }

    private fun setTimerText(newTimerSeconds: Int) {
        buttonTimer.text = getFormattedTimeFromSeconds(newTimerSeconds)
    }

    private fun getPreferenceShouldDisplayStartGameHelp(): Boolean {
        return getSharedPreferences(getString(R.string.preferences), Context.MODE_PRIVATE)
            .getBoolean(getString(R.string.preference_should_display_start_game_help), true)
    }

    private fun getPreferenceShouldShowPossibleScore(): Boolean {
        return getSharedPreferences(getString(R.string.preferences), Context.MODE_PRIVATE)
            .getBoolean(getString(R.string.preference_should_show_possible_score), false)
    }

    private val requiredTaps = 50

    private fun incrementTimerButtonTaps() {
        val prefs = getSharedPreferences(getString(R.string.preferences), Context.MODE_PRIVATE)
        var count = prefs.getInt(getString(R.string.preference_timer_button_taps), 0)
        if (count < requiredTaps) {
            count++
            prefs.edit()
                .putInt(getString(R.string.preference_timer_button_taps), count)
                .apply()
        }
    }

    private fun getJustWeatherInfoButtonVisibility(): Int {
        val prefs = getSharedPreferences(getString(R.string.preferences), Context.MODE_PRIVATE)
        val shouldDisplay = prefs.getBoolean(getString(R.string.preference_should_display_just_weather_info), true)
        val count = prefs.getInt(getString(R.string.preference_timer_button_taps), 0)
        return if (shouldDisplay && count >= requiredTaps) View.VISIBLE else View.GONE
    }

    private fun setPreferenceShouldDisplayStartGameHelpAsFalse() {
        getSharedPreferences(getString(R.string.preferences), Context.MODE_PRIVATE)
            .edit()
            .putBoolean(getString(R.string.preference_should_display_start_game_help), false)
            .apply()
    }

    private fun getRequiredCommonWordsCount(): Int {
        return getSharedPreferences(getString(R.string.preferences), Context.MODE_PRIVATE)
            .getInt(getString(R.string.preference_required_common_words_count), defaultRequiredCommonWordsCount)
    }

    private fun getShouldGameBeSkipped(): Boolean {
        val maxPathForSkipGameEvaluation = 4
        val pathsThroughGrid4x4 = diceModel.getAllPathsThroughDiceGrid4x4(maxPathSize = maxPathForSkipGameEvaluation)
        val letterCombinations = diceModel.getLetterCombinations(pathsThroughGrid4x4)
        val availableCommonWordsLengthFourOrFewer = filterForCommonWords(letterCombinations.toTypedArray())
        return availableCommonWordsLengthFourOrFewer.size <= getRequiredCommonWordsCount()
    }

    private fun skipBadGames() {
        while (getShouldGameBeSkipped()) {
            gameCode = generateGameCode()
            diceModel.updateDice(gameCode)
        }
    }

    private fun processHighScores() {
        val pointsSimplified = selectedWords.map { Word(it) }.getTotalPoints(ScoringStyles.SIMPLIFIED)
        if (pointsSimplified > 0) {
            val pointsClassic = selectedWords.map { Word(it) }.getTotalPoints(ScoringStyles.CLASSIC)
            val prefs = getSharedPreferences(getString(R.string.preferences), Context.MODE_PRIVATE)
            val highScoresAsStrings = prefs.getStringSet(getString(R.string.preference_high_scores), mutableSetOf())!!
            val highScores = highScoresAsStrings.map { HighScore.fromString(it) }.toMutableList()

            val highScore = HighScore(pointsSimplified, pointsClassic, previousGameCode, Calendar.getInstance().timeInMillis)
            highScores.add(highScore)
            val isClassicScoring = prefs.getBoolean(getString(R.string.preference_is_scoring_style_classic), false)
            if (isClassicScoring) {
                highScores.sortByDescending { it.pointsClassic }
            } else {
                highScores.sortByDescending { it.pointsSimplified }
            }
            val maxScoresCount = 20
            if (highScores.size <= maxScoresCount) {
                prefs.edit().putStringSet(getString(R.string.preference_high_scores), highScores.map { it.asPrefString }.toSet()).apply()
            } else {
                prefs.edit()
                    .putStringSet(getString(R.string.preference_high_scores), highScores.subList(0, maxScoresCount).map { it.asPrefString }.toSet())
                    .apply()
            }
        }
    }
}

enum class VibrationReason {
    AddLetter,
    FinishWordSuccess,
    FinishWordFailure,
    EndOfGame
}

fun vibrate(reason: VibrationReason, vibrator: Vibrator) {
    val shortDelay = 90L
    val longDelay = shortDelay * 2
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
        val timings: LongArray
        val amplitudes: IntArray
        if (reason == VibrationReason.AddLetter) {
            timings = longArrayOf(vibeConnectLetters.toLong())
            amplitudes = intArrayOf(vibeConnectLetters)
        } else if (reason == VibrationReason.FinishWordSuccess) {
            timings = longArrayOf(shortDelay, vibeCompleteAWord.toLong() * 3)
            amplitudes = intArrayOf(0, vibeCompleteAWord)
        } else if (reason == VibrationReason.FinishWordFailure) {
            timings = longArrayOf(shortDelay, vibeCompleteAWord.toLong() * 4, longDelay, vibeCompleteAWord.toLong() * 4)
            amplitudes = intArrayOf(0, vibeCompleteAWord, 0, vibeCompleteAWord)
        } else {
            timings =
                longArrayOf(shortDelay, vibeEndOfGame.toLong(), longDelay, vibeEndOfGame.toLong(), longDelay, vibeEndOfGame.toLong())
            amplitudes = intArrayOf(0, vibeEndOfGame, 0, vibeEndOfGame, 0, vibeEndOfGame)
        }
        if (timings.any { timing -> timing != 0L }) {
            val noRepeat = -1
            vibrator.vibrate(VibrationEffect.createWaveform(timings, amplitudes, noRepeat))
        }
    } else {
        val pattern: LongArray
        if (reason == VibrationReason.AddLetter) {
            pattern = longArrayOf(0, vibeConnectLetters.toLong() * 6)
        } else if (reason == VibrationReason.FinishWordSuccess) {
            pattern = longArrayOf(shortDelay, vibeCompleteAWord.toLong() * 8)
        } else if (reason == VibrationReason.FinishWordFailure) {
            val vibe = vibeCompleteAWord.toLong() * 8
            pattern = longArrayOf(shortDelay, vibe, longDelay, vibe)
        } else {
            val vibe = (vibeEndOfGame.toLong() * 2) - 10
            pattern = longArrayOf(shortDelay, vibe, longDelay, vibe, longDelay, vibe)
        }
        @Suppress("DEPRECATION")  // because the if-check ensures we use the updated method if possible
        vibrator.vibrate(pattern, -1)
    }
}

fun getTimerSecondsFromGameCode(code: String = gameCode): Int {
    require(code.isGameCode()) { "'code'-string must be a valid game-code." }
    var multiple = code.first().toUpperCase() - 'A'
    if (multiple < 1) {
        multiple = 1
    } else if (multiple > maxPossibleTimer / timerIncrement) {
        multiple = maxPossibleTimer / timerIncrement
    }
    return multiple * timerIncrement
}

fun getFormattedTimeFromSeconds(seconds: Int): CharSequence {
    var formattedTime = ""
    val minutes = seconds / 60
    formattedTime += "$minutes:"
    val remainder = seconds % 60
    if (remainder < 10) {
        formattedTime += '0'
    }
    formattedTime += remainder
    return formattedTime
}
