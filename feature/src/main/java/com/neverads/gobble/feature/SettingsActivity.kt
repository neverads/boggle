package com.neverads.gobble.feature

import android.content.Context
import android.content.Intent
import android.graphics.Typeface
import android.os.Build
import android.os.Bundle
import android.os.Vibrator
import android.support.v7.app.AppCompatActivity
import android.widget.SeekBar
import com.neverads.boggle.feature.R
import kotlinx.android.synthetic.main.activity_settings.*
import kotlin.math.min

const val defaultRequiredCommonWordsCount = 9

const val vibeConnectLettersDefault = 0  // 5
const val vibeCompleteAWordDefault = 0
const val vibeEndOfGameDefault = 0  // 22
var vibeConnectLetters = vibeConnectLettersDefault
var vibeCompleteAWord = vibeCompleteAWordDefault
var vibeEndOfGame = vibeEndOfGameDefault

class SettingsActivity : AppCompatActivity() {
    private var isGameInProgress = false
    val minPossibleWordLength = 2
    val progressBarAdjustment = 2
    var gameCode = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_settings)

        gameCode = intent.getStringExtra(getString(R.string.game_code_intent_label))
        isGameInProgress = intent.getBooleanExtra(getString(R.string.is_game_in_progress_intent_label), false)

        val missedWordsMaxLength = getPreferenceMissedWordsMaxLength()
        setTextsForMissedWordsMaxLength(missedWordsMaxLength)
        seekBarMissedWordsMaxLength.progress = missedWordsMaxLength - progressBarAdjustment

        if (getSavedScoringStyle() == ScoringStyles.SIMPLIFIED) {
            switchScoringStyle.isChecked = true
            switchScoringStyle.text = getString(R.string.text_scoring_style_switch_simplified)
            textViewSoringStyleExplanation.text = getString(R.string.message_scoring_style_simplified_explanation)
            textViewScoringStyleExamples.text = getString(R.string.message_scoring_style_simplified_example)
        } else {
            switchScoringStyle.isChecked = false
            switchScoringStyle.text = getString(R.string.text_scoring_style_switch_classic)
            textViewSoringStyleExplanation.text = getString(R.string.message_scoring_style_classic_explanation)
            textViewScoringStyleExamples.text = getString(R.string.message_scoring_style_classic_example)
        }

        if (getPreferenceShouldUseCustomDict()) {
            switchUseCustomDictionary.isChecked = true
            textViewCustomDictionaryExplanation.text = getString(R.string.message_custom_dict_enabled)
        } else {
            switchUseCustomDictionary.isChecked = false
            textViewCustomDictionaryExplanation.text = getString(R.string.message_custom_dict_disabled)
        }

        val prefs = getSharedPreferences(getString(R.string.preferences), Context.MODE_PRIVATE)
        val requiredCommonWordsCount = prefs.getInt(getString(R.string.preference_required_common_words_count), defaultRequiredCommonWordsCount)
        seekBarMaximumDifficulty.progress = seekBarMaximumDifficulty.max - requiredCommonWordsCount
        setDifficultyLabelAndExplanationTexts(requiredCommonWordsCount)
        seekBarMaximumDifficulty.isEnabled = isGameInProgress.not()

        val minWordLength = prefs.getInt(getString(R.string.preference_min_word_length), default_min_word_length)
        textViewMinWordLengthLabel.text = minWordLength.toString()
        seekBarMinWordLength.progress = minWordLength - minPossibleWordLength

        val shouldDisplayAlert = prefs.getBoolean(getString(R.string.preference_should_display_skip_score_screen_modal), true)
        switchShouldDisplayModal.isChecked = shouldDisplayAlert
        switchShouldDisplayModal.text =
            getString(if (shouldDisplayAlert) R.string.setting_title_switch_will_display else R.string.setting_title_switch_will_not_display)

        switchScoringStyle.setOnCheckedChangeListener { switch, isChecked ->
            if (isChecked) {
                switch.text = getString(R.string.text_scoring_style_switch_simplified)
                textViewSoringStyleExplanation.text = getString(R.string.message_scoring_style_simplified_explanation)
                textViewScoringStyleExamples.text = getString(R.string.message_scoring_style_simplified_example)
            } else {
                switch.text = getString(R.string.text_scoring_style_switch_classic)
                textViewSoringStyleExplanation.text = getString(R.string.message_scoring_style_classic_explanation)
                textViewScoringStyleExamples.text = getString(R.string.message_scoring_style_classic_example)
            }
            prefs
                .edit()
                .putBoolean(getString(R.string.preference_is_scoring_style_classic), isChecked.not())
                .apply()

            return@setOnCheckedChangeListener
        }

        switchUseCustomDictionary.setOnCheckedChangeListener { _, isChecked ->
            if (isChecked) {
                textViewCustomDictionaryExplanation.text = getString(R.string.message_custom_dict_enabled)
            } else {
                textViewCustomDictionaryExplanation.text = getString(R.string.message_custom_dict_disabled)
            }
            prefs
                .edit()
                .putBoolean(getString(R.string.preference_should_use_custom_dictionary), isChecked)
                .apply()

            return@setOnCheckedChangeListener
        }

        seekBarMissedWordsMaxLength.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
            override fun onStartTrackingTouch(seekBar: SeekBar?) {}

            override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
                setTextsForMissedWordsMaxLength(progress + progressBarAdjustment)
            }

            override fun onStopTrackingTouch(seekBar: SeekBar?) {
                prefs
                    .edit()
                    .putInt(getString(R.string.preference_missed_words_max_length), seekBar!!.progress + progressBarAdjustment)
                    .apply()
            }
        })

        seekBarMaximumDifficulty.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
            override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
                val maxCommonWordsCount = seekBar!!.max - progress
                setDifficultyLabelAndExplanationTexts(maxCommonWordsCount)
            }

            override fun onStartTrackingTouch(seekBar: SeekBar?) {}

            override fun onStopTrackingTouch(seekBar: SeekBar?) {
                val newRequiredCommonWordsCount = seekBar!!.max - seekBar.progress
                prefs
                    .edit()
                    .putInt(getString(R.string.preference_required_common_words_count), newRequiredCommonWordsCount)
                    .apply()

                val gameCodeWithoutDifficultyChar = gameCode.substring(0 until gameCode.lastIndex)
                val newDifficultyChar = 'A' + newRequiredCommonWordsCount
                gameCode = "$gameCodeWithoutDifficultyChar$newDifficultyChar"
            }
        })

        seekBarMinWordLength.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
            override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
                val minWordLength = progress + minPossibleWordLength
                textViewMinWordLengthLabel.text = minWordLength.toString()
            }

            override fun onStartTrackingTouch(seekBar: SeekBar?) {}

            override fun onStopTrackingTouch(seekBar: SeekBar?) {
                val minWordLength = seekBar!!.progress + minPossibleWordLength
                prefs
                    .edit()
                    .putInt(getString(R.string.preference_min_word_length), minWordLength)
                    .apply()
            }
        })

        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.O) {
            seekBarVibrationConnectLetters.max = seekBarVibrationConnectLetters.max / 2
            seekBarVibrationCompleteAWord.max = seekBarVibrationCompleteAWord.max / 2
            seekBarVibrationEndOfGame.max = seekBarVibrationEndOfGame.max / 2
        }

        vibeConnectLetters = prefs.getInt(getString(R.string.preference_vibe_connect_letters), vibeConnectLettersDefault)
        vibeCompleteAWord = prefs.getInt(getString(R.string.preference_vibe_complete_a_word), vibeCompleteAWordDefault)
        vibeEndOfGame = prefs.getInt(getString(R.string.preference_vibe_end_of_game), vibeEndOfGameDefault)

        seekBarVibrationConnectLetters.progress = vibeConnectLetters
        seekBarVibrationCompleteAWord.progress = vibeCompleteAWord
        seekBarVibrationEndOfGame.progress = vibeEndOfGame

        val vibrator = this.getSystemService(Context.VIBRATOR_SERVICE) as Vibrator
        seekBarVibrationConnectLetters.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
            override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {}
            override fun onStartTrackingTouch(seekBar: SeekBar?) {}
            override fun onStopTrackingTouch(seekBar: SeekBar?) {
                vibeConnectLetters = seekBar!!.progress
                prefs
                    .edit()
                    .putInt(getString(R.string.preference_vibe_connect_letters), vibeConnectLetters)
                    .apply()
                vibrate(VibrationReason.AddLetter, vibrator)
            }
        })
        seekBarVibrationCompleteAWord.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
            override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {}
            override fun onStartTrackingTouch(seekBar: SeekBar?) {}
            override fun onStopTrackingTouch(seekBar: SeekBar?) {
                vibeCompleteAWord = seekBar!!.progress
                prefs
                    .edit()
                    .putInt(getString(R.string.preference_vibe_complete_a_word), vibeCompleteAWord)
                    .apply()
                vibrate(VibrationReason.FinishWordSuccess, vibrator)
            }
        })
        seekBarVibrationEndOfGame.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
            override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {}
            override fun onStartTrackingTouch(seekBar: SeekBar?) {}
            override fun onStopTrackingTouch(seekBar: SeekBar?) {
                vibeEndOfGame = seekBar!!.progress
                prefs
                    .edit()
                    .putInt(getString(R.string.preference_vibe_end_of_game), vibeEndOfGame)
                    .apply()
                vibrate(VibrationReason.EndOfGame, vibrator)
            }
        })

        val hasDiceEdge = prefs.getBoolean(getString(R.string.preference_dice_style_edge), default_should_use_dice_edge_background)
        switchSettingsDiceStyle.isChecked = hasDiceEdge
        val background = if (hasDiceEdge) R.drawable.dice_background else R.drawable.rounded_rectangle
        for (exampleDie in listOf(settingsDiceStyleExampleB, settingsDiceStyleExampleQu)) {
            exampleDie.setBackgroundResource(background)
        }

        switchSettingsDiceStyle.setOnCheckedChangeListener { _, isChecked ->
            prefs
                .edit()
                .putBoolean(getString(R.string.preference_dice_style_edge), isChecked)
                .apply()

            val newBackground = if (isChecked) R.drawable.dice_background else R.drawable.rounded_rectangle
            for (exampleDie in listOf(settingsDiceStyleExampleB, settingsDiceStyleExampleQu)) {
                exampleDie.setBackgroundResource(newBackground)
            }
        }

        val minTextSize = 8f
        val textSizeBaseValue = prefs.getInt(getString(R.string.preference_dice_style_text_size), default_dice_text_size)
        seekBarSettingsDiceStyleFontSize.progress = textSizeBaseValue
        for (exampleDie in listOf(settingsDiceStyleExampleB, settingsDiceStyleExampleQu)) {
            exampleDie.textSize = textSizeBaseValue + minTextSize
        }

        seekBarSettingsDiceStyleFontSize.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
            override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
                for (exampleDie in listOf(settingsDiceStyleExampleB, settingsDiceStyleExampleQu)) {
                    exampleDie.textSize = progress.toFloat() + minTextSize
                }
            }

            override fun onStartTrackingTouch(seekBar: SeekBar?) {}
            override fun onStopTrackingTouch(seekBar: SeekBar?) {
                prefs
                    .edit()
                    .putInt(getString(R.string.preference_dice_style_text_size), seekBar!!.progress)
                    .apply()
            }
        })

        val boldness = prefs.getInt(getString(R.string.preference_dice_style_boldness), default_dice_text_boldness)
        seekBarSettingsDiceStyleBoldness.progress = boldness
        val typeface = when (boldness) {
            0 -> Typeface.SANS_SERIF
            2 -> Typeface.DEFAULT_BOLD
            else -> fontMedium
        }
        for (exampleDie in listOf(settingsDiceStyleExampleB, settingsDiceStyleExampleQu)) {
            exampleDie.typeface = typeface
        }

        seekBarSettingsDiceStyleBoldness.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
            override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
                val newTypeface = when (progress) {
                    0 -> Typeface.SANS_SERIF
                    2 -> Typeface.DEFAULT_BOLD
                    else -> fontMedium
                }
                for (exampleDie in listOf(settingsDiceStyleExampleB, settingsDiceStyleExampleQu)) {
                    exampleDie.typeface = newTypeface
                }
            }

            override fun onStartTrackingTouch(seekBar: SeekBar?) {}
            override fun onStopTrackingTouch(seekBar: SeekBar?) {
                prefs
                    .edit()
                    .putInt(getString(R.string.preference_dice_style_boldness), seekBar!!.progress)
                    .apply()
            }
        })

        switchShouldDisplayModal.setOnCheckedChangeListener { _, isChecked ->
            prefs
                .edit()
                .putBoolean(getString(R.string.preference_should_display_skip_score_screen_modal), isChecked)
                .apply()

            switchShouldDisplayModal.text =
                getString(if (isChecked) R.string.setting_title_switch_will_display else R.string.setting_title_switch_will_not_display)
        }

        if (prefs.getBoolean(getString(R.string.preference_should_show_possible_score), false)) {
            switchPossiblePoints.text = getString(R.string.setting_possible_points_label_displayed)
            switchPossiblePoints.isChecked = true
        } else {
            switchPossiblePoints.text = getString(R.string.setting_possible_points_label_hidden)
            switchPossiblePoints.isChecked = false
        }

        switchPossiblePoints.setOnCheckedChangeListener { _, isChecked ->
            prefs.edit()
                .putBoolean(getString(R.string.preference_should_show_possible_score), isChecked)
                .apply()

            textViewPossiblePointsExplanation.text =
                getString(if (isChecked) R.string.setting_possible_points_explanation_enabled else R.string.setting_possible_points_explanation_disabled)
            switchPossiblePoints.text =
                getString(if (isChecked) R.string.setting_possible_points_label_displayed else R.string.setting_possible_points_label_hidden)
        }
    }

    override fun onBackPressed() {
        val intent = Intent()
        intent.putExtra(getString(R.string.game_code_intent_label), gameCode)
        setResult(RESULT_OK, intent)
        finish()
    }

    private fun setDifficultyLabelAndExplanationTexts(requiredCommonWordsCount: Int) {
        val choiceText: String
        var explanationText: String
        when {
            requiredCommonWordsCount <= 4 -> {
                choiceText = getString(R.string.setting_maximum_difficulty_choice_impossible)
                explanationText = getString(R.string.setting_maximum_difficulty_explanation_impossible)
            }
            requiredCommonWordsCount <= 9 -> {
                choiceText = getString(R.string.setting_maximum_difficulty_choice_challenging)
                explanationText = getString(R.string.setting_maximum_difficulty_explanation_challenging)
            }
            requiredCommonWordsCount <= 15 -> {
                choiceText = getString(R.string.setting_maximum_difficulty_choice_normal)
                explanationText = getString(R.string.setting_maximum_difficulty_explanation_normal)
            }
            else -> {
                choiceText = getString(R.string.setting_maximum_difficulty_choice_easy)
                explanationText = getString(R.string.setting_maximum_difficulty_explanation_easy)
            }
        }
        if (isGameInProgress) {
            explanationText = "${getString(R.string.setting_maximum_difficulty_disabled)}\n\n$explanationText"
        }
        textViewMaximumDifficultyExplanation.text = explanationText
        textViewMaximumDifficultyLabel.text = choiceText
    }

    private fun getSavedScoringStyle(): ScoringStyles {
        val isClassic = getSharedPreferences(getString(R.string.preferences), Context.MODE_PRIVATE)
            .getBoolean(getString(R.string.preference_is_scoring_style_classic), false)
        return when {
            isClassic -> ScoringStyles.CLASSIC
            else -> ScoringStyles.SIMPLIFIED
        }
    }

    private fun getPreferenceShouldUseCustomDict(): Boolean {
        return getSharedPreferences(getString(R.string.preferences), Context.MODE_PRIVATE)
            .getBoolean(getString(R.string.preference_should_use_custom_dictionary), true)
    }

    private fun getPreferenceMissedWordsMaxLength(): Int {
        val setting = getSharedPreferences(getString(R.string.preferences), Context.MODE_PRIVATE)
            .getInt(getString(R.string.preference_missed_words_max_length), default_missed_words_max_length)
        val maxSettingToAvoidBuggyMissedWords = 6
        return min(setting, maxSettingToAvoidBuggyMissedWords)
    }

    private fun setTextsForMissedWordsMaxLength(length: Int) {
        val labelText: String
        val batteryUsage: String
        val baseExplanation: String
        if (length < 3) {
            labelText = getString(R.string.setting_missed_words_label, "—")
            batteryUsage = getString(R.string.setting_missed_words_explanation_battery, "—")
            baseExplanation = getString(R.string.setting_missed_words_explanation_disabled)
        } else {
            labelText = getString(R.string.setting_missed_words_label, length.toString())
            batteryUsage = when (length) {
                in 3..4 -> getString(R.string.setting_missed_words_explanation_battery, "very light")
                5 -> getString(R.string.setting_missed_words_explanation_battery, "light")
                else -> getString(R.string.setting_missed_words_explanation_battery, "moderate")
            }
            baseExplanation = getString(R.string.setting_missed_words_explanation_base, length)
        }

        textViewMissedWordsMaxLengthLabel.text = labelText
        textViewMissedWordsMaxLengthBattery.text = batteryUsage
        textViewMissedWordsMaxLengthExplanation.text = baseExplanation
    }
}
