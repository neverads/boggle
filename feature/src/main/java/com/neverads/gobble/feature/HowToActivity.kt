package com.neverads.gobble.feature

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.neverads.boggle.feature.R
import kotlinx.android.synthetic.main.activity_how_to.*

class HowToActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_how_to)

        val dice = arrayOf(
            fakeDie00, fakeDie01, fakeDie02, fakeDie03,
            fakeDie10, fakeDie11, fakeDie12, fakeDie13,
            fakeDie20, fakeDie21, fakeDie22, fakeDie23,
            fakeDie30, fakeDie31, fakeDie32, fakeDie33
        )
        dice.forEachIndexed { index, die ->
            val dieText = getString(R.string.title_dice_default)[index].toString()
            die.text = dieText
        }
    }
}
