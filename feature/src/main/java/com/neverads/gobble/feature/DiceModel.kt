package com.neverads.gobble.feature

import android.widget.TextView
import java.util.*
import kotlin.math.abs
import kotlin.math.max
import kotlin.math.min

data class Die(val textView: TextView, val rowIndex: Int, val columnIndex: Int, var isAvailable: Boolean = true) {
    fun isAdjacentToDie(die: Die?): Boolean {
        return when (die) {
            null -> true
            else -> abs(rowIndex - die.rowIndex) <= 1 && abs(columnIndex - die.columnIndex) <= 1
        }
    }
}


data class _Die(val letters: List<Char>, val initialIndex: Int, var currentSide: Int = randomlySeededDiceSide()) {
    fun getCurrentLetter(): Char {
        return letters[currentSide]
    }
}


fun randomlySeededDiceSide(): Int {
    val seed = kotlin.system.measureNanoTime {}.toInt() % 1000 + Calendar.getInstance().get(Calendar.MILLISECOND)
    return ((0..5).random() + seed) % 6
}


class DiceModel {
    private val die0 = _Die(letters = listOf('A', 'A', 'E', 'E', 'G', 'N'), initialIndex = 0)
    private val die1 = _Die(letters = listOf('A', 'B', 'B', 'J', 'O', 'O'), initialIndex = 1)
    private val die2 = _Die(letters = listOf('A', 'C', 'H', 'O', 'P', 'S'), initialIndex = 2)
    private val die3 = _Die(letters = listOf('A', 'F', 'F', 'K', 'P', 'S'), initialIndex = 3)
    private val die4 = _Die(letters = listOf('A', 'O', 'O', 'T', 'T', 'W'), initialIndex = 4)
    private val die5 = _Die(letters = listOf('C', 'I', 'M', 'O', 'T', 'U'), initialIndex = 5)
    private val die6 = _Die(letters = listOf('D', 'E', 'I', 'L', 'R', 'X'), initialIndex = 6)
    private val die7 = _Die(letters = listOf('D', 'E', 'L', 'R', 'V', 'Y'), initialIndex = 7)
    private val die8 = _Die(letters = listOf('D', 'I', 'S', 'T', 'T', 'Y'), initialIndex = 8)
    private val die9 = _Die(letters = listOf('E', 'E', 'G', 'H', 'N', 'W'), initialIndex = 9)
    private val die10 = _Die(letters = listOf('E', 'E', 'I', 'N', 'S', 'U'), initialIndex = 10)
    private val die11 = _Die(letters = listOf('E', 'H', 'R', 'T', 'V', 'W'), initialIndex = 11)
    private val die12 = _Die(letters = listOf('E', 'I', 'O', 'S', 'S', 'T'), initialIndex = 12)
    private val die13 = _Die(letters = listOf('E', 'L', 'R', 'T', 'T', 'Y'), initialIndex = 13)
    private val die14 = _Die(letters = listOf('H', 'I', 'M', 'N', 'U', 'Q'), initialIndex = 14)
    private val die15 = _Die(letters = listOf('H', 'L', 'N', 'N', 'R', 'Z'), initialIndex = 15)
    var dice = arrayOf(
        die0, die1, die2, die3,
        die4, die5, die6, die7,
        die8, die9, die10, die11,
        die12, die13, die14, die15
    )

    fun updateDice(gameCode: String, seed: Int = Calendar.getInstance(TimeZone.getTimeZone("UTC")).get(Calendar.DAY_OF_YEAR)) {
        require(gameCode.isGameCode()) { "Must use a valid game-code." }
        val newDice = Array<_Die?>(16) { null }
        dice.getSortedByInitialIndex().forEach { die ->
            val indicesOfGameCodeChars: IntArray = when {
                die.initialIndex % 3 == 0 -> intArrayOf(1, 2, 3)
                die.initialIndex % 3 == 1 -> intArrayOf(1, 3, 4)
                else -> intArrayOf(2, 3, 4)
            }

            var shiftForDie = seed
            indicesOfGameCodeChars.forEach {
                shiftForDie += gameCode[it].toUpperCase() - 'A'
            }

            var newDieIndex = (die.initialIndex + shiftForDie) % 16
            while (newDice[newDieIndex] != null) {
                newDieIndex = (newDieIndex + 1) % 16
            }
            var newDieSide = 0
            when (die.initialIndex) {
                0 -> newDieSide += (seed + gameCode[1].toInt()) * 3
                1 -> newDieSide += seed * gameCode[2].toInt()
                2 -> newDieSide += (seed / gameCode[3].toInt()) / 2
                3 -> newDieSide += seed + gameCode[4].toInt()
                4 -> newDieSide += (seed / gameCode[1].toInt() + gameCode[2].toInt()) / 2
                5 -> newDieSide += seed * gameCode[1].toInt() + gameCode[3].toInt()
                6 -> newDieSide += (seed / gameCode[1].toInt() + gameCode[4].toInt()) * 2
                7 -> newDieSide += seed * gameCode[2].toInt() + gameCode[3].toInt()
                8 -> newDieSide += (seed + gameCode[2].toInt() + gameCode[4].toInt()) / 3
                9 -> newDieSide += seed + gameCode[3].toInt() + gameCode[4].toInt()
                10 -> newDieSide += (seed + gameCode[1].toInt() + gameCode[2].toInt() + gameCode[3].toInt()) * 2
                11 -> newDieSide += (seed * gameCode[1].toInt() + gameCode[2].toInt() * gameCode[4].toInt()) / 3
                12 -> newDieSide += seed + gameCode[2].toInt() + gameCode[3].toInt() + gameCode[4].toInt()
                13 -> newDieSide += (seed / gameCode[1].toInt() * gameCode[2].toInt() + gameCode[3].toInt() + gameCode[4].toInt()) / 2
                14 -> newDieSide += (seed * gameCode[1].toInt() * gameCode[2].toInt() + gameCode[3].toInt() + gameCode[4].toInt()) / 3
                15 -> newDieSide += (seed / gameCode[1].toInt() + gameCode[2].toInt() * gameCode[3].toInt() + gameCode[4].toInt()) * 2
            }
            newDieSide %= 6
            newDice[newDieIndex] = _Die(letters = die.letters, initialIndex = die.initialIndex, currentSide = newDieSide)
        }
        dice = newDice.requireNoNulls()
    }

    fun getLetterCombinations(paths: List<Path>, letters: Array<CharArray> = dice.getLetters().toTwoDimensionalArray()): Set<String> {
        val lettersLowered = Array(letters.size) { CharArray(letters.size) }
        lettersLowered.forEachIndexed { rowIndex, row ->
            row.forEachIndexed { columnIndex, _ ->
                lettersLowered[rowIndex][columnIndex] = letters[rowIndex][columnIndex].toLowerCase()
            }
        }

        val letterCombinations = mutableListOf<String>()
        paths.forEach { path ->
            var letterCombination = ""
            path.coordinates.forEach { coordinate ->
                letterCombination += lettersLowered[coordinate.rowIndex][coordinate.columnIndex]
            }
            letterCombinations.add(letterCombination)
        }

        return letterCombinations.toSet()
    }

    fun getPathsThroughDiceGrid(
        coordinatesGrid: Array<Array<Coordinate>>,
        rowIndex: Int = 0,
        columnIndex: Int = 0,
        minPathSize: Int = 3,
        maxPathSize: Int = -1,  // -1 is treated as infinite
        path: MutableList<Coordinate> = mutableListOf(),
        paths: MutableList<Path> = mutableListOf()
    ): MutableList<Path> {
        val gridSize = coordinatesGrid.size
        require(gridSize > 0) {
            "Must have at least one row.  coordinates: ${coordinatesGrid.contentDeepToString()}"
        }
        coordinatesGrid.forEach { row ->
            require(row.size == gridSize) {
                "Must have the same number of rows & columns - a square.  coordinates: ${coordinatesGrid.contentDeepToString()}"
            }
        }

        if (maxPathSize >= 0 && path.size >= maxPathSize) {
            return paths
        }

        val currentCoordinate = coordinatesGrid[rowIndex][columnIndex]
        if (rowIndex in 0 until gridSize && columnIndex in 0 until gridSize && currentCoordinate !in path) {
            path.add(currentCoordinate)
            if (path.size >= minPathSize) {
                paths.add(Path(path.toTypedArray()))
            }

            val minNextRowIndex = max(rowIndex - 1, 0)
            val maxNextRowIndex = min(rowIndex + 1, gridSize - 1)
            (minNextRowIndex..maxNextRowIndex).forEach { nextRowIndex ->
                val minNextColumnIndex = max(columnIndex - 1, 0)
                val maxNextColumnIndex = min(columnIndex + 1, gridSize - 1)
                (minNextColumnIndex..maxNextColumnIndex).forEach { nextColumnIndex ->
                    val isSameCoordinate = (nextRowIndex == rowIndex) && (nextColumnIndex == columnIndex)
                    if (isSameCoordinate.not() && path.contains(coordinatesGrid[nextRowIndex][nextColumnIndex]).not())
                        getPathsThroughDiceGrid(coordinatesGrid, nextRowIndex, nextColumnIndex, minPathSize, maxPathSize, path, paths)
                }
            }
            path.removeAt(path.lastIndex)
        }

        return paths
    }

    fun getAllPathsThroughDiceGrid4x4(minPathSize: Int = 3, maxPathSize: Int = 6): MutableList<Path> {
        require(minPathSize in 0..maxPathSize)
        val allPaths = mutableListOf<Path>()
        coordinates4x4.forEach { row ->
            row.forEach { coordinate ->
                allPaths.addAll(getPathsThroughDiceGrid(coordinates4x4, coordinate.rowIndex, coordinate.columnIndex, minPathSize, maxPathSize))
            }
        }
        return allPaths
    }
}
