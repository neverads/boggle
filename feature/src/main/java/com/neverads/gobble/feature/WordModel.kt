package com.neverads.gobble.feature

import kotlin.math.max


data class Word(val text: String, var isCounted: Boolean = true, var isMissed: Boolean = false) {
    val pointsWithClassicScoring: Int
        get() = if (isCounted && isMissed.not()) possiblePointsWithClassicScoring else 0
    val possiblePointsWithClassicScoring: Int
        get() = when (text.length) {
            in listOf(3, 4) -> 1
            5 -> 2
            6 -> 3
            7 -> 5
            else -> 11
        }

    val pointsWithSimplifiedScoring: Int
        get() = if (isCounted && isMissed.not()) possiblePointsWithSimplifiedScoring else 0
    val possiblePointsWithSimplifiedScoring: Int
        get() = max(text.length - 2, 1)
}
