package com.neverads.gobble.feature

import android.content.Context
import android.content.res.ColorStateList
import android.graphics.Color
import android.os.Handler
import android.support.v4.content.ContextCompat
import android.support.v4.widget.ImageViewCompat
import android.support.v7.app.AlertDialog
import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import com.neverads.boggle.feature.R
import kotlinx.android.synthetic.main.recyclerview_item_row_missed_words_header.view.*
import kotlinx.android.synthetic.main.recyclerview_item_row_scoring.view.*


class RecyclerAdapterScoring(
    private val words: List<Word>,
    val clickListener: () -> Unit,
    var scoringStyle: ScoringStyles,
    val isCustomDict: Boolean,
    private val isDeviceConnected: Boolean,
    val onMissedWordsTapped: () -> Unit,
    val onMissedWordsIconTapped: () -> Unit,
    val missedWordsHeaderText: String?
) : RecyclerView.Adapter<RecyclerAdapterScoring.WordHolder>() {

    private val selectedWords = words.filter { it.isMissed.not() }
    private val missedWords = words.filter { it.isMissed }.sortByPossiblePoints()
    private val headerCount = if (missedWordsHeaderText == null) 0 else 1

    override fun getItemCount() = selectedWords.size + headerCount + missedWords.size

    override fun onBindViewHolder(holder: WordHolder, position: Int) {
        val viewType = getItemViewType(position)
        if (viewType == ViewTypes.FoundWord.value) {
            val word = words[position]
            holder.bindWord(word, clickListener, scoringStyle, isCustomDict, isDeviceConnected)
        } else if (viewType == ViewTypes.MissedWordsHeader.value) {
            holder.bindMissedWordsHeader(onMissedWordsTapped, onMissedWordsIconTapped, missedWordsHeaderText)
        } else {
            val missedWordIndex = position - selectedWords.size - headerCount
            val missedWord = missedWords[missedWordIndex]
            holder.bindWord(missedWord, {}, scoringStyle, isCustomDict, isDeviceConnected)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): WordHolder {
        val inflatedView = if (viewType == ViewTypes.MissedWordsHeader.value) {
            parent.inflate(R.layout.recyclerview_item_row_missed_words_header)
        } else {
            parent.inflate(R.layout.recyclerview_item_row_scoring)
        }
        return WordHolder(inflatedView)
    }

    override fun getItemViewType(position: Int): Int {
        return when {
            position < selectedWords.size -> ViewTypes.FoundWord.value
            position == selectedWords.size -> ViewTypes.MissedWordsHeader.value
            else -> ViewTypes.MissedWord.value
        }
    }

    class WordHolder(v: View) : RecyclerView.ViewHolder(v) {
        private var view: View = v
        private var word: Word? = null
        private val context = view.context
        private var areDictionaryIndicatorsDisplayed = false
        private var areDictionaryIndicatorsToggledOn = true
        private var shouldDisplayAlertDictionaryToggleInfo = true
        private val handler = Handler()

        fun bindWord(word: Word, clickListener: () -> Unit, scoringStyle: ScoringStyles, isCustomDict: Boolean, isDeviceConnected: Boolean) {
            shouldDisplayAlertDictionaryToggleInfo = getShouldDisplayAlertDictionaryToggleInfo()

            this.word = word

            if (isDeviceConnected) {
                view.imageButtonScoreScreenLookUpDefinition.visibility = View.VISIBLE
            } else {
                view.imageButtonScoreScreenLookUpDefinition.visibility = View.INVISIBLE
            }

            if (isCustomDict) {
                view.imageButtonExpandDictionaryToggle.visibility = View.VISIBLE
            } else {
                view.imageButtonExpandDictionaryToggle.visibility = View.INVISIBLE
            }

            listOf(view, view.checkBoxWordIsCounted).forEach { it.isEnabled = word.isMissed.not() }
            listOf(view.checkBoxWordIsCounted, view.textViewWordPoints).forEach { it.alpha = if (word.isMissed) 0.6f else 1.0f }

            areDictionaryIndicatorsDisplayed = wordsWithDictionaryToggleDisplayed.contains(word.text)
            setInitialDictionaryIndicatorUI()

            areDictionaryIndicatorsToggledOn = isWordInDict(word = word.text)
            updateDictionaryIndicators()
            view.checkBoxWordIsCounted.isChecked = word.isCounted
            view.checkBoxWordIsCounted.text = word.text
            if (scoringStyle == ScoringStyles.SIMPLIFIED) {
                view.textViewWordPoints.text = word.possiblePointsWithSimplifiedScoring.toString()
            } else {
                view.textViewWordPoints.text = word.possiblePointsWithClassicScoring.toString()
            }

            fun setTextColor() {
                val nearBlack = "#111111"
                val grey = "#999999"
                val colorParsed = Color.parseColor(if (view.checkBoxWordIsCounted.isChecked) nearBlack else grey)
                view.textViewWordPoints.setTextColor(colorParsed)
                view.checkBoxWordIsCounted.setTextColor(colorParsed)
            }
            setTextColor()

            view.setOnClickListener {
                view.checkBoxWordIsCounted.toggle()
                setTextColor()
                word.isCounted = word.isCounted.not()
                clickListener()
            }
            view.checkBoxWordIsCounted.setOnClickListener {
                setTextColor()
                word.isCounted = word.isCounted.not()
                clickListener()
            }
            view.imageButtonExpandDictionaryToggle.setOnClickListener {
                if (areDictionaryIndicatorsDisplayed.not() && getShouldDisplayAlertDictionaryToggleInfo()) {
                    displayAlertDictionaryToggleInfo()
                }
                if (wordsWithDictionaryToggleDisplayed.contains(word.text)) {
                    wordsWithDictionaryToggleDisplayed.remove(word.text)
                } else {
                    wordsWithDictionaryToggleDisplayed.add(word.text)
                }
                areDictionaryIndicatorsDisplayed = areDictionaryIndicatorsDisplayed.not()
                flipImageButtonExpandDictionaryToggle()
                toggleDictionaryIndicatorVisibility()
            }
            listOf(view.imageViewDictionary, view.imageViewIsInDictionary).forEach { view ->
                view.setOnClickListener {
                    areDictionaryIndicatorsToggledOn = areDictionaryIndicatorsToggledOn.not()
                    updateDictionaryIndicators()
                    updatePreferencesWithWord(word = word.text)
                }
            }
            listOf(view.imageButtonExpandDictionaryToggle, view.imageViewDictionary, view.imageViewIsInDictionary).forEach { view ->
                view.setOnLongClickListener {
                    displayAlertDictionaryToggleInfo()
                    return@setOnLongClickListener true
                }
            }
            view.imageButtonScoreScreenLookUpDefinition.setOnClickListener {
                view.progressBarScoreScreenLookUpDefinition.visibility = View.VISIBLE
                view.imageButtonScoreScreenLookUpDefinition.visibility = View.INVISIBLE

                Thread(object : Runnable {
                    override fun run() {
                        val definitions = getDefinitions(word = word.text, context = context)
                        handler.post {
                            run {
                                AlertDialog.Builder(context)
                                    .setIcon(R.drawable.ic_dictionary_icon_primary_color)
                                    .setTitle("\"${word.text}\"")
                                    .setMessage(definitions.joinToString("\n\n"))
                                    .setPositiveButton("ok", null)
                                    .show()
                            }
                        }
                        handler.post {
                            run {
                                view.progressBarScoreScreenLookUpDefinition.visibility = View.INVISIBLE
                                view.imageButtonScoreScreenLookUpDefinition.visibility = View.VISIBLE
                            }
                        }
                    }
                }).start()
            }
        }

        fun bindMissedWordsHeader(onMissedWordsTapped: () -> Unit, onIconTapped: () -> Unit, missedWordsHeaderText: String?) {
            view.textViewHeader.text = missedWordsHeaderText
            view.setOnClickListener { onMissedWordsTapped() }
            view.imageButtonBetaMessageTrigger.setOnClickListener { onIconTapped() }
        }

        private fun flipImageButtonExpandDictionaryToggle() {
            view.imageButtonExpandDictionaryToggle.rotation =
                (view.imageButtonExpandDictionaryToggle.rotation + 180) % 360
        }

        private fun updateDictionaryIndicators() {
            val newBackgroundColor: Int
            val visibilityOfIsRemoved: Int
            val newImageTint: ColorStateList
            if (areDictionaryIndicatorsToggledOn) {
                newBackgroundColor = ContextCompat.getColor(context, R.color.colorLightGreenBackground)
                visibilityOfIsRemoved = View.INVISIBLE
                newImageTint = ColorStateList.valueOf(ContextCompat.getColor(context, R.color.colorPrimary))
            } else {
                newBackgroundColor = ContextCompat.getColor(context, R.color.colorLightRedBackground)
                visibilityOfIsRemoved = View.VISIBLE
                newImageTint = ColorStateList.valueOf(Color.GRAY)
            }
            view.imageViewIsInDictionary.visibility = visibilityOfIsRemoved
            view.imageViewDictionary.setBackgroundColor(newBackgroundColor)
            ImageViewCompat.setImageTintList(view.imageViewDictionary, newImageTint)
        }

        private fun updatePreferencesWithWord(word: String) {
            val preferences = context.getSharedPreferences(context.getString(R.string.preferences), Context.MODE_PRIVATE)
            val refToAddedWords = context.getString(R.string.preference_custom_dict_added_words)
            val addedWords = preferences.getStringSet(refToAddedWords, emptySet<String>())!!
            if (addedWords.contains(word)) {
                if (areDictionaryIndicatorsToggledOn) {
                    preferences.edit().putStringSet(refToAddedWords, addedWords + word).apply()
                } else {
                    preferences.edit().putStringSet(refToAddedWords, addedWords - word).apply()
                }
            } else {
                val refToRemovedWords = context.getString(R.string.preference_custom_dict_removed_words)
                val removedWords = preferences.getStringSet(refToRemovedWords, emptySet<String>())!!
                if (areDictionaryIndicatorsToggledOn) {
                    preferences.edit().putStringSet(refToRemovedWords, removedWords - word).apply()
                } else {
                    preferences.edit().putStringSet(refToRemovedWords, removedWords + word).apply()
                }
            }
        }

        private fun isWordInDict(word: String): Boolean {
            val preferences = context.getSharedPreferences(context.getString(R.string.preferences), Context.MODE_PRIVATE)
            val addedWords = preferences.getStringSet(context.getString(R.string.preference_custom_dict_added_words), emptySet<String>())!!
            return if (addedWords.contains(word)) {
                true
            } else {
                preferences
                    .getStringSet(context.getString(R.string.preference_custom_dict_removed_words), emptySet<String>())!!
                    .contains(word)
                    .not()
            }
        }

        private fun getShouldDisplayAlertDictionaryToggleInfo(): Boolean {
            return if (shouldDisplayAlertDictionaryToggleInfo) {
                context.getSharedPreferences(context.getString(R.string.preferences), Context.MODE_PRIVATE)
                    .getBoolean(context.getString(R.string.preference_should_display_alert_dictionary_toggle_info), true)
            } else {
                shouldDisplayAlertDictionaryToggleInfo
            }
        }

        private fun displayAlertDictionaryToggleInfo() {
            AlertDialog.Builder(context)
                .setIcon(R.drawable.ic_dictionary_icon_primary_color)
                .setTitle(R.string.alert_title_dictionary_toggle_info)
                .setMessage(R.string.alert_message_dictionary_toggle_info)
                .setPositiveButton(R.string.alert_button_title_dictionary_toggle_info_got_it) { _, _ ->
                    shouldDisplayAlertDictionaryToggleInfo = false
                    val shouldDisplayAlert = context.getString(R.string.preference_should_display_alert_dictionary_toggle_info)
                    context.getSharedPreferences(context.getString(R.string.preferences), Context.MODE_PRIVATE)
                        .edit()
                        .putBoolean(shouldDisplayAlert, shouldDisplayAlertDictionaryToggleInfo)
                        .apply()
                }
                .create()
                .show()
        }

        private fun toggleDictionaryIndicatorVisibility() {
            val xFromRightOfScreen = if (areDictionaryIndicatorsDisplayed) {
                view.imageViewDictionary.width * -1f
            } else {
                0f
            }
            listOf(
                view.imageButtonExpandDictionaryToggle,
                view.imageViewDictionary,
                view.imageViewIsInDictionary
            ).forEach { view ->
                view.animate()
                    .translationX(xFromRightOfScreen)
                    .start()
            }
        }

        private fun setInitialDictionaryIndicatorUI() {
            val rotationNormal = 270f
            val rotationFlipAdjustment = 180

            val rightEdgeOfScreen = 0f
            val widthOfView = view.imageViewDictionary.width

            val newRotation: Float
            val newTranslationX: Float
            if (areDictionaryIndicatorsDisplayed) {
                newRotation = rotationNormal - rotationFlipAdjustment
                newTranslationX = rightEdgeOfScreen - widthOfView
            } else {
                newRotation = rotationNormal
                newTranslationX = rightEdgeOfScreen
            }

            view.imageButtonExpandDictionaryToggle.rotation = newRotation
            listOf(view.imageButtonExpandDictionaryToggle, view.imageViewDictionary, view.imageViewIsInDictionary).forEach { view ->
                view.translationX = newTranslationX
            }
        }
    }
}


enum class ViewTypes(val value: Int) {
    FoundWord(1),
    MissedWordsHeader(2),
    MissedWord(3)
}
