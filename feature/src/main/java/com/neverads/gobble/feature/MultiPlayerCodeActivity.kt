package com.neverads.gobble.feature

import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.content.pm.PackageInfo
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.text.Editable
import android.text.TextWatcher
import com.neverads.boggle.feature.R
import kotlinx.android.synthetic.main.activity_multi_player_code.*


class MultiPlayerCodeActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        var version = ""

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_multi_player_code)

        textViewMultiplayerHowTo.requestFocus()  // avoid focusing the game-code's text-view, which instantly & automatically opens the keyboard

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            text_field_game_code.letterSpacing = 0.1f
        }

        val gameCodeFromMainActivity = intent.getStringExtra(getString(R.string.game_code_intent_label))
        text_field_game_code.setText(gameCodeFromMainActivity)

        try {
            val pInfo: PackageInfo = this.packageManager.getPackageInfo(packageName, 0)
            version = pInfo.versionName
            text_view_app_version.text = "Version $version"
        } catch (e: PackageManager.NameNotFoundException) {
            e.printStackTrace()
        }

        text_view_app_version.setOnClickListener {
            AlertDialog.Builder(this)
                .setIcon(R.drawable.ic_multiple_people_green)
                .setTitle("Version $version")
                .setMessage(
                    "In order to ensure your multiplayer games will stay synced, all players should have the same version.\n\n" +
                            "If the versions are different, please check for app-updates."
                )
                .setPositiveButton("OK", null)
                .create()
                .show()
        }

        text_field_game_code.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(textAfterChange: Editable) {
                val isValidGameCode = textAfterChange.toString().isGameCode()
                image_button_done.isEnabled = isValidGameCode
                image_button_done.alpha = if (isValidGameCode) 1.0f else 0.3f
            }

            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {}
        })

        image_button_done.setOnClickListener {
            val gameCode = text_field_game_code.text.toString()

            val requiredCommonWordsCount = getRequiredCommonWordsCountFromGameCode(gameCode)
            setRequiredCommonWordsCount(requiredCommonWordsCount)

            val intent = Intent()
            intent.putExtra(getString(R.string.game_code_intent_label), gameCode)
            setResult(RESULT_OK, intent)
            finish()
        }
    }

    fun getRequiredCommonWordsCountFromGameCode(code: String): Int {
        require(code.isGameCode()) { "'code'-string must be a valid game-code." }
        val difficultyChar = code.last()
        return difficultyChar - 'A'
    }

    private fun setRequiredCommonWordsCount(newCount: Int) {
        getSharedPreferences(getString(R.string.preferences), Context.MODE_PRIVATE)
            .edit()
            .putInt(getString(R.string.preference_required_common_words_count), newCount)
            .apply()
    }
}
