package com.neverads.gobble.feature

import android.widget.ImageView


data class Line(val imageView: ImageView, val connectedDice: Pair<Die, Die>)
