package com.neverads.gobble.feature


data class DictionaryWord(val word: String, val isCustom: Boolean = true, var isRemoved: Boolean = false)
