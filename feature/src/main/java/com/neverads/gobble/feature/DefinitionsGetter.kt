package com.neverads.gobble.feature

import android.content.Context
import com.neverads.boggle.feature.R
import org.jsoup.Jsoup
import org.jsoup.select.Elements
import java.io.IOException

fun getDefinitions(word: String, context: Context): List<String> {
    if (word in storedDefinitions.keys && storedDefinitions[word].isNullOrEmpty().not()) {
        return storedDefinitions[word]!!
    }
    val definitions = mutableListOf<String>()

    definitions.addAll(getDefinitionsFromWikipedia(word))
    if (definitions.isNotEmpty()) {
        return definitions
    }

    try {
        val url = "https://www.wordnik.com/words/$word"
        val htmlDocument = Jsoup.connect(url).get()
        val definitionsHtmlElements = htmlDocument.getElementsByClass("guts active")[0].select("ul").select("li")
        definitionsHtmlElements.forEachIndexed { index, fullDefinition ->
            if (index < 5) {
                val partOfSpeech = fullDefinition.select("abbr").text()
                if (partOfSpeech.isNullOrBlank()) {
                    definitions.add(fullDefinition.text())
                } else {
                    val newDefinition = fullDefinition.text().removePrefix(partOfSpeech)
                    definitions.add("($partOfSpeech) $newDefinition")
                }
            } else {
                return@forEachIndexed
            }
        }
        storedDefinitions[word] = definitions
        if (definitions.isNullOrEmpty()) {
            definitions.add(context.getString(R.string.message_definition_blank_explanation))
        }
        return definitions
    } catch (networkException: IOException) {
        return listOf(
            context.getString(R.string.message_definition_network_failure),
            context.getString(R.string.message_definition_failure_details_intro),
            "${networkException.message}"
        )
    } catch (generalException: Exception) {
        return listOf(
            context.getString(R.string.message_definition_failure_apology),
            context.getString(R.string.message_definition_failure_explanation),
            context.getString(R.string.message_definition_failure_details_intro),
            "${generalException.message}"
        )
    }
}

private fun getDefinitionsFromWikipedia(word: String): List<String> {
    try {
        val url = "https://en.wiktionary.org/w/api.php?action=query&prop=extracts&format=json&titles=$word"
        val htmlDocument = Jsoup.connect(url).ignoreContentType(true).get()
        val bodyElements = htmlDocument.body().children()
        val indexOfEnglish = bodyElements.indexOfFirst { it.text().toLowerCase() == "english" }
        val indexOfFirstIrrelevant = bodyElements.indexOfFirst { it.tagName() == "hr" }
        val englishElements = if (indexOfFirstIrrelevant >= 0) {
            Elements(bodyElements.subList(indexOfEnglish, indexOfFirstIrrelevant))
        } else {
            bodyElements
        }
        val relevantElements = englishElements.select("ol:not(:has(ol)) > li:lt(2)").filter {
            val text = it.text()
            text.isNotBlank() && text.length > 3
        }
        val trimmedDefinitions = relevantElements.map {
            val definition = it.text().trim().split("""\n""", limit = 2).first()

            val pluralOf = "plural of "
            if (definition.startsWith(pluralOf)) {
                val singular = definition.split(pluralOf, limit = 2).last()
                "$pluralOf\"$singular\""
            } else {
                definition
            }
        }

        storedDefinitions[word] = trimmedDefinitions
        return trimmedDefinitions

    } catch (exception: Exception) {
        return emptyList()
    }
}
