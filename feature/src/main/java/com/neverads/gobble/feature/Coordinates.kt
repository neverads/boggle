package com.neverads.gobble.feature

data class Coordinate(val rowIndex: Int, val columnIndex: Int)

val coordinate00 = Coordinate(0, 0)
val coordinate01 = Coordinate(0, 1)
val coordinate02 = Coordinate(0, 2)
val coordinate03 = Coordinate(0, 3)
val coordinate10 = Coordinate(1, 0)
val coordinate11 = Coordinate(1, 1)
val coordinate12 = Coordinate(1, 2)
val coordinate13 = Coordinate(1, 3)
val coordinate20 = Coordinate(2, 0)
val coordinate21 = Coordinate(2, 1)
val coordinate22 = Coordinate(2, 2)
val coordinate23 = Coordinate(2, 3)
val coordinate30 = Coordinate(3, 0)
val coordinate31 = Coordinate(3, 1)
val coordinate32 = Coordinate(3, 2)
val coordinate33 = Coordinate(3, 3)

val coordinates4x4 = arrayOf(
    arrayOf(coordinate00, coordinate01, coordinate02, coordinate03),
    arrayOf(coordinate10, coordinate11, coordinate12, coordinate13),
    arrayOf(coordinate20, coordinate21, coordinate22, coordinate23),
    arrayOf(coordinate30, coordinate31, coordinate32, coordinate33)
)
