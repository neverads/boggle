package com.neverads.gobble.feature

import android.content.ClipData
import android.content.ClipboardManager
import android.content.Context
import android.content.SharedPreferences
import android.net.ConnectivityManager
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.text.Editable
import android.text.TextWatcher
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import com.neverads.boggle.feature.R
import kotlinx.android.synthetic.main.activity_your_dictionary.*

const val markerBoggleAddedWords = "<<<ADDED_WORDS>>>"
const val markerBoggleRemovedWords = "<<<REMOVED_WORDS>>>"
const val markerBoggleEnd = "<<<END>>>"

class YourDictionaryActivity : AppCompatActivity() {
    private val disabledAlpha = 0.2f
    private var customDictionaryWords = mutableListOf<DictionaryWord>()
    private var dictionaryWordsToDisplay = mutableListOf<DictionaryWord>()
    private var positionOfWordPreviouslyScrolledTo = 0
    private var positionOfCurrentWord = -1
    private var shouldNeverDisplayDictionaryChoiceAlert = false


    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.menu_dictionary, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.action_dictionary_import -> {
                val instructions = "Whenever you tap this button, we will check what you have copied (as in 'copy-paste') and try to import it.\n\n" +
                        "You can import a custom dictionary, as long as:\n" +
                        "  1. It's one block of text with line-breaks\n" +
                        "  2. Each word is made up of only letters\n" +
                        "  3. Sections are marked like this:\n\n" +
                        "    $markerBoggleAddedWords\n" +
                        "    aaa\n" +
                        "    bbb\n" +
                        "    $markerBoggleRemovedWords\n" +
                        "    zzz\n" +
                        "    $markerBoggleEnd\n\n" +
                        "Sorry, but what you currently have copied is not importable.  " +
                        "If you didn't expect this, you can email us: never.ads.info@gmail.com."
                val continueMessage = "Looks like you're ready to import your custom dictionary!\n\n" +
                        "Note that this will completely replace your current custom dictionary.\n\n" +
                        "You cannot undo this action."
                val importableOrNull = getImportableOrNull(getSystemService(CLIPBOARD_SERVICE) as ClipboardManager)
                val isImportable = importableOrNull != null
                AlertDialog.Builder(this)
                    .setTitle("Import custom dictionary")
                    .setMessage(if (isImportable) continueMessage else instructions)
                    .setPositiveButton(if (isImportable) "Import" else "OK") { _, _ ->
                        if (isImportable) {
                            val prefs: SharedPreferences = getSharedPreferences(getString(R.string.preferences), Context.MODE_PRIVATE)
                            val addedWords = importableOrNull?.first()
                            val removedWords = importableOrNull?.last()
                            if (addedWords.isNullOrEmpty()) {
                                prefs
                                    .edit()
                                    .remove(getString(R.string.preference_custom_dict_added_words))
                                    .apply()
                            } else {
                                prefs
                                    .edit()
                                    .putStringSet(getString(R.string.preference_custom_dict_added_words), addedWords.toSet())
                                    .apply()
                            }
                            if (removedWords.isNullOrEmpty()) {
                                prefs
                                    .edit()
                                    .remove(getString(R.string.preference_custom_dict_removed_words))
                                    .apply()
                            } else {
                                prefs
                                    .edit()
                                    .putStringSet(getString(R.string.preference_custom_dict_removed_words), removedWords.toSet())
                                    .apply()
                            }
                            recreate()
                        }
                    }
                    .setNegativeButton("Cancel", null)
                    .create()
                    .show()
                true
            }
            R.id.action_dictionary_export -> {
                AlertDialog.Builder(this)
                    .setTitle("Export custom dictionary")
                    .setMessage(
                        "You can copy your custom dictionary for later use.\n\n" +
                                "Maybe you want to share it with a fellow Gobble player, to transfer it to the same app on a new device, " +
                                "or to just have a backup.\n\n" +
                                "Tap the button below to copy (as in copy-paste) your custom dictionary.  " +
                                "Then you can paste it in a file, email, text-message, etc. for later use."
                    )
                    .setPositiveButton("Export/copy") { _, _ ->
                        val myClipboard = getSystemService(CLIPBOARD_SERVICE) as ClipboardManager
                        myClipboard.primaryClip = ClipData.newPlainText("Boggle custom dictionary", getExportableCustomDictionary())
                        Toast.makeText(this, "Custom dictionary copied!", Toast.LENGTH_LONG).show()
                    }
                    .setNegativeButton("Cancel", null)
                    .create()
                    .show()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_your_dictionary)

        shouldNeverDisplayDictionaryChoiceAlert = getSharedPreferences(getString(R.string.preferences), Context.MODE_PRIVATE)
            .getBoolean(getString(R.string.preference_should_never_display_dict_choice_alert), false)
        customDictionaryWords = getCustomDictionaryAddedWords()
        dictionaryWordsToDisplay.clear()
        dictionaryWordsToDisplay.addAll(customDictionaryWords)

        imageButtonAddWord.isEnabled = false
        imageButtonAddWord.alpha = disabledAlpha

        recyclerViewDictionary.layoutManager = LinearLayoutManager(this)
        recyclerViewDictionary.addItemDecoration(DividerItemDecoration(this, DividerItemDecoration.VERTICAL))
        updateDictionaryRecyclerUI()

        imageButtonAddWord.setOnClickListener {
            val text = editTextDictionarySearch.text
            if (text.isNullOrBlank().not()) {
                saveEnteredWordToDictionary()
                disableAddButton()
                val word = text.toString().toLowerCase()
                updateDictionaryRecyclerUI(startingLetter = word.first(), wordToHighlight = word)
                positionOfCurrentWord = dictionaryWordsToDisplay.indexOfWord(word = word)
                recyclerViewDictionary.scrollToPosition(positionOfCurrentWord)
                positionOfWordPreviouslyScrolledTo = positionOfCurrentWord
            }
        }
        editTextDictionarySearch.addTextChangedListener(object : TextWatcher {

            override fun afterTextChanged(s: Editable?) {
                if (s.isNullOrBlank()) {
                    updateDictionaryRecyclerUI()
                }
                setAddButtonIsEnabledBasedOnWord(word = s.toString().toLowerCase())
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                val word = s.toString().toLowerCase()
                if (s.isNullOrBlank()) {
                    updateDictionaryRecyclerUI()
                } else if (s.length == 1 && before == 0) {
                    updateDictionaryRecyclerUI(startingLetter = word.first())
                } else {
                    positionOfCurrentWord = dictionaryWordsToDisplay.indexOfFirst { dictionaryWord ->
                        dictionaryWord.word.startsWith(word)
                    }
                    if (positionOfCurrentWord >= 0) {
                        updateDictionaryRecyclerUI(startingLetter = word.first(), wordToHighlight = word)
                        recyclerViewDictionary.scrollToPosition(positionOfCurrentWord)
                        positionOfWordPreviouslyScrolledTo = positionOfCurrentWord
                    } else {
                        updateDictionaryRecyclerUI(startingLetter = word.first())
                        recyclerViewDictionary.scrollToPosition(positionOfWordPreviouslyScrolledTo)
                    }
                }
            }
        })
        imageButtonValidWordInfo.setOnClickListener {
            AlertDialog.Builder(this)
                .setIcon(R.drawable.ic_dictionary_icon_primary_color)
                .setTitle(R.string.alert_title_valid_word_info)
                .setMessage(R.string.alert_message_valid_word_info)
                .setPositiveButton("ok", null)
                .create()
                .show()
        }
    }

    private fun getCustomDictionaryAddedWords(): MutableList<DictionaryWord> {
        return getSharedPreferences(getString(R.string.preferences), Context.MODE_PRIVATE)
            .getStringSet(getString(R.string.preference_custom_dict_added_words), setOf())!!
            .sorted()
            .map {
                DictionaryWord(word = it)
            }
            .toMutableList()
    }

    private fun getStandardDictionaryAsStrings(startingLetter: Char): List<DictionaryWord> {
        val fileName = getString(R.string.file_name_dictionary, startingLetter)
        var wordsFromDictionary = emptyList<String>()
        try {
            wordsFromDictionary = assets.open(fileName).bufferedReader().readLines()
        } catch (e: Exception) {
            AlertDialog.Builder(this)
                .setIcon(R.drawable.ic_warning)
                .setTitle(R.string.alert_title_dictionary_file_error)
                .setMessage(getString(R.string.message_dictionary_file_general_error, e.message))
                .setPositiveButton("ok", null)
                .create()
                .show()
        } finally {
            return wordsFromDictionary.asDictionaryWords(areCustom = false, removedWords = getRemovedWords())
        }
    }

    private fun saveEnteredWordToDictionary() {
        val enteredWord = editTextDictionarySearch.text.toString().toLowerCase()
        customDictionaryWords.add(DictionaryWord(word = enteredWord))
        customDictionaryWords.sortWith(compareBy { it.word })
        getSharedPreferences(getString(R.string.preferences), Context.MODE_PRIVATE)
            .edit()
            .putStringSet(getString(R.string.preference_custom_dict_added_words), customDictionaryWords.asSortedSetOfStrings())
            .apply()
    }

    private fun setAddButtonIsEnabledBasedOnWord(word: String) {
        if (word.isValidBoggleWord() && dictionaryWordsToDisplay.containsWord(word).not()) {
            enableAddButton()
        } else {
            disableAddButton()
        }
    }

    private fun disableAddButton() {
        imageButtonAddWord.isEnabled = false
        imageButtonAddWord.alpha = disabledAlpha
    }

    private fun enableAddButton() {
        imageButtonAddWord.isEnabled = true
        imageButtonAddWord.alpha = 1.0f
    }

    private fun isDeviceConnected(): Boolean {
        val connectivityManager = getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetworkInfo = connectivityManager.activeNetworkInfo
        return activeNetworkInfo != null && activeNetworkInfo.isConnected
    }

    private fun updateDictionaryRecyclerUI(startingLetter: Char? = null, wordToHighlight: String? = null) {
        if (startingLetter == null) {
            dictionaryWordsToDisplay.clear()
            dictionaryWordsToDisplay.addAll(customDictionaryWords)
            val removedWords = mutableListOf<DictionaryWord>()
            getRemovedWords().forEach { removedWords.add(DictionaryWord(word = it, isCustom = false, isRemoved = true)) }
            dictionaryWordsToDisplay.addAll(removedWords)
            if (dictionaryWordsToDisplay.isEmpty()) {
                val instructions = listOf(
                    DictionaryWord(word = "add", isCustom = false),
                    DictionaryWord(word = "words", isCustom = false),
                    DictionaryWord(word = "to", isCustom = false),
                    DictionaryWord(word = "customize", isCustom = false),
                    DictionaryWord(word = "your", isCustom = false),
                    DictionaryWord(word = "dictionary", isCustom = false)
                )
                dictionaryWordsToDisplay.addAll(instructions)
            }
            recyclerViewDictionary.adapter = RecyclerAdapterDictionary(dictionaryWordsToDisplay, isDeviceConnected())
        } else {
            dictionaryWordsToDisplay.clear()
            dictionaryWordsToDisplay.addAll(getStandardDictionaryAsStrings(startingLetter))
            dictionaryWordsToDisplay.addAll(customDictionaryWords.startingWithLetter(startingLetter))
            dictionaryWordsToDisplay.sortBy { it.word }
            recyclerViewDictionary.adapter = RecyclerAdapterDictionary(dictionaryWordsToDisplay, isDeviceConnected(), wordToHighlight)
        }
        recyclerViewDictionary.invalidate()
    }

    private fun getRemovedWords(): Set<String> {
        return getSharedPreferences(getString(R.string.preferences), Context.MODE_PRIVATE)
            .getStringSet(getString(R.string.preference_custom_dict_removed_words), emptySet<String>())!!
            .toSortedSet()
    }


    private fun getExportableCustomDictionary(): String {
        val prefs: SharedPreferences = getSharedPreferences(getString(R.string.preferences), Context.MODE_PRIVATE)
        val addedWords = prefs.getStringSet(getString(R.string.preference_custom_dict_added_words), emptySet())?.joinToString("\n")
        val addedWordsPostfix = if (addedWords.isNullOrBlank()) "" else "\n"
        val removedWords = prefs.getStringSet(getString(R.string.preference_custom_dict_removed_words), emptySet())?.joinToString("\n")
        val removedWordsPostfix = if (removedWords.isNullOrBlank()) "" else "\n"
        return "$markerBoggleAddedWords\n$addedWords$addedWordsPostfix$markerBoggleRemovedWords\n$removedWords$removedWordsPostfix$markerBoggleEnd"
    }
}

fun getImportableOrNull(clipboardManager: ClipboardManager): List<List<String>>? {
    try {
        var copiedText = clipboardManager.primaryClip?.getItemAt(0)?.text.toString().trim()
        require(copiedText.startsWith(markerBoggleAddedWords) && copiedText.endsWith(markerBoggleEnd))
        copiedText = copiedText.removePrefix(markerBoggleAddedWords)
        copiedText = copiedText.removeSuffix(markerBoggleEnd)
        val split = copiedText.split(markerBoggleRemovedWords)
        require(split.size == 2)
        val addedWords = mutableListOf<String>()
        split.first().split('\n').forEach { line ->
            if (line.isNotBlank()) {
                val trimmed = line.trim()
                require(trimmed.all { char -> char.isLetter() })
                addedWords.add(trimmed.toLowerCase())
            }
        }
        val removedWords = mutableListOf<String>()
        split.last().split('\n').forEach { line ->
            if (line.isNotBlank()) {
                val trimmed = line.trim()
                require(trimmed.all { char -> char.isLetter() })
                removedWords.add(trimmed.toLowerCase())
            }
        }
        return listOf(addedWords, removedWords)
    } catch (e: Exception) {
        return null
    }
}
