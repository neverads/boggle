package com.neverads.gobble.feature

import android.app.Activity
import android.content.*
import android.net.ConnectivityManager
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.text.SpannableString
import android.text.SpannableStringBuilder
import android.text.Spanned
import android.text.style.RelativeSizeSpan
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import com.neverads.boggle.feature.R
import kotlinx.android.synthetic.main.fragment_scoring.*


val wordsWithDictionaryToggleDisplayed: MutableList<String> = mutableListOf()


class ScoringActivity : AppCompatActivity() {
    private var countedWords = emptyArray<String>()
    private var foundWords = mutableListOf<String>()
    private var isCustomDict = true
    private var words: MutableList<Word> = mutableListOf()
    private var scoringStyle = ScoringStyles.SIMPLIFIED
    private var missedWords = emptyArray<String>()
    private var previousGameCode = ""
    var shouldShowMissedWords = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.fragment_scoring)

        shouldShowMissedWords = getSharedPreferences(getString(R.string.preferences), Context.MODE_PRIVATE)
            .getBoolean(getString(R.string.preference_should_show_missed_words), false)
        val minWordLength = getSharedPreferences(getString(R.string.preferences), Context.MODE_PRIVATE)
            .getInt(getString(R.string.preference_min_word_length), default_min_word_length)
        val extras = intent.extras!!
        previousGameCode = extras.getString(getString(R.string.game_code_intent_label))!!
        countedWords = extras.getStringArray(getString(R.string.counted_words_list_intent_label))!!
        missedWords = extras.getStringArray(getString(R.string.missed_words_list_intent_label))!!.filter { it.length >= minWordLength }.toTypedArray()
        val diceLetters = extras.getCharArray(getString(R.string.dice_letters_intent_label))!!
        val grid = listOf(
            _fakeDie00,
            _fakeDie01,
            _fakeDie02,
            _fakeDie03,
            _fakeDie10,
            _fakeDie11,
            _fakeDie12,
            _fakeDie13,
            _fakeDie20,
            _fakeDie21,
            _fakeDie22,
            _fakeDie23,
            _fakeDie30,
            _fakeDie31,
            _fakeDie32,
            _fakeDie33
        )
        grid.forEachIndexed { index, textView ->
            textView.text = diceLetters[index].toString()
        }

        if (getPreferenceShouldShowPossibleScore()) {
            textViewScoreTotal.setOnClickListener {
                AlertDialog.Builder(this)
                    .setTitle("What is the 'possible' score?")
                    .setMessage(
                        "It shows how many points you would have gotten if you had also found all the 'Missed Words' above.\n\n" +
                                "You can enable/disable this feature in settings."
                    )
                    .setPositiveButton("ok", null)
                    .create()
                    .show()
            }
        }

        wordsWithDictionaryToggleDisplayed.clear()
        isCustomDict = getIsCustomDict()
        scoringStyle = getSavedScoringStyle()

        recyclerViewScoring.layoutManager = LinearLayoutManager(this)
        recyclerViewScoring.addItemDecoration(DividerItemDecoration(this, DividerItemDecoration.VERTICAL))
        foundWords.addAll(extras.getStringArray(getString(R.string.selected_words_list_intent_label))!!)
        foundWords.forEach {
            words.add(Word(text = it, isCounted = countedWords.contains(it)))
        }
        setScoreUI()
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        val extras = intent.extras!!
        val isInfiniteMode = extras.getBoolean(getString(R.string.is_infinite_mode_intent_label))
        val inflater = menuInflater
        if (isInfiniteMode) {
            inflater.inflate(R.menu.menu_scoring_without_copy_game_code, menu)
        } else {
            inflater.inflate(R.menu.menu_scoring, menu)
        }
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.action_high_scores -> {
                showHighScores(
                    getSharedPreferences(getString(R.string.preferences), Context.MODE_PRIVATE),
                    getString(R.string.preference_high_scores),
                    getString(R.string.preference_is_scoring_style_classic),
                    gameCode.first(),
                    getString(R.string.high_scores),
                    this
                )
                return true
            }
            R.id.action_copy_game_code -> {
                val clipboardManager = getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager
                val clipData = ClipData.newPlainText("This 'game-code' for Gobble can be used to replay a game.", previousGameCode)
                clipboardManager.setPrimaryClip(clipData)
                Toast.makeText(this, "Game-code copied: $previousGameCode.\n(Use to replay this game)", Toast.LENGTH_LONG).show()
                return true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onBackPressed() {
        val intent = Intent()
        intent.putExtra(getString(R.string.counted_words_list_intent_label), countedWords)
        setResult(Activity.RESULT_CANCELED, intent)
        super.onBackPressed()
    }

    private fun getIsCustomDict(): Boolean {
        return getSharedPreferences(getString(R.string.preferences), Context.MODE_PRIVATE)
            .getBoolean(getString(R.string.preference_should_use_custom_dictionary), true)
    }

    private fun getSavedScoringStyle(): ScoringStyles {
        val isClassic = getSharedPreferences(getString(R.string.preferences), Context.MODE_PRIVATE)
            .getBoolean(getString(R.string.preference_is_scoring_style_classic), false)
        return when {
            isClassic -> ScoringStyles.CLASSIC
            else -> ScoringStyles.SIMPLIFIED
        }
    }

    private fun isDeviceConnected(): Boolean {
        val connectivityManager = getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetworkInfo = connectivityManager.activeNetworkInfo
        return activeNetworkInfo != null && activeNetworkInfo.isConnected
    }

    private fun setScoreUI() {
        var wordsToDisplay = words.sortByPossiblePoints()
        val missedWordsHeaderText: String?
        if (missedWords.isEmpty()) {
            missedWordsHeaderText = null
        } else if (shouldShowMissedWords) {
            wordsToDisplay = wordsToDisplay + missedWords.map { Word(text = it, isMissed = true) }
            missedWordsHeaderText = getString(R.string.missed_words_header_hide)
        } else {
            missedWordsHeaderText = getString(R.string.missed_words_header_show)
        }
        recyclerViewScoring.adapter = RecyclerAdapterScoring(
            words = wordsToDisplay,
            clickListener = { setTotalPointsText(); countedWords = words.getCounted() },
            scoringStyle = scoringStyle,
            isCustomDict = isCustomDict,
            isDeviceConnected = isDeviceConnected(),
            onMissedWordsTapped = {
                val scrollPosition = (recyclerViewScoring.layoutManager as LinearLayoutManager).findFirstVisibleItemPosition()
                shouldShowMissedWords = !shouldShowMissedWords
                getSharedPreferences(getString(R.string.preferences), Context.MODE_PRIVATE)
                    .edit()
                    .putBoolean(getString(R.string.preference_should_show_missed_words), shouldShowMissedWords)
                    .apply()
                setScoreUI()
                recyclerViewScoring.invalidate()
                recyclerViewScoring.scrollToPosition(scrollPosition)
            },
            onMissedWordsIconTapped = { showMissedWordsPopUp(this) },
            missedWordsHeaderText = missedWordsHeaderText
        )
        recyclerViewScoring.invalidate()
        setTotalPointsText()
    }

    private fun setTotalPointsText() {
        val totalPoints = words.getTotalPoints(scoringStyle)
        val totalText = "${getString(R.string.score_total_text)}$totalPoints"
        if (getPreferenceShouldShowPossibleScore()) {
            val possibleWords = (foundWords + missedWords).toSet()
            val possiblePoints = possibleWords.map { Word(it) }.getTotalPoints(getScoringType())
            val possibleText = SpannableString("\n($possiblePoints possible)")
            possibleText.setSpan(RelativeSizeSpan(0.75f), 0, possibleText.length, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE)
            val builder = SpannableStringBuilder(totalText)
            builder.append(possibleText)
            textViewScoreTotal.text = builder
        } else {
            textViewScoreTotal.text = totalText
        }
    }

    private fun getPreferenceShouldShowPossibleScore(): Boolean {
        return getSharedPreferences(getString(R.string.preferences), Context.MODE_PRIVATE)
            .getBoolean(getString(R.string.preference_should_show_possible_score), false)
    }

    private fun getScoringType(): ScoringStyles {
        val isClassicScoring = getSharedPreferences(getString(R.string.preferences), Context.MODE_PRIVATE)
            .getBoolean(getString(R.string.preference_is_scoring_style_classic), false)
        return when {
            isClassicScoring -> ScoringStyles.CLASSIC
            else -> ScoringStyles.SIMPLIFIED
        }
    }

    private fun showMissedWordsPopUp(context: Context) {
        android.app.AlertDialog.Builder(context)
            .setIcon(R.drawable.ic_info_outline)
            .setTitle("Missed Words")
            .setMessage(
                "These are some common English words which were available in your game, but you didn't select them.\n\n" +
                        "We don't show you every possible word, because that would just be waaay too many."
            )
            .setPositiveButton("ok", null)
            .create()
            .show()
    }
}

fun showHighScores(
    prefs: SharedPreferences,
    prefsKeyHighScores: String,
    prefsKeyIsClassicScoring: String,
    firstGameCodeChar: Char,
    titleOfPopup: String,
    context: Context
) {
    val highScoresAsStrings = prefs.getStringSet(prefsKeyHighScores, emptySet())!!
    val isClassicScoring = prefs.getBoolean(prefsKeyIsClassicScoring, false)
    val scoring = when {
        isClassicScoring -> ScoringStyles.CLASSIC
        else -> ScoringStyles.SIMPLIFIED
    }
    val highScores = highScoresAsStrings.map { HighScore.fromString(it) }.toMutableList()
    if (isClassicScoring) {
        highScores.sortByDescending { it.pointsClassic }
    } else {
        highScores.sortByDescending { it.pointsSimplified }
    }
    val highScoresSameTimer = highScores.filter { it.gameCode.startsWith(firstGameCodeChar) }

    val highScoresWithSameTimer = android.app.AlertDialog.Builder(context)
    val highScoresAll = android.app.AlertDialog.Builder(context)

    val placeholderMessage = "Nothing to show here… yet."
    val timerChars = highScores.map { it.gameCode.first() }.toSortedSet()
    var highScoresAllMessage = ""
    timerChars.forEach { char ->
        highScoresAllMessage += "\uD83D\uDD59  ${getFormattedTimeFromSeconds(getTimerSecondsFromGameCode("${char}xxxxx"))}\n"
        highScores.filter { it.gameCode.startsWith(char) }.map { highScoresAllMessage += "${it.getFormatted(scoring)}\n" }
        if (char != timerChars.last()) {
            highScoresAllMessage += "\n"
        }
    }

    highScoresAll
        .setIcon(R.drawable.ic_trophy)
        .setTitle("all $titleOfPopup")
        .setMessage(if (highScoresAllMessage.isEmpty()) placeholderMessage else highScoresAllMessage)
        .setPositiveButton("ok", null)
        .setNegativeButton("delete all") { _, _ ->
            android.app.AlertDialog.Builder(context)
                .setIcon(R.drawable.ic_warning)
                .setTitle("Delete all?!")
                .setMessage("Are you absolutely sure you want to PERMANENTLY delete ALL your high scores?")
                .setPositiveButton("delete all") { _, _ ->
                    prefs.edit().remove(prefsKeyHighScores).apply()
                    isPaused = false
                }
                .setNegativeButton("cancel", null)
                .setOnDismissListener { isPaused = false }
                .create()
                .show()
        }
        .setNeutralButton("see fewer") { dialog, _ ->
            dialog.dismiss()
            highScoresWithSameTimer.show()
        }
        .create()

    val formattedTime = getFormattedTimeFromSeconds(getTimerSecondsFromGameCode("${firstGameCodeChar}xxxxx"))
    val highScoresWithSameTimerMessage = highScoresSameTimer.joinToString("\n") { it.getFormatted(scoring) }
    highScoresWithSameTimer
        .setIcon(R.drawable.ic_trophy)
        .setTitle("$titleOfPopup  -  $formattedTime")
        .setMessage(if (highScoresWithSameTimerMessage.isEmpty()) placeholderMessage else highScoresWithSameTimerMessage)
        .setPositiveButton("ok", null)
        .setNeutralButton("see all") { dialog, _ ->
            dialog.dismiss()
            highScoresAll.show()
        }
        .create()

    highScoresWithSameTimer.show()
}
