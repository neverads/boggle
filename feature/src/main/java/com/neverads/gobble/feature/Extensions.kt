package com.neverads.gobble.feature

import android.graphics.Rect
import android.support.annotation.LayoutRes
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import kotlin.math.roundToInt
import kotlin.math.sqrt

fun Array<_Die>.getLetters(): CharArray {
    val letters = CharArray(this.size)
    this.forEachIndexed { index, die ->
        letters[index] = die.getCurrentLetter()
    }
    return letters
}

fun Array<_Die>.getSumOfDiceSides(indicesOfSides: IntArray): Int {
    require(indicesOfSides.min()!! >= 0)
    require(indicesOfSides.max()!! < this.size)

    var sum = 0
    indicesOfSides.forEach { sum += this[it].currentSide }
    return sum
}

fun Array<_Die>.getDiceSides(): IntArray {
    val diceSides = mutableListOf<Int>()
    this.forEach { diceSides.add(it.currentSide) }
    return diceSides.toIntArray()
}

fun Array<_Die>.getSortedByInitialIndex(): Array<_Die> {
    val sorted = Array<_Die?>(this.size) { null }
    this.forEach { die ->
        sorted[die.initialIndex] = die
    }
    return sorted.requireNoNulls()
}

fun Array<_Die>.getInitialIndices(): IntArray {
    val actualOrderByInitialSides = mutableListOf<Int>()
    this.forEach { actualOrderByInitialSides.add(it.initialIndex) }
    return actualOrderByInitialSides.toIntArray()
}

fun CharArray.toTwoDimensionalArray(): Array<CharArray> {
    val dimensionSize = sqrt(this.size.toDouble())
    require(dimensionSize >= 3.0) { "square root of size (${this.size}) of char-array [${this.joinToString()}] is '$dimensionSize' -- must be >= 3" }
    val isOneDimensionalArrayAPerfectSquare = dimensionSize.rem(1) == 0.0
    require(isOneDimensionalArrayAPerfectSquare) { "'${this.size}' is not a perfect square" }
    val dimensionSizeAsInt = dimensionSize.roundToInt()
    val twoDimensionalArray = Array(dimensionSizeAsInt) { CharArray(dimensionSizeAsInt) }
    twoDimensionalArray.forEachIndexed { rowIndex, row ->
        row.forEachIndexed { columnIndex, _ ->
            val indexFromOriginalArray = (rowIndex * dimensionSizeAsInt) + columnIndex
            twoDimensionalArray[rowIndex][columnIndex] = this[indexFromOriginalArray]
        }
    }
    return twoDimensionalArray
}

fun Set<Die>.setIsAvailable(isAvailable: Boolean) {
    this.forEach { die -> die.isAvailable = isAvailable }
}

fun Set<Die>.getConnectedDie(row: Int, column: Int): Die {
    this.forEach { die -> if (die.rowIndex == row && die.columnIndex == column) return die }
    throw NoSuchElementException("No 'Die' was found with row $row & column $column.")
}

fun Set<Die>.getDieWithEvent(event: MotionEvent): Die? {
    this.forEach { die ->
        if (event.isWithinView(view = die.textView)) {
            return die
        }
    }
    return null
}

fun Set<Die>.getConnectedDice(die1RowColumn: Pair<Int, Int>, die2RowColumn: Pair<Int, Int>): Pair<Die, Die> {
    val die1 = this.getConnectedDie(row = die1RowColumn.first, column = die1RowColumn.second)
    val die2 = this.getConnectedDie(row = die2RowColumn.first, column = die2RowColumn.second)
    return Pair(die1, die2)
}

fun MutableList<Die>.getWord(): String {
    var word = ""
    this.forEach { die ->
        word += die.textView.text
    }
    return word.toLowerCase()
}

fun Set<Line>.getByConnectingDice(die1: Die, die2: Die): Line {
    this.forEach {
        if (die1 in it.connectedDice.toList() && die2 in it.connectedDice.toList()) {
            return it
        }
    }
    throw NoSuchElementException("No 'Line' was found which connects dice $die1 & $die2.")
}

fun Set<Line>.setInvisible() {
    this.forEach { it.imageView.visibility = View.INVISIBLE }
}

fun Set<Die>.setTextColor(color: Int) {
    this.forEach { die ->
        die.textView.setTextColor(color)
    }
}

fun Set<Die>.setTexts(chars: CharArray) {
    require(this.size == chars.size) {
        "Must have exactly one TextView for each char in 'textsAsString'.\n" +
                "    text-views: ${this.size}\n" +
                "    [${chars.joinToString()}]-length: ${chars.size}"
    }
    this.forEachIndexed { index, die ->
        var dieText = chars[index].toString()
        if (dieText == "Q") {
            dieText = "Qu"
        }
        die.textView.text = dieText
    }
}

fun List<String>.asDictionaryWords(areCustom: Boolean, removedWords: Set<String>): List<DictionaryWord> {
    val dictionaryWords = mutableListOf<DictionaryWord>()
    this.forEach {
        dictionaryWords.add(DictionaryWord(word = it, isCustom = areCustom, isRemoved = removedWords.contains(it)))
    }
    return dictionaryWords
}

fun List<Word>.getAllTexts(): List<String> {
    val allTexts = mutableListOf<String>()
    this.forEach {
        allTexts.add(it.text)
    }
    return allTexts
}

fun List<Word>.getTotalPoints(scoringStyle: ScoringStyles): Int {
    var pointsTotal = 0
    this.forEach {
        pointsTotal += if (scoringStyle == ScoringStyles.CLASSIC) {
            it.pointsWithClassicScoring
        } else {
            it.pointsWithSimplifiedScoring
        }
    }
    return pointsTotal
}

fun List<Word>.getCounted(): Array<String> {
    val countedWords = this.filter { it.isCounted }
    val textsOfCountedWords = countedWords.map { it.text }
    return textsOfCountedWords.toTypedArray()
}

fun List<Word>.sortByPossiblePoints(): List<Word> {
    val sorted = this.sortedWith(compareBy(Word::possiblePointsWithSimplifiedScoring, Word::text))
    val tinyWords = sorted.filter { it.text.length == 2 }
    return tinyWords + (sorted - tinyWords)
}

fun MotionEvent.isWithinView(view: View): Boolean {
    val dieRect = Rect()
    view.getGlobalVisibleRect(dieRect)
    return dieRect.contains(this.rawX.roundToInt(), this.rawY.roundToInt())
}

fun String.isGameCode(): Boolean {
    val isCorrectLength = this.trim().length == 6
    val hasOnlyLetters = this.trim().toCharArray().all { it.isLetter() }
    return isCorrectLength && hasOnlyLetters
}

fun String.isValidBoggleWord(): Boolean {
    val maxLength = if ('q' in this) 17 else 16
    val hasValidLength = this.length in 3..maxLength
    val hasOnlyLowerLetters = this.all { char ->
        char.isLowerCase()
    }
    return hasValidLength && hasOnlyLowerLetters
}

fun ViewGroup.inflate(@LayoutRes layoutRes: Int, shouldAttachToRoot: Boolean = false): View {
    return LayoutInflater.from(context).inflate(layoutRes, this, shouldAttachToRoot)
}

fun List<DictionaryWord>.asSortedSetOfStrings(): Set<String> {
    val setOfStrings = mutableSetOf<String>()
    this.forEach { setOfStrings.add(it.word) }
    return setOfStrings.toSortedSet()
}

fun List<DictionaryWord>.containsWord(word: String): Boolean {
    return this.indexOfWord(word) >= 0
}

fun List<DictionaryWord>.indexOfWord(word: String): Int {
    this.forEachIndexed { index, dictionaryWord ->
        if (word == dictionaryWord.word) return@indexOfWord index
    }
    return -1
}

fun List<DictionaryWord>.startingWithLetter(letter: Char?): List<DictionaryWord> {
    return this.filter { letter == it.word.first() }
}
