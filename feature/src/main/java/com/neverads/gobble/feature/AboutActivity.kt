package com.neverads.gobble.feature

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.text.method.LinkMovementMethod
import com.neverads.boggle.feature.R
import kotlinx.android.synthetic.main.activity_about.*

class AboutActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_about)

        aboutWhoWeAreTextView.movementMethod = LinkMovementMethod.getInstance()
        arrayOf(aboutOtherAppsJustWeatherTextView, imageViewJustWeatherIcon).forEach { view ->
            view.setOnClickListener { openUrl(getString(R.string.about_text_link_just_weather)) }
        }
        arrayOf(aboutOtherAppsSleepSweetTextView, imageViewSleepSweetIcon).forEach { view ->
            view.setOnClickListener { openUrl(getString(R.string.about_text_link_sleep_sweet)) }
        }
        arrayOf(aboutOtherAppsTalkToMeTextView, imageViewTalkToMeIcon).forEach { view ->
            view.setOnClickListener { openUrl(getString(R.string.about_text_link_talk_to_me)) }
        }
    }

    private fun openUrl(url: String) {
        val intent = Intent()
        intent.action = Intent.ACTION_VIEW
        intent.addCategory(Intent.CATEGORY_BROWSABLE)
        intent.data = Uri.parse(url)
        startActivity(intent)
    }
}
