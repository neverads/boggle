package com.neverads.gobble.feature

import java.util.*

const val sep = "___"

class HighScore(val pointsSimplified: Int, val pointsClassic: Int, val gameCode: String, val dateInMillis: Long, val version: Int = 0) {
    // version denotes which parsing algorithm we should use, in case this ever needs to be updated

    companion object Factory {
        fun fromString(string: String): HighScore {
            val split = string.split(sep)
            return HighScore(split[1].toInt(), split[2].toInt(), split[3], split.last().toLong(), split.first().toInt())
        }
    }

    val asPrefString: String
        get() {
            return "$version$sep$pointsSimplified$sep$pointsClassic$sep$gameCode$sep$dateInMillis"
        }

    fun getFormatted(scoringStyle: ScoringStyles): String {
        val date = Calendar.getInstance()
        date.timeInMillis = dateInMillis
        val formattedDate = "${date.get(Calendar.YEAR)}.${date.get(Calendar.MONTH) + 1}.${date.get(Calendar.DAY_OF_MONTH)}"

        val spacer = "   -   "
        if (scoringStyle == ScoringStyles.SIMPLIFIED) {
            return "$pointsSimplified points$spacer$gameCode$spacer$formattedDate"
        } else {
            return "$pointsClassic points$spacer$gameCode$spacer$formattedDate"
        }
    }
}
