package com.neverads.gobble.feature

import android.content.Context
import android.graphics.Color
import android.os.Handler
import android.support.v7.app.AlertDialog
import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import com.neverads.boggle.feature.R
import kotlinx.android.synthetic.main.recyclerview_item_row_dictionary.view.*


val storedDefinitions = mutableMapOf<String, List<String>>()


class RecyclerAdapterDictionary(
    private val dictionaryWords: List<DictionaryWord>,
    private val isDeviceConnected: Boolean,
    private val wordToHighlight: String? = null
) : RecyclerView.Adapter<RecyclerAdapterDictionary.WordHolder>() {

    override fun getItemCount() = dictionaryWords.size

    override fun onBindViewHolder(holder: WordHolder, position: Int) {
        val shouldHighlight = wordToHighlight == dictionaryWords[position].word
        holder.bindWord(dictionaryWords[position], shouldHighlight, isDeviceConnected)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): WordHolder {
        val inflatedView = parent.inflate(R.layout.recyclerview_item_row_dictionary)
        return WordHolder(inflatedView)
    }

    class WordHolder(v: View) : RecyclerView.ViewHolder(v) {
        private var view: View = v
        private var dictionaryWord: DictionaryWord? = null
        private val context = view.context
        private val handler = Handler()

        fun bindWord(dictionaryWord: DictionaryWord, shouldHighlight: Boolean, isDeviceConnected: Boolean) {
            if (isDeviceConnected) {
                view.imageButtonDictScreenLookUpDefinition.visibility = View.VISIBLE
            } else {
                view.imageButtonDictScreenLookUpDefinition.visibility = View.INVISIBLE
            }
            if (shouldHighlight) {
                val mintGreen = Color.parseColor("#DCF1E8")
                view.setBackgroundColor(mintGreen)
            } else {
                val lightGrey = Color.parseColor("#F3F3F3")
                view.setBackgroundColor(lightGrey)
            }

            if (dictionaryWord.isRemoved) {
                if (dictionaryWord.isCustom) {
                    view.alpha = 0.0f
                } else {
                    view.alpha = 0.5f
                }
            } else {
                view.alpha = 1.0f
            }

            this.dictionaryWord = dictionaryWord
            view.textViewWord.text = dictionaryWord.word
            view.imageViewIsCustomWord.setImageResource(R.drawable.ic_star)
            if (dictionaryWord.isCustom) {
                view.imageViewIsCustomWord.visibility = View.VISIBLE
            } else {
                view.imageViewIsCustomWord.visibility = View.INVISIBLE
            }
            view.setOnClickListener {
                dictionaryWord.isRemoved = dictionaryWord.isRemoved.not()
                if (dictionaryWord.isRemoved) {
                    if (dictionaryWord.isCustom) {
                        view.alpha = 0.0f
                    } else {
                        view.alpha = 0.5f
                    }
                } else {
                    view.alpha = 1.0f
                }
                updatePreferencesWithWord(dictionaryWord)
            }
            view.imageButtonDictScreenLookUpDefinition.setOnClickListener {
                view.progressBarDictScreenLookUpDefinition.visibility = View.VISIBLE
                view.imageButtonDictScreenLookUpDefinition.visibility = View.INVISIBLE

                Thread(object : Runnable {
                    override fun run() {
                        val definitions = getDefinitions(word = dictionaryWord.word, context = context)
                        handler.post {
                            run {
                                AlertDialog.Builder(context)
                                    .setIcon(R.drawable.ic_dictionary_icon_primary_color)
                                    .setTitle("\"${dictionaryWord.word}\"")
                                    .setMessage(definitions.joinToString("\n\n"))
                                    .setPositiveButton("ok", null)
                                    .show()
                            }
                        }
                        handler.post {
                            run {
                                view.progressBarDictScreenLookUpDefinition.visibility = View.INVISIBLE
                                view.imageButtonDictScreenLookUpDefinition.visibility = View.VISIBLE
                            }
                        }
                    }


                }).start()
            }
        }

        private fun updatePreferencesWithWord(dictionaryWord: DictionaryWord) {
            val refToGeneralPrefs = view.context.getString(R.string.preferences)
            val refToSpecificPrefs = if (dictionaryWord.isCustom) {
                view.context.getString(R.string.preference_custom_dict_added_words)
            } else {
                view.context.getString(R.string.preference_custom_dict_removed_words)
            }
            val preferences = view.context.getSharedPreferences(refToGeneralPrefs, Context.MODE_PRIVATE)
            val wordsFromPreferences = preferences.getStringSet(refToSpecificPrefs, emptySet<String>())!!
            if (dictionaryWord.isCustom == dictionaryWord.isRemoved) {
                preferences.edit().putStringSet(refToSpecificPrefs, wordsFromPreferences - dictionaryWord.word).apply()
            } else {
                preferences.edit().putStringSet(refToSpecificPrefs, wordsFromPreferences + dictionaryWord.word).apply()
            }
        }
    }
}
